from copy import deepcopy
from datetime import datetime, timezone
from io import BytesIO
import logging
from mastodon import Mastodon, MastodonError # type: ignore[import-untyped]
import os
import random
import string
from time import sleep, monotonic
import multiprocessing

import requests
from tenacity import RetryError, Retrying, before_sleep_log, retry, retry_if_exception_type, stop_after_attempt, wait_fixed
import db
import image

import util
import time

# Maximum number of options in a poll on this instance
MAX_POLL_SIZE = 4
# Maximum length of a status on this instance
STATUS_LIMIT = 500
# Maximum length of a single poll option on this instance
POLL_OPTION_LIMIT = 50
# Minimum poll duration on this instance
MIN_POLL_DURATION = 300
# Maximum poll duration on this instance
MAX_POLL_DURATION = 2629746

# Number of - to display on top and bottom of --dry posts
DRY_SEP_LEN = 30

# TODO: define hashtags in the config file instead
# Hashtag to use for tips
HASHTAG_TIP = "#tip"
# Hashtag to use for campaign start statuses
HASHTAG_CAMPAIGN_START = "#dnd"
# Hashtag to use for campaign end statuses
HASHTAG_CAMPAIGN_END = "#dndend"
# Hashtag to use for character level ups
HASHTAG_CHARACTER_LEVELUP = "#levelup"
# Hashtag to use for update posts
HASHTAG_UPDATE = "#update"

# Content Warning for tips
CW_TIP = "Tip"

# Whether to actually make posts or not
dry = False
# Whether to autorun campaigns (random options)
auto = False
# When dry, used for simulating poll ids
poll_id = 0
# When dry, used for keeping track of past poll options
last_poll: list[str] = []
# The last status ID that was posted
status_chain: int | None = None
# The last status dict that was posted
last_status: dict| None  = None
# Mastodon client
mastodon: Mastodon = None
# Thread to post tips on
tip_thread: multiprocessing.Process | None = None
# Thread to generate avatar images on
avatar_image_thread: multiprocessing.Process | None = None
# Thread to generate header images on
header_image_thread: multiprocessing.Process | None = None
# Thread to generate story images on
story_image_thread: multiprocessing.Process | None = None


def init_client():
    """Initialize the Mastodon connection.
    """
    global mastodon
    global STATUS_LIMIT, MAX_POLL_SIZE, POLL_OPTION_LIMIT, MIN_POLL_DURATION, MAX_POLL_DURATION

    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    configuration = mastodon.instance()["configuration"]
    STATUS_LIMIT = configuration["statuses"]["max_characters"]
    MAX_POLL_SIZE = configuration["polls"]["max_options"]
    POLL_OPTION_LIMIT = configuration["polls"]["max_characters_per_option"]
    MIN_POLL_DURATION = configuration["polls"]["min_expiration"]
    MAX_POLL_DURATION = configuration["polls"]["max_expiration"]


def start_image_gen(character, campaign):
    """Start a process to generate a new profile photo.

    `character`: the `Character` to generate from.
    `campaign`: the `Character` to generate from.
    """
    if dry:
        return
    global avatar_image_thread
    avatar_image_thread = multiprocessing.Process(target=_image_gen_avatar, args=(character, campaign), daemon=True)
    avatar_image_thread.start()
    global header_image_thread
    header_image_thread = multiprocessing.Process(target=_image_gen_header, args=(character, campaign), daemon=True)
    header_image_thread.start()


def start_image_reply_gen(character, campaign, description):
    """Start a process to generate an image to reply to the latest action.

    `character`: the `Character` to generate from.
    `campaign`: the `Character` to generate from.
    `description`: the text description of the current action.
    """
    if dry:
        return
    global story_image_thread
    story_image_thread = multiprocessing.Process(target=_image_gen_reply, args=(character, campaign, description), daemon=True)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def _image_gen_reply(character, campaign, description: image.Description):
    """Generates a new story photo and replies with it.

    `character`: the `Character` to generate from.
    `campaign`: the `Character` to generate from.
    `description`: the text description of the current action.
    """
    mastodon_image_replies = None
    try:
        mastodon_image_replies = Mastodon(access_token=os.path.join(util.root, "data/.token_image_replies"))
    except:
        util.info("No image reply token is set. Will not generate AI story replies.")
    if not mastodon_image_replies:
        return
    if not status_chain:
        util.info("Avoiding reply since there's no last status")
        return
    cached_last_status = last_status
    img_path = image.generate_reply(character, campaign, description)

    if img_path is None:
        util.error("Could not generate story image.")
        return

    weapon = character.hands[0].name
    armor = character.armor[0].name if character.armor else ""

    weapon_and_armor = f"equipped with a {weapon}"
    if armor:
        weapon_and_armor += f" and wearing {armor}."

    if description.hero:
        desc_text = (
            f"The #DnD hero {character.title(True)} {weapon_and_armor} {description.text}"
        )
    else:
        desc_text = (
            f"A #DnD {description.text}."
        )
    media = get_media_post(
        img_path,
        f"{desc_text}. Image generated via Stable Diffusion through The AI Horde.",
        mastodon_image_replies
    )

    if media is None:
        util.error(f"Could not upload media {img_path}.")
        return

    assert last_status
    for iter in range(util.config["max_retries"]):
        try:
            #TODO: Replace this with AI Horde story generation
            status_text = f"{desc_text}\n\nThis is a #StableDiffusion #AIArt image illustrating the current events.\n\n#AIHorde"
            visibility = "unlisted" if last_status["visibility"] == "public" else last_status["visibility"]
            mastodon_image_replies.status_reply(
                to_status=cached_last_status,
                status=status_text,
                media_ids=media,
                spoiler_text="AI-generated depiction of events",
                visibility=visibility,
                untag=True
            )
            util.info("Posted story reply")
            break
        except MastodonError as err:
            util.info(f"Error '{err}' when posting reply. Retry {iter + 1}/{util.config['max_retries']}")
            sleep(util.config["retry_delay"])


def _image_gen_avatar(character, campaign):
    """Generates a new character avatar and sets it.

    `character`: the `Character` to generate from.
    `campaign`: the `Character` to generate from.
    """
    urls = image.generate_avatar(character, campaign)

    if urls is None:
        reset_avatar()
        return

    db.upload_file(urls[0], f"{character.id}-avatar.webp")
    try:
        for attempt in Retrying(stop=stop_after_attempt(util.config["max_retries"]),
            wait=wait_fixed(util.config["retry_delay"]),
            retry=retry_if_exception_type((MastodonError)),
            before_sleep=before_sleep_log(util.logger, logging.WARN)
        ):
            with attempt:
                mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
                util.debug(f"Updating avatar with {urls}")
                mastodon.account_update_credentials(avatar=urls[1])
                util.debug("Updated account avatar")
    except RetryError:
        util.error("Failed to update account avatar.")


def _image_gen_header(character, campaign):
    """Generates a new header banner and sets it.

    `character`: the `Character` to generate from.
    `campaign`: the `Character` to generate from.
    """
    util.info("sleeping 5 before header")
    time.sleep(5) # We need to sleep 1 second to avoid running into the limiter
    urls = image.generate_header(character, campaign)

    if urls is None:
        reset_header()
        return

    db.upload_file(urls[0], f"{character.id}-header.webp")
    try:
        for attempt in Retrying(
            stop=stop_after_attempt(util.config["max_retries"]),
            wait=wait_fixed(util.config["retry_delay"]),
            retry=retry_if_exception_type((MastodonError)),
            before_sleep=before_sleep_log(util.logger, logging.WARN)
        ):
            with attempt:
                mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
                util.debug(f"Updating header with {urls}")
                mastodon.account_update_credentials(header=urls[1])
                util.debug("Updated account header")
    except RetryError:
        util.error("Failed to update account header.")


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def reset_avatar():
    """Reset the avatar to the default, if available.
    """
    if dry:
        return
    default_avatar = image.DEFAULT_AVATAR_PATH
    if os.path.exists(default_avatar):
        mastodon.account_update_credentials(avatar=default_avatar)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def reset_header():
    """Reset the header to the default, if available.
    """
    if dry:
        return
    default_header = image.DEFAULT_HEADER_PATH
    if os.path.exists(default_header):
        mastodon.account_update_credentials(header=default_header)


def start_tips():
    """Start a thread to display tips on an interval.
    """
    global tip_thread
    tip_thread = multiprocessing.Process(target=_tips, daemon=True)
    tip_thread.start()


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def _tips():
    """Posts tips on an interval.
    """
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    interval = util.config["tip_interval"]
    next_time = monotonic() + interval

    while mastodon:
        sleep(max(0, next_time - monotonic()))
        util.info("Posting tip")
        tip = random.choice(list(util.tips.values()))
        tip += f"\n{HASHTAG_TIP}"
        size = len(tip) + len(CW_TIP)
        if size > STATUS_LIMIT:
            tip = tip[:STATUS_LIMIT - len(CW_TIP) - 3] + "..."
            util.warn("Tried to post a tip exceeding the character limit.")
        if not dry:
            mastodon.status_post(tip, spoiler_text=CW_TIP)
        # Skips execution if posting took longer than interval
        next_time += ((monotonic() - next_time) // (interval * interval)) + interval


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_media_post(image_path: str, alt_text: str = "", client: Mastodon = None) -> dict | None:
    """Generate a media post dict from the given image.

    `image_path`: path or URL to the image.
    `alt_text`: alt text to use.

    Returns the media post dict, or None if an invalid image.
    """
    media_post = None
    if dry:
        return media_post

    if client is None:
        client = mastodon

    util.debug(f"Generating media for {image_path}...")

    for i in range(util.config["max_retries"]):
        try:
            extension = os.path.splitext(image_path)[1].replace(".", "")
            if os.path.exists(image_path):
                media_post = client.media_post(
                    image_path,
                    description=alt_text,
                    file_name=f"image.{extension}",
                )
                break
            else:
                image_req = requests.get(image_path)
                image = BytesIO(image_req.content)
                media_post = client.media_post(
                    image,
                    f"image/{extension}",
                    description=alt_text,
                    file_name=f"image.{extension}",
                )
                break

        except Exception as e:
            util.warn(f"Could not generate media post for {image_path}: {e} ({i + 1}/{util.config['max_retries']})")
            sleep(util.config["retry_delay"])

    return media_post


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def command_reply(status_text: str, reply_to: dict, command = "", image_path: str | None = None, image_alt = "") -> dict:
    """Reply to a user that issued a command.

    `status_text`: the content of the reply.
    `reply_to`: the status to reply to.
    `command`: the command that was replied to.
    `image_path`: path to an image to include in the post.
    """
    if dry:
        global poll_id
        poll_id += 1
        return {
            "id": poll_id
        }

    util.debug("Replying to command...")
    status_text = f"\n{status_text}"
    size = len(status_text) + len(reply_to["account"]["username"]) + len(command) + 4
    if size > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - size + len(status_text) - 4] + "..."
        util.warn("Tried to post a command reply exceeding the character limit.")
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    status = {}

    media_post = None
    if image_path:
        media_post = get_media_post(image_path, image_alt)

    visibility = "unlisted" if reply_to["visibility"] == "public" else reply_to["visibility"]
    status = mastodon.status_reply(
        reply_to,
        status_text,
        untag=True,
        spoiler_text=util.config["command_prefix"] + command if command else None,
        media_ids=media_post,
        sensitive=bool(media_post),
        visibility=visibility
    )
    util.debug(f"Replied to command")
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def post_status(campaign_id: int, status_text: str, last_result = "", image_path: str | None = None, image_alt = "", is_story = False) -> dict:
    """Post a plain-text status, returning its data.

    `status_text`: text to populate the post with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    `image_path`: path to an image to include in the post.
    """
    util.info("Posting status")
    global status_chain, last_status

    id_str = f"\n\n#c{campaign_id}"

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) + len(id_str) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3 - len(id_str)] + "..."
        util.warn("Tried to post a status exceeding the character limit.")

    status_text += id_str

    if dry:
        global poll_id
        poll_id += 1
        status_chain = poll_id
        print("-" * DRY_SEP_LEN + f" {poll_id} " + "-" * DRY_SEP_LEN)
        print(f"{status_text}")
        print("-" * (len(str(poll_id)) + 2 + (2 * DRY_SEP_LEN)))
        return {
            "id": poll_id,
            "created_at": datetime.now(timezone.utc),
            "url": ""
        }

    media_post = None
    if image_path:
        media_post = get_media_post(image_path, image_alt)

    status = mastodon.status_post(
        status_text,
        in_reply_to_id=None,
        media_ids=media_post,
        sensitive=bool(media_post)
    )
    last_status = status
    status_chain = status["id"]
    if (
        is_story
        and story_image_thread
        and story_image_thread.exitcode is None
        and not story_image_thread.is_alive()
    ):
        story_image_thread.start()
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def post_poll(campaign_id: int, status_text: str, last_result: str, options: list[str], duration: int | None=None, is_multiple=False) -> dict:
    """Post a poll, returning its data.

    `status_text`: text to populate the poll body with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    `duration`: poll duration, or the default duration if None.
    """
    util.info("Posting poll")
    global status_chain, last_status

    if duration is None:
        duration = int(util.config["default_poll_duration"])
    duration = int(util.clamp(duration, MIN_POLL_DURATION, MAX_POLL_DURATION))

    id_str = f"\n\n#c{campaign_id}"

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) + len(id_str) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3 - len(id_str)] + "..."
        util.warn("Tried to post a poll exceeding the character limit.")

    status_text += id_str

    for i in range(len(options)):
        if len(options[i]) > POLL_OPTION_LIMIT:
            options[i] = options[i][:POLL_OPTION_LIMIT - 3] + "..."

    if dry:
        global last_poll
        global poll_id
        last_poll = options
        poll_id += 1
        status_chain = poll_id
        print("-" * DRY_SEP_LEN + f" {poll_id} " + "-" * DRY_SEP_LEN)
        print(f"{status_text}")
        for i, option in enumerate(options):
            print(f"{i + 1}: {option}")
        print("-" * (len(str(poll_id)) + 2 + (2 * DRY_SEP_LEN)))
        return {
            "id": poll_id,
            "poll": {},
            "url": "",
            "created_at": str(datetime.now(timezone.utc))
        }

    poll = mastodon.make_poll(options, duration, multiple=is_multiple)
    status = mastodon.status_post(
        status_text,
        poll=poll,
        in_reply_to_id=None
    )
    last_status = status
    status_chain = status["id"]

    if (
        story_image_thread
        and story_image_thread.exitcode is None
        and not story_image_thread.is_alive()
    ):
        story_image_thread.start()
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def delete_post(status_id: int) -> dict:
    """Delete the given post, returning its data.

    `status_id`: ID of the status to delete.
    """
    global status_chain, last_status

    if dry:
        if status_chain == status_id:
            status_chain = poll_id - 1

        return {
            "id": poll_id
        }

    status = mastodon.status_delete(status_id)
    last_status = None
    if status["id"] == status_chain:
        status_chain = status["in_reply_to_id"]

    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def poll_result(status_id: int, chain = True, option_groups: list | None = None, dry_skip: list[int] | None = None, multiple: int = 1) -> tuple[list[int], int, dict] | None:
    """Get poll results from the post with given ID.
    This is a blocking call and will wait until the poll is expired.

    If the queried poll allows for multiple votes, `option_groups` is ignored.

    `status_id`: ID of the status to query.
    `chain`: whether to chain (reply to) the previous status (applicable only if the poll is remade).
    `option_groups`: list of poll indexes to group and count together before individually.
    `dry_skip`: indices to not choose when running `--dry`
    `multiple`: how many poll options to pick

    Returns a tuple of (winning vote indices, number of votes, status)
    """
    global status_chain, last_status

    if dry:
        vote_int = 0
        if auto:
            ranges = []
            for i in range(len(last_poll)):
                if dry_skip is None or i not in dry_skip:
                    ranges.append(i)
            vote_int = random.choice(ranges) + 1
        else:
            while vote_int == 0:
                vote = input(f"Vote [1-{len(last_poll)}]: ")
                try:
                    vote_int = int(vote)
                except ValueError:
                    vote_int = 0
        return ([vote_int - 1], 1, {"id": 0, "url": ""})

    util.debug("Waiting for poll to end...")
    sleep(util.config["default_poll_duration"] + 1)
    status = mastodon.status(status_id)

    if "poll" not in status:
        raise ValueError("Status does not contain a poll.")

    # update status info
    util.debug("Probing poll...")
    poll = mastodon.poll(status["poll"]["id"])

    # probe poll
    if not poll["expired"]:
        diff = poll["expires_at"] - datetime.now(timezone.utc)
        sleep(max(1, diff.total_seconds() + 1))

    poll = mastodon.poll(status["poll"]["id"])

    # No votes yet, remake poll for extended time
    if poll["votes_count"] <= 0:
        delete_post(status_id)
        return None

    # gather results
    util.debug("Polling results...")

    if option_groups is None:
        option_groups = []
    votes: list[int] = [result["votes_count"] for result in poll["options"]]

    # Add any index not in a group as its own group
    for i in range(len(votes)):
        if not util.int_in_array(option_groups, i):
            option_groups.append(i)

    option_groups = util.sort_multi(option_groups) # allows us to prefer lower index items on tie
    vote_order = util.flatten_array(sort_groups(votes, option_groups))
    # we only want results that are actually voted for
    # otherwise, multi-option polls return 0-vote options, too
    vote_order = [v for v in vote_order if votes[v] > 0]
    return (vote_order[:multiple], sum(votes), status)


def find_winning_index(votes: list[int], groups: list | None = None):
    """Find the winning index of an array of votes, given an arbitrarily nested
    int array of groups. For example, `[0, [1, [2, 3]]]` will first find a
    winner between `votes[0]` and `votes[1] + votes[2] + votes[3]`, then between
    `votes[1]` and `votes[2] + votes[3]` and so on. If a vote index is not
    present in groups, it will *not* be counted at all. Indices may be repeated.

    In the event of a tie, preference is given to the group at the lowest index.
    To prefer the lowest vote index, use `util.sort_multi` on `groups`.

    If `groups` is not provided, this function simply returns the vote index
    with the largest value.

    `votes`: list of vote counts.
    `groups`: arbitrarily nested vote indices to group together.
    """
    return util.flatten_array(sort_groups(votes, groups))[0]


def sort_groups(votes: list[int], groups: list | None = None):
    """Sorts the groups by their vote counts, with the "winner" being at index 0.
    `votes`: list of vote counts.
    `groups`: arbitrarily nested vote indices to group together.
    """
    if groups is None:
        groups = [i for i in range(len(votes))]
        groups.sort(key = lambda x: votes[x], reverse = True)
        return groups

    group_values = []
    groups_copy = deepcopy(groups)

    for i, group in enumerate(groups):
        if isinstance(group, list):
            sum_array = util.sum_array(group, lambda g: votes[g])
            group_values.append({ "index": i, "value": sum_array })
        else:
            group_values.append({ "index": i, "value": votes[group] })

    group_values.sort(key = lambda x: x["value"], reverse = True)

    for i, group in enumerate(groups):
        if isinstance(group, list):
            groups_copy[i] = sort_groups(votes, group)

    return [groups_copy[g["index"]] for g in group_values]


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_followers() -> set[str]:
    """Get the list of follower names.
    """
    followers: set[str] = set()
    if dry:
        followers = set()
        for _ in range(100):
            followers.add("".join(random.choices(string.printable, k=random.randint(1, 16))))
        return followers

    all_followers = mastodon.fetch_remaining(mastodon.account_followers(mastodon.me()))
    return {f["display_name"] for f in all_followers if str(f["id"]) not in util.blacklist["b"]}


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def update_profile(bio: str) -> dict:
    """Update the user profile based upon the Character description.

    `bio`: the string to update the bot bio with.
    """
    if dry:
        print("-" * DRY_SEP_LEN + f" {poll_id} Bio " + "-" * DRY_SEP_LEN)
        print(bio)
        print("-" * (len(str(poll_id)) + 6 + (2 * DRY_SEP_LEN)))
        return {
            "note": "This is a bio"
        }

    return mastodon.account_update_credentials(note=bio)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_supporters() -> set[str]:
    """Get a list of supporter first names.
    """
    if not util.config["supporter_url"]:
        util.debug("No supporter_url provided, skipping...")
        return set()

    result = requests.get(util.config["supporter_url"])
    json: list[str] = result.json()
    filtered: set[str] = set()
    for j in json:
        name = j.split(" ")[0]
        name = "".join([n for n in name if n.isalpha()])
        if len(name) >= 2:
            filtered.add(name.title())
    return filtered


def get_supporter_name() -> str:
    """Get a random supporter name, depending on supporter count.

    This has a chance to return an empty string.
    """
    names = get_supporters()

    # We base name chance on the amount of supporters since the amount of
    # supporters could be very low and we don't want a novel name to
    # be the same name over and over
    to_name = random.random() <= util.clamp(len(names) * 0.05, 0, 0.75)
    if to_name and len(names) > 0:
        return random.choice(list(names))

    return ""

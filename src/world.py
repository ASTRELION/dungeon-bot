import random


class World:

    def __init__(self) -> None:

        self.location = Location()
        self.weather = Weather()


    def reroll_location(self) -> "Location":
        """Choose a new random location.
        """
        self.location = Location()
        return self.location


    def reroll_weather(self) -> "Weather":
        """Choose a new random weather.
        """
        self.weather = Weather()
        return self.weather


class Location:

    POSSIBLE_LOCATIONS = [
        "mountains",
        "jungle",
        "bustling medieval city",
        "open plains",
        "swamp",
        "tiny fantasy village",
        "forest",
        "rolling hills",
        "empty desert",
        "rocky fields",
        "lakeside",
        "seaside",
        "deep cavern",
    ]


    """World Location
    """
    def __init__(self, location: str | None = None) -> None:

        if location is None:
            self.location = random.choice(Location.POSSIBLE_LOCATIONS)
        else:
            self.location = location


    @staticmethod
    def none() -> "Location":

        return Location("none")


    def is_none(self) -> bool:

        return self.location == "none"


    def __call__(self) -> str:

        return self.location


    def __str__(self) -> str:

        return self.location


class Weather:

    POSSIBLE_WEATHER = [
        "clear weather",
        "raining",
        "misty",
        "snowing",
        "cloudy",
        "overcast",
        "raging thunderstorm",
        "windy",
    ]


    """World Weather
    """
    def __init__(self, weather: str | None = None) -> None:

        if weather is None:
            self.weather = random.choice(Weather.POSSIBLE_WEATHER)
        else:
            self.weather = weather


    @staticmethod
    def none() -> "Weather":

        return Weather("none")


    def is_none(self) -> bool:

        return self.weather == "none"


    def __call__(self) -> str:

        return self.weather


    def __str__(self) -> str:

        return self.weather

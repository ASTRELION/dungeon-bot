from enum import Enum


class State(Enum):
    END = "end"
    BUILD = "build"
    ADVENTURE = "adventure"
    FIGHT = "fight"
    TOWN = "town"
    SHOP = "shop"
    ADVENTURE_MONSTER = "adventure_monster"
    ADVENTURE_STRANGER = "adventure_stranger"
    ADVENTURE_CHEST = "adventure_chest"
    TRADER = "trader"
    TREASURE = "treasure"


class Rarity(Enum):

    VARIES = "varies"
    COMMON = "common"
    UNCOMMON = "uncommon"
    RARE = "rare"
    VERY_RARE = "very_rare"
    LEGENDARY = "legendary"


class Dimensions():

    SQUARE = (1024, 1024)       # 1:1

    WIDE = (1280, 768)          # 5:3
    ULTRA_WIDE = (1536, 512)    # 3:1

    TALL = (768, 1280)          # 3:5
    ULTRA_TALL = (512, 1536)    # 1:3


MONSTER_SUBTYPES = {
    "any race": [
        "human",
        "elf",
        "half-elf",
        "dwarf",
        "tiefling",
        "halfling",
        "gnome",
        "orc",
        "half-orc",
    ]
}

MONSTER_TYPE_MODIFIERS = {
    "construct": [
        "azure",
        "slate",
        "purple",
        "ochre",
        "green",
        "obsidian",
    ],
    "tiny beasts": [
        "",
        "",
        "",
        "brown",
        "black",
        "gray",
        "spotted",
    ],
    "beast": [
        "",
        "",
        "",
        "brown",
        "black",
        "gray",
        "spotted",
        "striped",
        "white",
    ],
    "plant": [
        "",
        "green",
        "dark green",
        "light green",
        "yellow",
        "orange brown",
        "crimson",
        "lila",
        "violet",
    ],
    "fey": [
        "",
        "rainbow",
        "purple",
        "pink",
        "surreal",
        "flickering",
        "virbrant colors",
        "blue",
        "yellow",
    ],
    "dragon": [
        "",
        "chrome",
        "red",
        "blue",
        "green",
        "yellow",
        "black",
        "white",
    ],
    "undead": [
        "",
        "black",
        "gray",
        "crimson",
        "mossy",
        "yellow",
        "reinforced",
    ],
}

GENDERS = [
    "Male",
    "Female",
    "androgynous, gender-neutral",
]

CULTURES = [
    "", # Leave empty to get the generic fantasy culture sometimes
    "",
    "Roman",
    "Byzantine",
    "Summerian (Akkadian Empire)",
    "Babylonian",
    "Assyrian",
    "Spartan",
    "Medieval German",
    "Celtic",
    "Zulu",
    "Namibian",
    "Sahelian",
    "Wagadu Ghanese",
    "Songhai",
    "Ancient Egyptian",
    "Phoenician",
    "Jolof",
    "Kanem",
    "Mali Empire",
    "Mongolian",
    "Greek (Seleucid Empire)",
    "Medieval Arabian",
    "Frankish",
    "Viking",
    "Chinese (Han Dynasty)",
    "Chinese (Three Kingdoms Period)",
    "Chinese (Six Dynasties Period)",
    "Minoan",
    "Japanese (Edo Period)",
    "Japanese (Muromachi Period)",
    "Mayan",
    "Incan",
    "Aztec",
    "Cherokee",
    "Persian (Achaemenid Empire)",
    "Nabatean",
    "Hindu (Vijayanagaran Period)",
    "Carthaginian",
    "Cambodian (Khmer Empire)",
    "Tibetan",
    "Rashidun",
    "Ottoman",
    "Macedonian",
    "Hunnic",
    "Indian (Delhi Sultanate)",
    "Arthurian English",
    "Scythian",
    "Visigoth",
    "Bulgarian",
    "Kievan Rus",
    "Srivijaya",
    "Polynesian",
    "Indonesian",
    "Isreali (Solomon period)",
    "Medieval Lithuanian",
    "Medieval Polish",
    "Medieval Papal",
    "Medieval Spanish",
    "Romania (Wallachia Kingdom)",
    "Aboriginal Australian",
    "Kushite",
    "Numidian",
    "Barghawatan",
    "Mamluk",
]

# We should probably keep this in regard to lightness/darkness since actual
# skin-tones likely won't always translate to non-human races
COMPLEXIONS = [
    "light",
    "medium",
    "dark",
]

RACE_ADJECTIVES = {
    "dwarf": [
        "bearded",
        "bulky",
    ],
    "half-orc": [
        "big prominent teeth",
        "scars",
        "hulking",
    ],
    "tiefling": [
        "horned",
    ],
    "gnome": [
        "excited",
        "slender",
    ],
    "halfling": [
        "small",
    ],
    "elf": [
        "slender",
        "graceful",
    ],
    "dragonborn": [
        "humanoid lizard",
        "humanoid dragon",
    ],
}

CLOTHES_COLORS = [
    "blue",
    "crimson",
    "red",
    "ochre",
    "purple",
    "yellow",
    "green",
    "khaki",
    "white",
    "gold",
    "silver",
    "brown",
    "black",
    "pink",
    "gray",
    "cyan",
]

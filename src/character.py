import copy
import math
import random
from collections import namedtuple
from attacks import Attack, AttackRange, AttackResult, AttackSpell, AttackSummary, AttackWeapon

import client
from constants import CLOTHES_COLORS, COMPLEXIONS, CULTURES, GENDERS
from dice import Dice
from creature import Creature, LongRestResult, ShortRestResult
from currency import Currency, Unit
from dnd.clazz import Class
import dnd.dndclient as dndclient
from dnd.race import Race
from dnd.spell import Spell
from dnd.subclass import Subclass
from dnd.subrace import Subrace
from effect import Effect, EffectEnd, EffectMod, EffectStat, EffectType
from dnd.feature import Feature
import lang
import util
from dnd.equipment import Equipment, UNARMED_EQUIPMENT
from item import Item
from dnd.magic_item import PotionOfFlying, PotionOfHealing, PotionOfGiantStrength, PotionOfHeroism, PotionOfInvisibility, PotionOfResistance, PotionOfSpeed


class Clothes():

    def __init__(self, primary = "", secondary = "") -> None:

        self.primary = primary
        self.secondary = secondary


class Character(Creature):
    """Describes a player Character.
    """
    GOLD_ROLL = Dice("3d4")

    def __init__(
        self,
        campaign_id: int,
        name: str,
        race: dndclient.DNDResponse | None = None,
        clazz: dndclient.DNDResponse | None = None,
        subrace: dndclient.DNDResponse | None = None,
        subclass: dndclient.DNDResponse | None = None
    ) -> None:
        """Create a new D&D character with a random race, class, and stats.

        `name`: name of the Character.
        `race`: race dict of the Character.
        `clazz`: class dict of the Character.
        """
        super().__init__(name.title())
        util.info(f"Creating character '{name}'")

        self.campaign_id = campaign_id

        self.race: Race
        # Random race
        if race is None:
            races = dndclient.get_races()
            self.race = Race(dndclient.get_race(random.choice(races)["index"]))
        else:
            self.race = Race(race)

        self.clazz: Class
        # Random class
        if clazz is None:
            classes = dndclient.get_classes()
            self.clazz = Class(dndclient.get_class(random.choice(classes)["index"]))
        else:
            self.clazz = Class(clazz)

        self.subrace = None
        if subrace is None and len(self.race.subraces) > 0:
            if random.random() < util.config["subrace_chance"]:
                self.subrace = Subrace(dndclient.get_subrace(random.choice(self.race.subraces)["index"]))
        elif subrace is not None:
            self.subrace = Subrace(subrace)

        self.subclass = None
        if subclass is None and len(self.clazz.subclasses) > 0:
            if random.random() < util.config["subclass_chance"]:
                self.subclass = Subclass(dndclient.get_subclass(random.choice(self.clazz.subclasses)["index"]))
        elif subclass is not None:
            self.subclass = Subclass(subclass)

        self.hands: list[Equipment] = []
        self.armor: list[Equipment] = []

        self.currency = Currency(0)

        self.class_specific: dict = {}

        # Level
        self.level = 1
        self.hit_dice = 1
        self.xp = 0

        self.stats = {
            "kills": 0
        }

        self.has_death_protection = True

        self.complexion = random.choice(COMPLEXIONS)
        self.culture = random.choice(CULTURES)
        self.gender = random.choice(GENDERS)
        self.clothes = Clothes(
            random.choice(CLOTHES_COLORS),
            random.choice(CLOTHES_COLORS)
        )
        while self.clothes.secondary == self.clothes.primary:
            self.clothes.secondary = random.choice(CLOTHES_COLORS)


    def init(self) -> None:
        """Initialize the Character with data.
        """
        util.info("Initializing character")

        # Roll for ability scores
        rolls = []
        for _ in range(len(self.abilities)):
            rolls.append(Dice("1d6").roll(4, highest=3))

        rolls.sort()
        for ability in self.ability_priority():
            self.abilities[ability] = rolls.pop()

        # assign the rest randomly
        for ability in self.abilities:
            if self.abilities[ability] == 0:
                self.abilities[ability] = random.choice(rolls)
                rolls.remove(self.abilities[ability])

        # Apply racial ability bonuses
        for bonus in self.race.ability_bonuses:
            ability = bonus["ability_score"]["index"]
            self.abilities[ability] += bonus["bonus"]

        # Apply subrace ability bonuses
        if self.subrace:
            for bonus in self.subrace.ability_bonuses:
                ability = bonus["ability_score"]["index"]
                self.abilities[ability] += bonus["bonus"]

        # Apply ability constraints
        for ability in self.abilities:
            self.abilities[ability] = min(20, self.abilities[ability])

        # Class proficiencies
        for p in self.clazz.proficiencies:
            self.add_proficiency(p["index"])

        # Race proficiencies
        for p in self.race.starting_proficiencies:
            self.add_proficiency(p["index"])

        # Subrace proficiencies
        if self.subrace:
            for p in self.subrace.starting_proficiencies:
                self.add_proficiency(p["index"])

        # Choice (class) proficiencies
        prof_choice = self.clazz.proficiency_choices[0]
        amount = prof_choice["choose"]
        choices: list = prof_choice["from"]["options"]
        for _ in range(amount):
            choice = random.choice(choices)
            choices.remove(choice)
            self.add_proficiency(choice["item"]["index"])

        # Add racial traits
        for trait in self.race.traits:
            self.add_trait(trait["index"])

        # Add subracial traits
        if self.subrace:
            for trait in self.subrace.racial_traits:
                self.add_trait(trait["index"])

        # Subclass features
        if self.subclass:
            level_dict = dndclient.get_subclass_level(self.subclass.index, 1)
            if level_dict:
                for feature in level_dict["features"]:
                    self.add_feature(feature["index"])

        weapon_options: list[dndclient.DNDResponse] = []
        armor_options: list[dndclient.DNDResponse] = []

        def add_equipment_option(equipment_index):
            equipment_item = dndclient.get_equipment(equipment_index)
            if equipment_item["equipment_category"]["index"] == "weapon" and "damage" in equipment_item:
                weapon_options.append(equipment_item)
            elif equipment_item["equipment_category"]["index"] == "armor" and equipment_item["armor_category"].lower() != "shield":
                armor_options.append(equipment_item)
            else:
                util.debug("{} not a valid weapon or armor".format(equipment_index))

        def add_equipment_by_category(category):
            equipment = dndclient.get_equipment_category(category)["equipment"]
            for e in equipment:
                add_equipment_option(e["index"])

        def add_equipment_for_options(options):
            for o in options:
                if o["option_type"] == "choice":
                    add_equipment_by_category(o["choice"]["from"]["equipment_category"]["index"])

                elif o["option_type"] == "multiple":
                    for t in o["items"]:
                        if t["option_type"] == "counted_reference":
                            add_equipment_option(t["of"]["index"])

                        elif t["option_type"] == "choice":
                            add_equipment_by_category(t["choice"]["from"]["equipment_category"]["index"])

                elif o["option_type"] == "counted_reference":
                    add_equipment_option(o["of"]["index"])

        # Add all base starting weapons
        for e in self.clazz.starting_equipment:
            add_equipment_option(e["equipment"]["index"])

        for option in self.clazz.starting_equipment_options:

            if (option["from"]["option_set_type"] == "equipment_category"):
                add_equipment_by_category(option["from"]["equipment_category"]["index"])

            elif (option["from"]["option_set_type"] == "options_array"):
                add_equipment_for_options(option["from"]["options"])

        self.hands = []
        self.armor = []
        if weapon_options:
            self.equip_item(Equipment(random.choice(weapon_options)))
        if armor_options:
            self.equip_item(Equipment(random.choice(armor_options)))

        self.currency = Currency(util.config["start_gold"] + (self.GOLD_ROLL.roll() * 10), Unit.gp)

        # TODO: temporary, remove constant 12 when low hitpoints are more viable
        self.set_max_hitpoints(12 + self.ability_modifier("con"))
        self.hitpoints = self.max_hitpoints

        self.walk_speed = self.race.speed

        # Level (1) specific
        class_level = dndclient.get_class_level(self.clazz.index, self.level)

        self.class_specific = dict(class_level["class_specific"])
        self.proficiency_bonus = int(class_level["prof_bonus"])

        for feature in class_level["features"]:
            self.add_feature(feature["index"])

        # Spellcasters
        if "spellcasting" in class_level:
            self.spell_ability = str(self.clazz.spellcasting["spellcasting_ability"]["index"])

            if "cantrips_known" in class_level["spellcasting"]:
                self.spell_slots[0] = dndclient.SpellSlot(0, cantrip=True)

            for spellcast in class_level["spellcasting"]:
                # populate spell slots
                if spellcast.startswith("spell_slots_level_"):
                    level = int(spellcast[-1])
                    self.spell_slots[level] = dndclient.SpellSlot(level, class_level["spellcasting"][spellcast])

            self.learn_spells()

        # Languages
        for language in self.race.languages:
            self.add_language(language["index"])

        # Subrace languages
        if self.subrace:
            for language in self.subrace.languages:
                self.add_language(language["index"])

            if self.subrace.language_options:
                choose_count = int(self.subrace.language_options["choose"])
                options = list(self.subrace.language_options["from"]["options"])
                for _ in range(choose_count):
                    choice = random.choice(options)
                    options.remove(choice)
                    self.add_language(choice["item"]["index"])

        self.size = self.race.size
        self.alignment = self.race.alignment

        util.info(f"Created Character {self}")


    def unknown_spells(self) -> list[list[Spell]]:
        """Get a list of unknown spells. Each index is a usable spell level.
        """
        spells: list[list[Spell]] = [[] for _ in range(10)]
        for slot in self.spell_slots:
            if not slot.can_use():
                continue

            all_spells = dndclient.get_class_spells(self.clazz.index, slot.level)
            for spell in all_spells:
                if not any(spell["index"] == s.index for s in self.spells[slot.level]):
                    s = dndclient.get_spell(spell["index"])
                    spells[slot.level].append(Spell(s))

        return spells


    def learn_spells(self) -> tuple:
        """Learn spells if any are left to learn, and gain spell slots if applicable.

        Learning spells is random, but spells with damage have a higher chance of being
        learned.
        """
        class_level = dndclient.get_class_level(self.clazz.index, self.level)
        spells_learned: list[Spell] = []
        slots_gained = [0] * len(self.spell_slots)
        new_slots_gained: set[int] = set()

        if "spellcasting" not in class_level:
            return (spells_learned, slots_gained)
        spellcasting: dict[str, int] = dict(class_level["spellcasting"])

        # if we know no spells, we assume all slots are gained for the first time
        if self.spells_known["spells"] == 0:
            for spell_slot in self.spell_slots:
                if spell_slot.max_charges > 0:
                    new_slots_gained.add(spell_slot.level)

        # update spell slots
        for spellcast in spellcasting:
            if spellcast.startswith("spell_slots_level_"):
                s = int(spellcast[-1])
                old_max = self.spell_slots[s].max_charges
                self.spell_slots[s].max_charges = spellcasting[spellcast]
                slots_gained[s] = spellcasting[spellcast] - old_max
                current = self.spell_slots[s].current_charges
                self.spell_slots[s].current_charges = min(current, spellcasting[spellcast])
                if old_max == 0 and self.spell_slots[s].max_charges > 0:
                    new_slots_gained.add(s)
                    util.debug(f"Gained slot {s} for the first time")

        # amount of spells available
        unknown_spells = self.unknown_spells()
        unknown_spell_count = sum([len(u) for u in unknown_spells])

        # give spells with damage higher probability
        weights: list[list[float]] = [[] for _ in range(len(unknown_spells))]
        for i, slot in enumerate(unknown_spells):
            weights[i] = [1] * len(slot)
            slot.sort(key=lambda s: bool(s.damage), reverse=True)
            k = 0 # number of damaging spells
            for j, unknown_spell in enumerate(slot):
                if not bool(unknown_spell.damage):
                    k = j
                    break
            non_damage_count = len(slot) - k
            if k > 0:
                weights[i][:k] = [non_damage_count / k] * k

        cantrip_count = int(spellcasting["cantrips_known"]) if "cantrips_known" in spellcasting else 0
        spell_count = int(spellcasting["spells_known"]) if "spells_known" in spellcasting else 0

        # classes with special spell count features
        if self.has_feature("spellcasting-druid") or self.has_feature("spellcasting-cleric"):
            spell_count = max(1, self.level + self.ability_modifier("wis"))
        elif self.has_feature("spellcasting-paladin"):
            spell_count = max(1, math.floor(self.level / 2) + self.ability_modifier("cha"))
        elif self.has_feature("spellcasting-wizard"):
            spell_count = 6 + ((self.level - 1) * 2)

        # amount of spells to learn
        cantrip_learn_count = min(len(unknown_spells[0]), cantrip_count - self.spells_known["cantrips"])
        spell_learn_count = min(unknown_spell_count, spell_count - self.spells_known["spells"])

        temp_unknown = copy.deepcopy(unknown_spells)
        # guarantee a damage cantrip, if possible
        if self.spells_known["cantrips"] == 0 and cantrip_learn_count > 0:
            cantrip = None
            while cantrip is None and cantrip_learn_count > 0:
                cantrip = random.choice(temp_unknown[0])
                temp_unknown[0].remove(cantrip)
                if not cantrip.damage:
                    cantrip = None
                    continue
            if cantrip is not None:
                i = unknown_spells[0].index(cantrip)
                weights[0].pop(i)
                unknown_spells[0].pop(i)
                cantrip_learn_count -= 1
                self.spells[0].add(cantrip)
                spells_learned.append(cantrip)
                util.debug(f"Learned new cantrip spell {cantrip.name}")

        # guarantee a damage spell for each new slot level, if possible
        for new_slot in new_slots_gained:
            spell = None
            while spell is None and spell_learn_count > 0 and len(temp_unknown[new_slot]) > 0:
                spell = random.choice(temp_unknown[new_slot])
                temp_unknown[new_slot].remove(spell)
                if not spell.damage or spell.index == "sleep":
                    spell = None
                    continue
            if spell is None:
                continue
            i = unknown_spells[new_slot].index(spell)
            weights[new_slot].pop(i)
            unknown_spells[new_slot].pop(i)
            spell_learn_count -= 1
            self.spells[new_slot].add(spell)
            spells_learned.append(spell)
            util.debug(f"Learned new slot spell {spell.name}")

        # learn cantrips
        for _ in range(cantrip_learn_count):
            if not unknown_spells[0]:
                continue
            cantrip = None
            while not cantrip and unknown_spells[0]:
                cantrip = random.choices(unknown_spells[0], weights=weights[0], k=1)[0]
                i = unknown_spells[0].index(cantrip)
                weights[0].pop(i)
                unknown_spells[0].pop(i)

            if not cantrip:
                continue
            self.spells[0].add(cantrip)
            spells_learned.append(cantrip)
            util.debug(f"Learned {cantrip.name}")

        # learn spells
        levels = [s.level for s in self.spell_slots[1:] if s.can_use()]
        for _ in range(spell_learn_count):
            level = None
            spell = None
            while not spell:
                levels = [l for l in levels if len(unknown_spells[l]) > 0]
                if not levels:
                    break
                level = random.choice(levels)
                spell = random.choices(unknown_spells[level], weights=weights[level], k=1)[0]
                i = unknown_spells[level].index(spell)
                weights[level].pop(i)
                unknown_spells[level].pop(i)

            if not spell or not level:
                continue
            self.spells[level].add(spell)
            spells_learned.append(spell)
            util.debug(f"Learned {spell.name}")

        self.spells_known["cantrips"] = cantrip_count
        self.spells_known["spells"] = spell_count
        return (spells_learned, slots_gained)


    def ability_priority(self) -> list[str]:
        """Get a list of abilities in order of best-assumed priority for this Character.

        Note: the purpose of this function is NOT to get the objectively "best" ability priority,
        but rather an ability priority that would be realistic for the Character while still
        introducing randomized elements for variety.
        """
        priority: dict = {}

        # prefer primary ability scores, reflected in multi-classing requirements
        multi_classing = self.clazz.multi_classing
        if "prerequisites" in multi_classing:
            for prereq in multi_classing["prerequisites"]:
                priority.setdefault(prereq["ability_score"]["index"])
        if "prerequisite_options" in multi_classing:
            options = [p["ability_score"]["index"] for p in multi_classing["prerequisite_options"]["from"]["options"]]
            random.shuffle(options)
            for prereq in options:
                priority.setdefault(prereq)

        # then randomly-ordered saving throws
        saving_throws = [t["index"] for t in self.clazz.saving_throws]
        random.shuffle(saving_throws)
        for saving_throw in saving_throws:
            priority.setdefault(saving_throw)

        # then randomly-ordered con, dex, and str
        condexstr = ["con", "dex", "str"]
        random.shuffle(condexstr)
        for ability in condexstr:
            priority.setdefault(ability)

        # then randomly-ordered abilities with racial bonus
        bonuses = [b["ability_score"]["index"] for b in self.race.ability_bonuses]
        random.shuffle(bonuses)
        for bonus in bonuses:
            priority.setdefault(bonus)

        return list(priority)


    def proficient_weapons(self) -> list[Equipment]:
        """Get a list of all weapons usable by the Character.
        """
        weapons: list[Equipment] = []
        for proficiency in self.proficiencies:
            if proficiency.type == "weapons":
                category = dndclient.get_equipment_category(proficiency.reference["index"])
                if category:
                    for w in category["equipment"]:
                        weapon_dict = dndclient.get_equipment(w["index"])
                        weapon = Equipment(weapon_dict)
                        if Equipment.is_valid(weapon_dict) and weapon.is_weapon()\
                                and all(w.index != weapon.index for w in weapons):
                            weapons.append(weapon)
                else:
                    weapon_dict = dndclient.get_equipment(proficiency.reference["index"])
                    weapon = Equipment(weapon_dict)
                    if Equipment.is_valid(weapon_dict) and weapon.is_weapon()\
                            and all(w.index != weapon.index for w in weapons):
                        weapons.append(weapon)

        return weapons


    def proficient_armor(self) -> list[Equipment]:
        """Get a list of all armor usable by the Character.
        """
        armors: list[Equipment] = []
        for proficiency in self.proficiencies:
            if proficiency.type == "armor":
                category = dndclient.get_equipment_category(proficiency.reference["index"])
                if category:
                    for a in category["equipment"]:
                        armor_dict = dndclient.get_equipment(a["index"])
                        armor = Equipment(armor_dict)
                        if Equipment.is_valid(armor_dict) and armor.is_armor() and all(a.index != armor.index for a in armors):
                            armors.append(armor)
                else:
                    armor = Equipment(dndclient.get_equipment(proficiency.reference["index"]))
                    if armor.is_armor() and all(armor.index != a.index for a in armors):
                        armors.append(armor)

        return armors


    def armor_class(self, armor: list[Equipment] | None = None) -> int:
        """Get the effective Armor Class of the Character. Sums both armor and shields.

        `armor`: calculate Armor Class as if the Character was wearing the given armor dict.
        """
        if armor is None:
            armor = self.armor

        ac = 10
        # (mostly) spell casters
        # TODO: remove this when spell casters are more viable
        if all(p.type != "armor" for p in self.proficiencies):
            ac = 12

        ac += self.ability_modifier("dex")

        # monk feature
        if self.has_feature("monk-unarmored-defense")\
                and all(not h.is_shield() for h in self.hands):
            ac = 10 + self.ability_modifier("dex") + self.ability_modifier("wis")
        # barbarian feature
        elif self.has_feature("barbarian-unarmored-defense"):
            ac = 10 + self.ability_modifier("dex") + self.ability_modifier("con")

        # armor
        if armor:
            ac = 0
            for a in armor:
                armor_ac = a.armor_class
                if a.armor_class_dex:
                    if a.armor_class_max_dex > 0:
                        armor_ac += min(a.armor_class_max_dex, self.ability_modifier("dex"))
                    else:
                        armor_ac += self.ability_modifier("dex")
                ac += armor_ac

        # shield
        for hand in self.hands:
            if hand.is_shield():
                shield_ac = hand.armor_class
                if hand.armor_class_dex:
                    if hand.armor_class_max_dex > 0:
                        shield_ac += min(hand.armor_class_max_dex, self.ability_modifier("dex"))
                    else:
                        shield_ac += self.ability_modifier("dex")
                ac += shield_ac

        return Effect.apply_effects(ac, [e for e in self.effects if e.effect_stat == EffectStat.ARMOR_CLASS])


    def attack_count(self) -> int:
        """Get the number of attacks this Character can perform in a single turn.
        """
        action_count_effects = [e for e in self.effects if e.effect_stat == EffectStat.ACTION_COUNT]
        count = Effect.apply_effects(1, action_count_effects)
        return count


    def weapon_dice_str(self, weapon: Equipment, dice: Dice) -> str:
        """Get the damage dice string for the given weapon, e.g. '2d4+2, +3'

        `weapon`: weapon dict
        `dice`: damage dice to use
        """
        return "{}{:+}, {:+}".format(
            str(dice),
            self.weapon_damage_modifier(weapon),
            self.weapon_attack_modifier(weapon) + self.proficiency_bonus
        )


    def spend(self, currency: Currency) -> Currency:
        """Spend the given amount of gold.

        `currency`: Currency for the amount of gold to spend.
        """
        assert isinstance(currency, Currency)
        self.currency -= currency
        return self.currency


    def earn(self, currency: Currency) -> Currency:
        """Earn the given amount of gold.

        `currency`: Currency for the amount of gold to spend.
        """
        assert isinstance(currency, Currency)
        self.currency += currency
        return self.currency


    def equip_equipment(self, equipment: Equipment, replace = False, dry = False) -> list[Equipment]:
        """Equip the given equipment (armor or weapon). Returns unequipped equipment if applicable.

        `equipment`: equipment dict to equip.
        `replace`: True to override any currently equipped equipment if needed (this will try to
        be as smart as possible)
        `dry`: True to not actually equip, only return what would be replaced, if any.
        """
        if equipment.is_weapon() or equipment.is_shield():
            # No weapons exist, just equip
            if len(self.hands) == 0:
                if not dry:
                    self.hands.append(equipment)
                    self.hands.sort(key=lambda x: x.damage_dice.max(), reverse=True)

            # 2 light weapons, or 1 shield and 1 one-handed weapon
            elif len(self.hands) == 1:
                if ((self.hands[0].is_shield() and equipment.is_shield()) or \
                        self.hands[0].has_property("light") and equipment.has_property("light")) or \
                        (not self.hands[0].has_property("two-handed") and equipment.is_shield()) or \
                        (self.hands[0].is_shield() and not equipment.has_property("two-handed")):
                    if not dry:
                        self.hands.append(equipment)
                        self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                # Special case ignoring `replace`, we don't want to not have a weapon
                elif replace:
                    old_hands = copy.deepcopy(self.hands)
                    if not dry:
                        self.hands.clear()
                        self.hands.append(equipment)
                        self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                    return old_hands

            # Hands full, attempt to smartly replace current equipment
            elif len(self.hands) == 2 and replace:
                # Replace existing shield first, then the lowest damage weapon
                if equipment.is_shield():
                    min_damage_index = 0
                    min_damage = float("inf")
                    for i, hand in enumerate(self.hands):
                        if hand.is_shield():
                            min_damage_index = i
                            break
                        roll = hand.damage_dice.max()
                        if (roll <= min_damage):
                            min_damage = roll
                            min_damage_index = i
                    old_hand = self.hands[min_damage_index]
                    if not dry:
                        self.hands[min_damage_index] = equipment
                        self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                    return [old_hand]

                # Equip light weapon alongside another light weapon only
                if equipment.has_property("light"):
                    hand_to_replace = 0
                    if self.hands[0].is_shield():
                        if self.hands[1].has_property("light"):
                            hand_to_replace = 0
                        else:
                            hand_to_replace = 1
                    elif self.hands[1].is_shield():
                        if self.hands[0].has_property("light"):
                            hand_to_replace = 1
                        else:
                            hand_to_replace = 0
                    else:
                        # Choose first the weapon with the lowest damage, then lowest average damage,
                        # then lowest attack modifier
                        sorted_hands = sorted(
                            {i: h for i, h in enumerate(self.hands)}.items(),
                            key=lambda x: (
                                self.weapon_max_damage(x[1]),
                                x[1].damage_dice.mean() + self.weapon_damage_modifier(x[1]),
                                self.weapon_attack_modifier(x[1]),
                            ),
                        )
                        hand_to_replace = sorted_hands[0][0]

                    old_hand = self.hands[hand_to_replace]
                    if not dry:
                        self.hands[hand_to_replace] = equipment
                        self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                    return [old_hand]

                # Equip one-handed weapon alongside shield only
                if not equipment.has_property("two-handed"):
                    old_hands = []
                    new_hands: list[Equipment] = []
                    for hand in self.hands:
                        if hand.is_shield() and len(new_hands) < 1:
                            new_hands.append(hand)
                        else:
                            old_hands.append(hand)
                    new_hands.insert(0, equipment)
                    if not dry:
                        self.hands = new_hands
                        self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                    return old_hands

                # Anything else will replace all hands
                old_hands = copy.deepcopy(self.hands)
                if not dry:
                    self.hands.clear()
                    self.hands.append(equipment)
                    self.hands.sort(key=lambda x: self.weapon_max_damage(x), reverse=True)
                return old_hands

        elif equipment.is_armor():
            if not self.armor or replace:
                old_armor = copy.deepcopy(self.armor)
                if not dry:
                    self.armor = [equipment]
                return old_armor

        return []


    def unequip_equipment(self) -> list[Equipment]:
        """Unequips all equipment, returning each item in a list.
        """
        equipped = []
        for hand in self.hands:
            equipped.append(hand)

        for armor in self.armor:
            equipped.append(armor)

        self.hands.clear()
        self.armor.clear()
        return equipped


    def equip_item(self, item: Item, replace = False, dry = False):
        """Equip the given Item onto the Character.

        `item`: the item to equip.
        `replace`: whether to replace current equipment or not.
        `dry`: do not actually equip the item, if True.
        """
        match item:
            case Currency():
                return self.earn(item)

            case Equipment():
                return self.equip_equipment(item, replace, dry)

            case PotionOfHealing():
                return self.heal(item.use())

            case PotionOfGiantStrength():
                strength = item.use()
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.MAX,
                    EffectStat.STRENGTH,
                    strength,
                    item.duration
                ))
                return strength

            case PotionOfFlying():
                speed = item.use()
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.FLY_SPEED,
                    self.walk_speed * speed,
                    item.duration
                ))
                return speed

            case PotionOfHeroism():
                heroism = item.use()
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.TEMP_HP,
                    heroism.temp_hitpoints,
                    item.duration
                ))
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.ATTACK_ROLL,
                    heroism.bless,
                    item.duration
                ))
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.SAVING_THROW,
                    heroism.bless,
                    item.duration
                ))
                return heroism

            case PotionOfInvisibility():
                invisibility = item.use()
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.CONSTANT,
                    EffectStat.VISIBILITY,
                    not invisibility,
                    item.duration,
                    set([EffectEnd.ATTACK, EffectEnd.SPELL])
                ))
                return invisibility

            case PotionOfResistance():
                resistance = item.use()
                assert resistance
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.CONSTANT,
                    EffectStat.RESISTANCE,
                    resistance.value,
                    item.duration
                ))
                return resistance

            case PotionOfSpeed():
                speed = item.use()
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.MULT,
                    EffectStat.WALK_SPEED,
                    2,
                    item.duration
                ))
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.ARMOR_CLASS,
                    2,
                    item.duration
                ))
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.CONSTANT,
                    EffectStat.SAVING_THROW_ADVANTAGE,
                    "dex",
                    item.duration
                ))
                self.add_effect(Effect(
                    EffectType.POSITIVE,
                    EffectMod.ADD,
                    EffectStat.ACTION_COUNT,
                    1,
                    item.duration
                ))
                return speed

            case _:
                util.warn(f"Tried to equip unequippable item: {item}")

        return self


    def attack_options(self, max_count: int, target: Creature) -> list[AttackSummary]:
        """Get the list of attacks the Character can make right now.
        When possible, this function will try to provide one attack with each weapon's
        strongest attack, then to have a melee and ranged option.

        `max_count`: maximum number of attacks to return.
        """
        attacks_dict: dict[int, list[AttackSummary]] = {
            0: [],
            1: []
        }
        weapons = [hand for hand in self.hands if hand.is_weapon()]

        for i, weapon in enumerate(weapons):
            attack_modifier = self.weapon_attack_modifier(weapon)
            damage_modifier = self.weapon_damage_modifier(weapon)
            two_handed = weapon.has_property("two-handed")
            default_dice = weapon.damage_dice

            # Melee weapon
            if weapon.weapon_range == "melee":
                # One-handed attack as two-handed
                if weapon.has_property("versatile") and len(self.hands) == 1:
                    dice = weapon.two_handed_damage_dice
                    attack_weapon = AttackWeapon(
                        weapon,
                        dice,
                        weapon.two_handed_damage_type,
                        attack_modifier,
                        damage_modifier,
                        True,
                        AttackRange.MELEE,
                        2
                    )
                    attack_prompt = lang.attack_character.type.melee_two
                    attack_summary = AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

                # Regular attack
                else:
                    attack_prompt = lang.attack_character.type.melee_one
                    if two_handed:
                        attack_prompt = lang.attack_character.type.melee_two
                    attack_weapon = AttackWeapon(
                        weapon,
                        default_dice,
                        weapon.damage_type,
                        attack_modifier,
                        damage_modifier,
                        True,
                        AttackRange.MELEE,
                        2 if two_handed else 1
                    )
                    attack_summary = AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

                # Thrown attack
                if weapon.has_property("thrown"):
                    attack_weapon = AttackWeapon(
                        weapon,
                        default_dice,
                        weapon.damage_type,
                        attack_modifier,
                        damage_modifier,
                        True,
                        AttackRange.RANGED,
                        2 if two_handed else 1
                    )
                    attack_prompt = lang.attack_character.type.melee_thrown
                    attack_summary = AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

                # Improvised thrown
                else:
                    dice = Dice("1d4")
                    attack_weapon = AttackWeapon(
                        weapon,
                        dice,
                        "bludgeoning",
                        self.attack_modifier("dex"),
                        self.damage_modifier("dex"),
                        False,
                        AttackRange.RANGED,
                        2 if two_handed else 1
                    )
                    attack_prompt = lang.attack_character.type.melee_improvised
                    attack_summary = AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

            # Ranged weapon
            else:
                # Regular attack
                attack_weapon = AttackWeapon(
                    weapon,
                    default_dice,
                    weapon.damage_type,
                    attack_modifier,
                    damage_modifier,
                    True,
                    AttackRange.RANGED,
                    2 if two_handed else 1
                )
                attack_prompt = lang.attack_character.type.ranged_one
                if two_handed:
                    attack_prompt = lang.attack_character.type.ranged_two
                attack_summary = AttackSummary([attack_weapon], attack_prompt)
                attacks_dict[i].append(attack_summary)

                # Improvised melee attack
                dice = Dice("1d4")
                attack_weapon = AttackWeapon(
                    weapon,
                    dice,
                    "bludgeoning",
                    self.attack_modifier("str"),
                    self.damage_modifier("str"),
                    False,
                    AttackRange.MELEE,
                    2 if two_handed else 1
                )
                attack_prompt = lang.attack_character.type.ranged_improvised
                attack_summary = AttackSummary([attack_weapon], attack_prompt)
                attacks_dict[i].append(attack_summary)

        combined_attacks: list[AttackSummary] = []
        for attack1 in attacks_dict[0]:
            for attack2 in attacks_dict[1]:
                attack_summary = AttackSummary.combine(copy.deepcopy(attack1), copy.deepcopy(attack2))
                attack_summary.attacks = sorted(
                    attack_summary.get_attacks(),
                    key=lambda a: (
                        self.attack_rank(a, target),
                        a.max_damage() + a.damage_modifier,
                        a.mean_damage() + a.damage_modifier,
                        random.random()
                    ),
                    reverse=True
                )

                assert len(attack_summary.attacks) == 2
                # the off-hand attack should not do bonus damage, unless negative
                if attack_summary.attacks[1].damage_modifier > 0:
                    attack_summary.attacks[1].damage_modifier = 0

                prompt = lang.attack_character.type.melee_two
                if attack_summary.get_range() == AttackRange.MIXED:
                    prompt = lang.attack_character.type.mixed
                elif attack_summary.get_range() == AttackRange.RANGED:
                    prompt = lang.attack_character.type.ranged_two
                else:
                    attack_summary.emoji = util.emojis["attacks"]["melee_2h"]

                attack_summary.set_prompt(prompt)
                combined_attacks.append(attack_summary)

        for attack_summary in attacks_dict[0]:
            combined_attacks.append(attack_summary)

        for attack_summary in attacks_dict[1]:
            combined_attacks.append(attack_summary)

        # Unarmed Strike
        unarmed_attack = AttackWeapon(
            Equipment(dndclient.DNDResponse(UNARMED_EQUIPMENT)),
            Dice("1"),
            "bludgeoning",
            self.attack_modifier("str"),
            self.damage_modifier("str"),
            True,
            AttackRange.MELEE,
            0
        )
        unarmed_prompt = lang.attack_character.type.melee_unarmed
        summary = AttackSummary([unarmed_attack], unarmed_prompt)
        combined_attacks.append(summary)

        # Some attacks may be virtually identical, so remove those
        unique_attacks: list[AttackSummary] = []
        unique_prompts = set()
        attack_count = self.attack_count()

        for attack_summary in combined_attacks:
            dice_strs = []
            # construct prompt
            for i, attack in enumerate(attack_summary.get_attacks()):
                dice_str = " + ".join([str(d) for d in attack.damage_dice])
                if attack.damage_modifier != 0:
                    dice_str += "{:+}".format(attack.damage_modifier)
                if i == 0 and attack_count > 1:
                    dice_str += f" x {attack_count}"
                dice_strs.append(dice_str)

            modifier_emoji = ""
            rank = self.attack_rank(attack_summary, target)
            if rank < 0:
                modifier_emoji = str(util.emojis["prompts"]["disadvantage"])
            elif rank > 0:
                modifier_emoji = str(util.emojis["prompts"]["advantage"])
            attack_summary.prompt = attack_summary.prompt.choose(
                attack_summary.get_emoji(),
                " | ".join(dice_strs),
                modifier_emoji
            )

            # duplicate attacks
            attack_summary.attacks = ([attack_summary.attacks[0]] * attack_count) + attack_summary.attacks[1:]

            # only include unique prompts
            if attack_summary.get_prompt() not in unique_prompts:
                unique_prompts.add(attack_summary.get_prompt())
                unique_attacks.append(attack_summary)

        # Order attacks by advantage/disadvantage status, then total damage, then range
        weapon_attacks = sorted(
            unique_attacks,
            key=lambda attack: (
                self.attack_rank(attack, target),
                sum([a.max_damage() + a.damage_modifier for a in attack.get_attacks()]),
                attack.get_range() != AttackRange.MIXED,
                random.random()
            ),
            reverse=True
        )

        # Spell Attacks
        if self.spell_ability:
            cantrip_spells: list[AttackSpell] = []
            regular_spells: list[AttackSpell] = []
            for level in self.spells:
                for spell in level:

                    spell_level = spell.level
                    # find an available spell slot of level or greater
                    while spell_level < len(self.spell_slots) - 1\
                            and not self.spell_slots[spell_level].has_charge():
                        spell_level += 1

                    # filter non-damaging or no charge spells
                    slot: dndclient.SpellSlot = self.spell_slots[spell_level]
                    if (not slot.has_charge()
                        or not bool(spell.damage)
                        or spell.index == "sleep" # sleep has a damage block
                        or spell.ritual
                    ):
                        continue

                    # construct attack
                    spell_attack = AttackSpell(
                        spell,
                        self.attack_modifier(self.spell_ability),
                        self.damage_modifier(self.spell_ability),
                        slot.level,
                        self.level,
                        8 + self.ability_modifier(self.spell_ability) + self.proficiency_bonus
                    )

                    if spell_attack.slot_level == 0:
                        cantrip_spells.append(spell_attack)
                    else:
                        regular_spells.append(spell_attack)

            # dedicated spell casters should have mostly spells,
            # others should have mostly weapons
            spell_ratio = 2/3
            if self.has_feature("spellcasting-paladin") or self.has_feature("spellcasting-ranger"):
                spell_ratio = 1/3
            spell_count = min(int(max_count * spell_ratio), len(cantrip_spells) + len(regular_spells))
            attacks: list[AttackSpell] = []

            cantrip_spells = sorted(
                cantrip_spells,
                key=lambda spell: (
                    self.attack_rank(spell, target),
                    random.random()
                ),
                reverse=True
            )

            regular_spells = sorted(
                regular_spells,
                key=lambda spell: (
                    self.attack_rank(spell, target),
                    random.random()
                ),
                reverse=True
            )

            # guarantee 1 cantrip spell
            if spell_count > 0 and len(cantrip_spells) > 0:
                attacks.append(cantrip_spells[0])
                cantrip_spells = cantrip_spells[1:]
                spell_count -= 1

            # fill remaining with leveled spells, if available
            while spell_count > 0 and len(regular_spells) > 0:
                attacks.append(regular_spells[0])
                regular_spells = regular_spells[1:]
                spell_count -= 1

            # otherwise, fill remaining with more cantrips
            while spell_count > 0 and len(cantrip_spells) > 0:
                attacks.append(cantrip_spells[0])
                cantrip_spells = cantrip_spells[1:]
                spell_count -= 1

            attacks = sorted(attacks, key=lambda attack: attack.slot_level)

            spell_summaries: list[AttackSummary] = []
            for attack_spell in attacks:
                subattacks = [attack_spell]
                match attack_spell.spell.index:
                    case "magic-missile":
                        subattacks *= 3 + (attack_spell.slot_level - 1)
                    case "eldritch-blast":
                        blast_count = 1
                        if attack_spell.caster_level >= 5:
                            blast_count = 2
                        if attack_spell.caster_level >= 11:
                            blast_count = 3
                        if attack_spell.caster_level >= 17:
                            blast_count = 4
                        subattacks *= blast_count
                    case "scorching-ray":
                        subattacks *= 3 + (attack_spell.slot_level - 2)

                spell_summary = AttackSummary(list(subattacks))
                dice_str = " + ".join([str(d) for d in attack_spell.damage_dice])
                dice_str = dice_str.replace("MOD", str(attack_spell.damage_modifier))
                if len(subattacks) > 1:
                    dice_str += f" x {len(subattacks)}"
                modifier_emoji = ""
                if self.attack_rank(spell_summary, target) < 0:
                    modifier_emoji = util.emojis["prompts"]["disadvantage"]
                elif self.attack_rank(spell_summary, target) > 0:
                    modifier_emoji = util.emojis["prompts"]["advantage"]
                prompt = lang.attack_character.type.choose(
                    spell_summary.get_emoji(),
                    attack_spell.spell.name,
                    dice_str,
                    modifier_emoji,
                    option="spell"
                )
                spell_summary.set_prompt(prompt)
                spell_summaries.append(spell_summary)

            weapon_count = max_count - len(attacks)
            return spell_summaries + weapon_attacks[:weapon_count]

        return weapon_attacks[:max_count]


    def attack_rank(self, attack: Attack | AttackSummary, creature: Creature) -> int:
        """Attempts to rank an attack based on its damage type and disadvantaged status.

        `attack`: the `Attack` or `AttackSummary` to find modifier for.
        `creature`: the `Creature` to use the attack against.

        Returns the rank of the attack, higher being better.
        """
        # AttackSummary s are only ranked if every attack has a different sign
        if isinstance(attack, AttackSummary):
            rank = 0
            unique_ranks = set([util.sign(self.attack_rank(a, creature)) for a in attack.get_attacks()])
            if len(unique_ranks) == 1:
                return unique_ranks.pop()
            return 0

        rank = 0

        if creature.immune_to(attack=attack):
            rank -= 2

        if creature.resistant_to(attack=attack):
            rank -= 1

        if creature.vulnerable_to(attack=attack):
            rank += 1

        if self.has_disadvantage(attack, creature):
            rank -= 1

        return rank


    def has_disadvantage(self, attack: Attack | AttackSummary, creature: Creature) -> bool:
        """Disadvantage when fighting flying with melee, non-flying with ranged,
        or non-specialized water weapons.

        Usually melee weapons/spells will simply not be able to be used against opponents at range,
        but for the sake of this format those attacks will just have disadvantage.
        """
        # Spells only have disadvantage if vs a flier with a melee spell
        # ranged spells are assumed to always be ok
        if isinstance(attack, AttackSpell):
            if attack.is_attack:
                return (attack.range == AttackRange.MELEE and creature.is_flier() and self.get_fly_speed() <= 0)\
                    or (attack.range == AttackRange.RANGED and not creature.is_flier())

            return attack.range == AttackRange.MELEE and creature.is_flier() and self.get_fly_speed() <= 0

        # Attacks with weapons
        elif isinstance(attack, AttackWeapon):
            if creature.is_swimmer() and attack._dnd_dict.is_aquatic_weapon():
                return False

            return (not creature.is_flier() and attack.range == AttackRange.RANGED)\
                or (creature.is_flier() and attack.range != AttackRange.RANGED and self.get_fly_speed() <= 0)\
                or (creature.is_swimmer() and not attack._dnd_dict.is_aquatic_weapon())

        # Attack summaries only are considered to have disadvantage when all attacks
        # have disadvantage in the summary
        elif isinstance(attack, AttackSummary):
            return all([self.has_disadvantage(a, creature) for a in attack.get_attacks()])

        return False


    def attack(self, other: Creature, attack_summary: AttackSummary) -> AttackResult:
        """Simulate an attack against the given Creature with the given attack, returning the result.

        `other`: `Creature` to attack
        `attack_summary`: `AttackSummary` attacks to use
        """
        if self.get_surprised():
            return AttackResult([], 0, dndclient.ActionOutcome.NONE, "")
        total_hit = 0
        attack_hit = dndclient.ActionOutcome.CRIT_FAILURE.value

        def modify_damage(hit: int, attack: Attack) -> int:
            if isinstance(attack, AttackWeapon):
                hit += attack.damage_modifier

            if other.resistant_to(attack=attack):
                hit = math.ceil(hit / 2)
                util.debug(f"{other.name} resistant to {attack.damage_types}")
            if other.vulnerable_to(attack=attack):
                hit *= 2
                util.debug(f"{other.name} vulnerable to {attack.damage_types}")
            if other.immune_to(attack=attack):
                hit *= 0
                util.debug(f"{other.name} immune to {attack.damage_types}")

            # Immune damage types will still do 1 damage, since its a party of one I think this
            # should stay in since the player will not have a lot of damage types available to them
            return max(1, hit)

        # Prefer weapons that do not have disadvantage to make the primary attack
        attack_list = attack_summary.get_attacks()
        attack_range = attack_list[0].range
        attacks: list[Attack] = []
        self.recent_rolls["Attack Roll"] = []
        self.recent_rolls["Damage Roll"] = []

        util.debug(f"Character making {self.attack_count()} attacks")

        for attack in attack_list:
            prof_bonus = self.proficiency_bonus if attack.proficient else 0
            hit_check = 0
            damage_mod = 1.0
            is_dc = False

            # Spell DC attacks
            if isinstance(attack, AttackSpell) and attack.dc_type:
                is_dc = True
                result = other.saving_throw(attack.dc_type)
                if self.has_disadvantage(attack, other):
                    util.debug("Character attacking with disadvantage.")
                    result = max(result, other.saving_throw(attack.dc_type))
                util.debug(f"Spell Saving Throw: {result}/{attack.dc_value}")
                other.recent_rolls["Saving Throw"] = result
                if result > attack.dc_value:
                    damage_mod = attack.dc_modifier
            # Normal attacks
            else:
                if self.has_disadvantage(attack, other):
                    util.debug("Character attacking with disadvantage.")
                    hit_check = Dice("1d20").roll_disadvantage()
                else:
                    hit_check = Dice("1d20").roll()

            util.debug(f"Character attacking with {attack}")
            util.debug(f"Character attack roll: {hit_check} + {prof_bonus} + {attack.attack_modifier} / {other.armor_class()}")

            # spell dc attack
            if is_dc:
                if damage_mod > 0:
                    hit = sum(d.roll(custom_mod=attack.damage_modifier) for d in attack.damage_dice)
                    self.recent_rolls["Damage Roll"].append(hit)
                    total_hit += math.ceil(modify_damage(hit, attack) * damage_mod)
                    assert attack_range
                    assert attack.range
                    attack_range = AttackRange.combine(attack_range, attack.range)
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                    attacks.append(attack)

                else:
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.FAILURE.value)

            # always hit spells
            elif isinstance(attack, AttackSpell) and not attack.is_attack:
                hit = sum(d.roll(custom_mod=attack.damage_modifier) for d in attack.damage_dice)
                self.recent_rolls["Damage Roll"].append(hit)
                total_hit += math.ceil(modify_damage(hit, attack) * damage_mod)
                assert attack_range
                assert attack.range
                attack_range = AttackRange.combine(attack_range, attack.range)
                attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                attacks.append(attack)

            # weapon/spell attack
            else:
                self.recent_rolls["Attack Roll"].append(hit_check)

                # Critical success
                if hit_check == 20:
                    hit = sum(d.roll(2, custom_mod=attack.damage_modifier) for d in attack.damage_dice)
                    self.recent_rolls["Damage Roll"].append(hit)
                    total_hit += modify_damage(hit, attack)
                    assert attack_range
                    assert attack.range
                    attack_range = AttackRange.combine(attack_range, attack.range)
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.CRIT_SUCCESS.value)
                    attacks.append(attack)

                # Critical failure
                elif hit_check == 1:
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.CRIT_FAILURE.value)

                # Normal hit
                elif hit_check + prof_bonus + attack.attack_modifier >= other.armor_class():
                    hit = sum(d.roll(custom_mod=attack.damage_modifier) for d in attack.damage_dice)
                    self.recent_rolls["Damage Roll"].append(hit)
                    total_hit += math.ceil(modify_damage(hit, attack) * damage_mod)
                    assert attack_range
                    assert attack.range
                    attack_range = AttackRange.combine(attack_range, attack.range)
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                    attacks.append(attack)

                # Normal miss
                else:
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.FAILURE.value)

        # TODO: each attack should do a separate instance of damage
        assert attack_range
        attack_name = f"{attack_range.name.title()} Attack"
        if all(isinstance(a, AttackSpell) for a in attacks):
            attack_name = f"Spell Attack"

        # Remove effects that end when making an attack
        self.clear_effects_end(EffectEnd.ATTACK)
        attack_result = AttackResult(attacks, total_hit, dndclient.ActionOutcome(attack_hit), attack_name)
        util.debug(f"Attack result: {attack_result}")
        return attack_result


    def death_saving_throws(self) -> bool:
        """Perform death saving throws, returning True if throws stabilize the Character.
        If the Character becomes stable, they also heal 1 hitpoint (differs from normal rules).
        """
        # allow 1 death save for free
        if self.has_death_protection:
            self.has_death_protection = False
            self.hitpoints = 1
            return True

        self.recent_rolls["Death Saving Throws"] = []
        successes = 0
        failures = 0
        while successes < 3 and failures < 3:
            roll = Dice("1d20").roll()
            effects = [e for e in self.effects if e.effect_stat == EffectStat.SAVING_THROW]
            roll = Effect.apply_effects(roll, effects)
            self.recent_rolls["Death Saving Throws"].append(roll)
            if roll == 20:
                successes = 3
            elif roll == 1:
                failures += 2
            elif roll >= 10:
                successes += 1
            else:
                failures += 1

        if successes >= 3:
            self.hitpoints = 1

        return successes >= 3


    def add_experience(self, xp: int) -> bool:
        """Add experience to the Character, leveling up if needed.

        `exp`: experience points to add.
        """
        old_level = self.level
        self.xp = int(self.xp + xp)
        new_level = dndclient.experience_level(self.xp)

        hitpoints_gained = 0
        prof_bonus_increase = 0
        ability_score_increases = {
            "str": 0,
            "dex": 0,
            "con": 0,
            "int": 0,
            "wis": 0,
            "cha": 0
        }
        features_gained: list[Feature] = []
        spell_slots_gained = [0 for _ in range(len(self.spell_slots))]
        spells_learned: list[Spell] = []
        subclass_gained = {}

        # Level up
        for i in range(old_level + 1, new_level + 1):
            self.level = i

            prev_level_dict = dndclient.get_class_level(self.clazz.index, i - 1)
            level_dict = dndclient.get_class_level(self.clazz.index, i)
            subclass_level_dict: dndclient.DNDResponse = dndclient.DNDResponse({})
            if self.subclass:
                subclass_level_dict = dndclient.get_subclass_level(self.subclass.index, i)
                # This is the first time we're gaining subclass features, so we
                # should note that we gained the subclass
                if dndclient.get_subclass_levels(self.subclass.index)[0]["level"] == i:
                    subclass_gained = subclass_level_dict["subclass"]

            # proficiency increase
            old_proficiency_bonus = self.proficiency_bonus
            self.proficiency_bonus = level_dict["prof_bonus"]
            prof_bonus_increase += self.proficiency_bonus - old_proficiency_bonus

            # HP
            old_max_hitpoints = self.max_hitpoints
            hit_die = Dice(f"1d{self.clazz.hit_die}")
            self.max_hitpoints += max(1, hit_die.roll() + self.ability_modifier("con"))
            hitpoints_gained += self.max_hitpoints - old_max_hitpoints

            # class features
            for feature in level_dict["features"]:
                f = self.add_feature(feature["index"])
                if f is not None:
                    features_gained.append(f)

            # subclass features
            if "features" in subclass_level_dict:
                for feature in subclass_level_dict["features"]:
                    f = self.add_feature(feature["index"])
                    if f is not None:
                        features_gained.append(f)

            # ability score increase
            if "ability_score_bonuses" in level_dict:
                prev_bonus = 0
                if "ability_score_bonuses" in prev_level_dict:
                    prev_bonus = prev_level_dict["ability_score_bonuses"]

                def ability_to_increase():
                    for a in self.ability_priority():
                        if self.abilities[a] < 20:
                            return a
                    for a, score in self.abilities.items():
                        if score < 20:
                            return a
                    return None

                points_to_spend = 2 * (level_dict["ability_score_bonuses"] - prev_bonus)
                for _ in range(points_to_spend):
                    ability = ability_to_increase()
                    if ability:
                        self.abilities[ability] += 1
                        ability_score_increases.setdefault(ability, 0)
                        ability_score_increases[ability] += 1

            new_spells, new_slots = self.learn_spells()
            spells_learned.extend(new_spells)
            for slot, count in enumerate(new_slots):
                spell_slots_gained[slot] += count

        status = "{} {} reached level {}! {}\n\n".format(
            util.emojis["experience"]["level"],
            self.name,
            new_level,
            client.HASHTAG_CHARACTER_LEVELUP
        )
        rewards: list[str] = []

        if subclass_gained:
            rewards.append("New Subclass! {} {}".format(
                util.emojis["subclasses"][subclass_gained["index"]],
                subclass_gained["name"]
            ))

        if hitpoints_gained != 0:
            rewards.append("{:+} {}".format(
                hitpoints_gained,
                util.emojis["health"]["full"]
            ))

        abilities = []
        for ability, increase in ability_score_increases.items():
            if increase == 0: continue
            abilities.append("{:+} {}".format(increase, ability.title()))
        if len(abilities) > 0:
            rewards.append(" ".join(abilities))

        if prof_bonus_increase != 0:
            rewards.append("{:+} Proficiency Bonus".format(prof_bonus_increase))

        if len(features_gained) > 0:
            rewards.append("\n")
            display_count = min(5, len(features_gained))
            extra_count = max(0, len(features_gained) - display_count)
            features = [f.name for f in features_gained[:display_count]]
            if extra_count > 0:
                features.append(f"+ {extra_count} more")
            rewards.append(f"Features: " + ", ".join(features))

        slots = []
        for slot, count in enumerate(spell_slots_gained):
            if count == 0: continue
            slots.append("{:+} {}".format(count, util.emojis["spell_slots"][str(slot)]))

        spells = []
        for spell in spells_learned:
            spells.append("{} {}".format(
                spell.name,
                util.emojis["magic_schools"][spell.school["index"]]
            ))

        if len(slots) > 0 or len(spells) > 0:
            rewards.append("\n")
        if len(slots) > 0:
            rewards.append(" ".join(slots))
        if len(spells) > 0:
            display_count = min(5, len(spells))
            extra_count = max(0, len(spells) - display_count)
            spell_strs = spells[:display_count]
            if extra_count > 0:
                spell_strs.append(f"+ {extra_count} more")
            rewards.append("+ " + ", ".join(spell_strs))

        if self.level > old_level:
            client.post_status(self.campaign_id, status + "\n".join(rewards))
        return self.level > old_level


    def short_rest(self) -> ShortRestResult:
        """Perform a short rest.

        - Restores all hitpoints by using hit dice
        - Sometimes restores spell slots
        """
        # Regenerate hitpoints
        hitpoints_gained = 0
        hitdice_used = 0
        while self.hitpoints < self.max_hitpoints and self.hit_dice > 0:
            roll = max(1, Dice(f"1d{self.clazz.hit_die}").roll() + self.ability_modifier("con"))
            hitpoints_gained += self.heal(roll)
            self.hit_dice -= 1
            hitdice_used += 1

        # Recharge spell slots
        # Warlock
        if self.has_feature("pact-magic"):
            for slot in self.spell_slots:
                slot.recharge()
        # Wizard
        elif self.has_feature("arcane-recovery"):
            if "arcane-recovery" not in self.used_today:
                self.used_today["arcane-recovery"] = 0

            if self.used_today["arcane-recovery"] == 0:
                recovery_count = math.ceil(self.level / 2)
                recovered = False
                # restore highest level slots first, up to level 5
                for slot in reversed(self.spell_slots[:6]):
                    if slot.current_charges < slot.max_charges and recovery_count >= slot.level:
                        recharge_count = recovery_count // slot.level
                        slots_recharged = slot.recharge(recharge_count)
                        recovery_count -= slot.level * slots_recharged
                        recovered = True
                if recovered:
                    self.used_today["arcane-recovery"] += 1

        removed_effects = self.pass_time(60 * 30) # short rests are 30 minutes
        return ShortRestResult(hitpoints_gained, hitdice_used, removed_effects)


    def long_rest(self) -> LongRestResult:
        """Perform a long rest.

        - Restores all hitpoints and hit dice
        - Restores all spell slots
        - Resets per-day abilities/features
        """
        # regen hitpoints
        old_hitpoints = self.hitpoints
        old_hit_dice = self.hit_dice
        self.hitpoints = self.max_hitpoints
        # TODO: long rests should restore self._level/2 per rest, but it is
        # cumbersome when long rests serve no other purpose. Add this back when
        # long rests are more meaningful
        # self._hit_dice = min(self._level, self._hit_dice + math.ceil(self._level / 2))
        self.hit_dice = self.level

        # recharge spell slots
        for slot in self.spell_slots:
            slot.recharge()

        # reset any daily use features/abilities
        self.used_today = {u: 0 for u in self.used_today}

        effects_lost = self.pass_time(60 * 60 * 8) # long rests are ~8 hours
        return LongRestResult(self.hitpoints - old_hitpoints, self.hit_dice - old_hit_dice, effects_lost)


    def title(self, detailed = False) -> str:
        """Get the Character's title; including their name, race, and class.

        `detailed`: whether to display detailed information (subrace and subclass).
        """
        race_name = self.race.name
        class_name = self.clazz.name
        if detailed:
            if self.subrace:
                race_name = self.subrace.name
            if self.subclass and dndclient.get_subclass_levels(self.subclass.index)[0]["level"] <= self.level:
                class_name += " ({})".format(self.subclass.name)
        return f"{self.name}, the {race_name} {class_name}"


    def emoji(self) -> str:
        """Get the emoji that represents the Character's class.
        """
        return util.emojis["classes"][self.clazz.index]


    def hand_str(self, detailed = False) -> str:
        """Get the string to display hand equipment.
        """
        hand_strs = []
        for hand in self.hands:
            if hand.is_weapon():
                hand_str = "{} {} [{}]".format(
                    util.emojis["equipment"]["weapon_2h"] \
                        if hand.has_property("two-handed") \
                        else util.emojis["equipment"]["weapon_1h"],
                    hand.nice_name(),
                    self.weapon_dice_str(hand, hand.damage_dice)
                )
                if detailed:
                    hand_str += " ~ {}".format(
                        " ".join(hand.get_property_emojis()),
                    )
                hand_strs.append(hand_str)
            else:
                hand_strs.append("{} {}".format(
                    util.emojis["equipment"]["armor"],
                    hand.nice_name()
                ))

        return " ".join(hand_strs)


    def armor_str(self) -> str:
        """Get the string to display armor.
        """
        armors = []
        for armor in self.armor:
            armors.append("{} {} [{}]".format(
                util.emojis["equipment"]["armor"],
                armor.nice_name() if armor else "none",
                self.armor_class()
            ))
        if not armors:
            armors.append("{} None [{}]".format(
                util.emojis["equipment"]["armor"],
                self.armor_class()
            ))
        return ", ".join(armors)


    def bio(self) -> str:
        """Create a bio string for this Character.
        """
        bio = "{} {}".format(
            self.emoji(),
            self.title()
        )
        bio += f"\n{self.hitpoint_bar()}"
        bio += "\n{} {} ({})".format(
            util.emojis["experience"]["level"],
            self.level,
            self.xp
        )
        bio += " {}".format(
            "{} {}/{}".format(util.emojis["equipment"]["dice"], self.hit_dice, self.level)
        )
        bio += f"\n{self.hand_str()}"
        bio += f"\n{self.armor_str()}"
        spell_slots = ""
        for slot in self.spell_slots[1:]:
            if not slot.can_use():
                continue
            spell_slots += "{}{}/{} ".format(
                util.emojis["spell_slots"][str(slot.level)],
                slot.current_charges,
                slot.max_charges
            )
        if spell_slots:
            bio += f"\n{spell_slots}"
        bio += "\n{} {}".format(
            util.emojis["equipment"]["gold"],
            int(self.currency.amount)
        )
        bio += f"\n{self.abilities_str()}"

        return bio


    def short_bio(self) -> str:
        """Create a death recap string for this Character.
        """
        health = "{} {}".format(
            util.emojis["health"]["full"],
            self.max_hitpoints
        )
        level = "{} {}".format(
            util.emojis["experience"]["level"],
            self.level
        )
        gold = "{} {}".format(
            util.emojis["equipment"]["gold"],
            int(self.currency.amount)
        )
        equipment = f"{self.hand_str()} {self.armor_str()}"
        abilities = self.abilities_str()
        kills = "{} {}".format(
            util.emojis["health"]["death"],
            self.stats["kills"]
        )

        return lang.bio_character.short.choose(
            util.emojis["health"]["grave"],
            self.title(),
            health,
            level,
            gold,
            equipment,
            abilities,
            kills
        )


    def extended_bio(self) -> str:
        """Extended bio to be displayed when queried with commands.
        """
        bio = "{} {}\n".format(
            self.emoji(),
            self.title(True)
        )

        health_emoji = util.emojis["health"]["full"]
        if self.has_death_protection:
            health_emoji = util.emojis["health"]["protected"]
        elif self.hitpoints < self.max_hitpoints:
            health_emoji = util.emojis["health"]["broken"]
        if self.temp_hitpoints() > 0:
            health_emoji += util.emojis["health"]["temp"]
        bio += "{} {}/{} ".format(
            health_emoji,
            self.hitpoints,
            self.max_hitpoints,
        )
        if self.temp_hitpoints() > 0:
            bio += "+ {} ".format(self.temp_hitpoints())

        bio += "{} {} ({}) ".format(
            util.emojis["experience"]["level"],
            self.level,
            self.xp
        )
        bio += "{} {}/{}\n".format(
            util.emojis["equipment"]["dice"],
            self.hit_dice,
            self.level
        )

        bio += f"{self.hand_str(True)}\n"
        bio += f"{self.armor_str()}\n"

        spell_slots = ""
        for slot in self.spell_slots[1:]:
            if not slot.can_use():
                continue
            spell_slots += "{}{}/{} ".format(
                util.emojis["spell_slots"][str(slot.level)],
                slot.current_charges,
                slot.max_charges
            )
        if spell_slots:
            bio += f"{spell_slots}\n"

        bio += "{} {}\n".format(
            util.emojis["equipment"]["gold"],
            int(self.currency.amount)
        )

        bio += self.abilities_str()
        bio += " Prof. +{}\n".format(self.proficiency_bonus)

        bio += "{} {}\n".format(
            util.emojis["prompts"]["run"],
            self.get_walk_speed()
        )

        bio += "{} {}".format(
            util.emojis["health"]["death"],
            self.stats["kills"]
        )

        return bio


    def __str__(self) -> str:
        """String representation of the Character.
        """
        return f"""{self.name}
        {self.race.name}
        {self.clazz.name}
        {" ".join(f"{a.title()} {self.abilities[a]}" for a in self.abilities)}"""

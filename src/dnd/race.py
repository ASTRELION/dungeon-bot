import dnd.dndclient as dndclient


class Race:
    """D&D Race.
    """
    def __init__(self, race_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = race_dict.reduce()
        self.index = str(race_dict["index"])
        self.name = str(race_dict["name"])
        self.speed = int(race_dict["speed"])
        self.ability_bonuses: list[dict] = list(race_dict["ability_bonuses"])
        self.alignment = str(race_dict["alignment"])
        self.age = str(race_dict["age"])
        self.size = str(race_dict["size"]).lower()
        self.size_description = str(race_dict["size_description"])
        self.starting_proficiencies: list[dict] = list(race_dict["starting_proficiencies"])
        self.starting_proficiency_options = {}
        if "starting_proficiency_options" in race_dict:
            self.starting_proficiency_options = dict(race_dict["starting_proficiency_options"])
        self.languages: list[dict] = list(race_dict["languages"])
        self.language_desc = str(race_dict["language_desc"])
        self.traits: list[dict] = list(race_dict["traits"])
        self.subraces: list[dict] = list(race_dict["subraces"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)

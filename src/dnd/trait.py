import dnd.dndclient as dndclient


class Trait:
    """D&D Trait
    """
    def __init__(self, trait_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = trait_dict.reduce()
        self.index = str(trait_dict["index"])
        self.name = str(trait_dict["name"])
        self.desc = "\n".join(trait_dict["desc"])
        self.proficiencies: list[dict] = list(trait_dict["proficiencies"])
        self.parent = {}
        if "parent" in trait_dict:
            self.parent = dict(trait_dict["parent"])
        self.proficiency_choices = {}
        if "proficiency_choices" in trait_dict:
            self.proficiency_choices = dict(trait_dict["proficiency_choices"])
        self.language_options = {}
        if "language_options" in trait_dict:
            self.language_options = dict(trait_dict["language_options"])
        self.trait_specific = {}
        if "trait_specific" in trait_dict:
            self.trait_specific = dict(trait_dict["trait_specific"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)

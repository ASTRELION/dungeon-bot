import dnd.dndclient as dndclient


class Proficiency:
    """D&D Proficiency
    """
    def __init__(self, proficiency_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = proficiency_dict.reduce()
        self.index = str(proficiency_dict["index"])
        self.name = str(proficiency_dict["name"])
        self.type = str(proficiency_dict["type"]).lower()
        self.reference = dict(proficiency_dict["reference"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)

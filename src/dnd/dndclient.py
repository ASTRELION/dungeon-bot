from enum import Enum
import logging
import math
import random
from time import sleep
import requests_cache
from tenacity import before_sleep_log, retry, stop_after_attempt, wait_fixed

import util

# URL where the API is available.
# All requests will be prefixed with /api/
BASE_URL = "https://www.dnd5eapi.co"
# Experience requirement to reach each level.
# index 0 = level 1, index 1 = level 2, etc.
EXPERIENCE_REQUIREMENT = [
    0, 300, 900, 2700,               # 1-4   (prof 2)
    6500, 14000, 23000, 34000,       # 5-8   (prof 3)
    48000, 64000, 85000, 100000,     # 9-12  (prof 4)
    120000, 140000, 165000, 195000,  # 13-16 (prof 5)
    225000, 265000, 305000, 355000   # 17-20 (prof 6)
]
# Currency units valued by copper pieces
CURRENCY_RATES = {
    "cp": 1,
    "sp": 10,
    "ep": 50,
    "gp": 100,
    "pp": 1000
}
# Weapons that do not have disadvantage when used underwater
AQUATIC_WEAPON = [
    "dagger",
    "javelin",
    "shortsword",
    "spear",
    "trident",
    "crossbow",
    "net",
    "dart",
]
EXTRA_ATTACK_FEATURES = set([
    "barbarian-extra-attack",
    "extra-attack-1",
    "extra-attack-2",
    "extra-attack-3",
    "monk-extra-attack",
    "paladin-extra-attack",
    "ranger-extra-attack"
])
# All possible challenge ratings
CHALLENGE_RATINGS = [0, 0.125, 0.25, 0.5] + [i + 1 for i in range(30)]


class SpellSlot:

    def __init__(self, spell_slot_level: int, max_charges = 0, cantrip = False) -> None:
        """Create a new Spell Slot of the given level."""
        self.level = spell_slot_level
        self.max_charges = max_charges
        self.current_charges = self.max_charges
        self.cantrip = cantrip


    def recharge(self, count: int | None = None) -> int:
        """Replenish charges of this Spell Slot.

        `count`: restore the given amount of charges, otherwise restores all charges.
        """
        old_current = self.current_charges
        if count is None:
            self.current_charges = self.max_charges
        else:
            self.current_charges = min(self.max_charges, self.current_charges + count)
        return self.current_charges - old_current


    def can_use(self) -> bool:
        """Whether this Spell Slot level can be used.
        This determines if a Character can cast a spell at this level.
        """
        return self.max_charges > 0 or self.cantrip


    def has_charge(self) -> bool:
        """Whether this Spell Slot has at least 1 charge.
        """
        return self.current_charges > 0 or self.cantrip


    def use(self) -> int:
        """Use this slot and return the amount of charges.
        """
        self.current_charges = max(0, self.current_charges - 1)
        return self.current_charges


    def __str__(self) -> str:

        return f"{self.level}:{self.current_charges}/{self.max_charges}"


class ActionOutcome(Enum):
    """Outcome of an action.

    `>0`: (crit) success
    `=0`: no outcome
    `<0`: (crit) failure
    """
    CRIT_FAILURE = -2
    FAILURE = -1
    NONE = 0
    SUCCESS = 1
    CRIT_SUCCESS = 2


class DNDResponse:
    """Wrapper for lists and dicts returned by D&D API requests
    """
    def __init__(self, dnd_dict: dict) -> None:
        """Construct a D&D response.
        """
        assert isinstance(dnd_dict, dict | list)
        self._dnd_data = dnd_dict

        if dnd_dict == {}:
            return

        if isinstance(self._dnd_data, dict):
            self._dnd_data["dnd_api_url"] = BASE_URL
        elif isinstance(self._dnd_data, list):
            for r in self._dnd_data:
                assert isinstance(r, dict)
                r["dnd_api_url"] = BASE_URL


    def reduce(self) -> "DNDResponse":
        """Reduce a DNDResponse into its minimum parts. These being the API
        endpoint data and index. This operation is non-destructive and returns
        a new DNDResponse instance.
        """
        if self._dnd_data == {}:
            return DNDResponse({})
        elif isinstance(self._dnd_data, dict):
            return DNDResponse({
                "index": self._dnd_data["index"],
                "url": self._dnd_data["url"],
                "dnd_api_url": self._dnd_data["dnd_api_url"],
            })
        elif isinstance(self._dnd_data, list):
            data = []
            for d in self._dnd_data:
                data.append({
                    "index": d["index"],
                    "url": d["url"],
                    "dnd_api_url": d["dnd_api_url"]
                })
            return DNDResponse(data)


    def valid(self) -> bool:
        """Determine if the response was valid. Valid responses are non-empty.
        """
        return self._dnd_data != {}


    def __str__(self) -> str:

        return str(self._dnd_data)


    def __len__(self) -> int:

        return len(self._dnd_data)


    def __iter__(self):

        return iter(self._dnd_data)


    def __setitem__(self, key, value):

        self._dnd_data[key] = value


    def __getitem__(self, key):

        return self._dnd_data[key]


    def __eq__(self, __value: object) -> bool:

        if not isinstance(__value, self.__class__): return False
        return self._dnd_data == __value._dnd_data


    def __bool__(self) -> bool:

        return bool(self._dnd_data)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def query(path: str, params: dict | None = None) -> DNDResponse:
    """Query the D&D API.

    `path`: API path to query, usually starts with '/api/'.
    `params`: URL parameters to pass to the request.
    """
    response: requests_cache.AnyResponse | None = None
    tries = 0
    while response is None and tries < util.config["max_retries"]:
        tries += 1
        try:
            response = util.get_session().get(f"{BASE_URL}{path}", params=params)
            if response.from_cache:
                util.debug(f"Requested {response.url} (cached)")
            else:
                util.debug(f"Requested {response.url}")
        except Exception as e:
            util.warn(f"Failed to request {BASE_URL}{path}: {e}")
            sleep(util.config["retry_delay"])

    if response and response.status_code == 200:
        response_json = response.json()
        return DNDResponse(response_json)
    elif response:
        util.warn(f"Requesting {response.url} resulted in an invalid request.")
    return DNDResponse({})


def api_health() -> bool:
    """Get API health. `True` if the API is reachable, `False` otherwise.
    """
    response = query("/api")
    return bool(response)


def experience_level(experience_points: int) -> int:
    """Determine the corresponding level from the given experience points.

    `experience_points`: number of xp to determine the level for.
    """
    level = 0
    for i, exp_requirement in enumerate(EXPERIENCE_REQUIREMENT):
        if experience_points >= exp_requirement:
            level = i
        else:
            break

    return level + 1


def get_races() -> DNDResponse:
    """Get a list of all available races.
    """
    return query("/api/races")["results"]


def get_race(race_index: str) -> DNDResponse:
    """Get information about a race.

    `race_index`: index of the race to get.
    """
    return query(f"/api/races/{race_index}")


def get_subraces() -> DNDResponse:
    """Get a list of all available subraces.
    """
    return query("/api/subraces")["results"]


def get_subrace(subrace_index: str) -> DNDResponse:
    """Get information about a subrace.
    """
    return query(f"/api/subraces/{subrace_index}")


def get_classes() -> DNDResponse:
    """Get a list of all available classes.
    """
    return query("/api/classes")["results"]


def get_class(class_index: str) -> DNDResponse:
    """Get information about a class.

    `class_index`: index of the class to get.
    """
    return query(f"/api/classes/{class_index}")


def get_class_level(class_index: str, level: int) -> DNDResponse:
    """Get level information for a class and level.

    `class_index`: index of the class to get levels for.
    `level`: level to get data for.
    """
    return query(f"/api/classes/{class_index}/levels/{level}")


def get_subclasses() -> DNDResponse:
    """Get a list of all available subclasses.
    """
    return query("/api/subclasses")["results"]


def get_subclass(subclass_index: str) -> DNDResponse:
    """Get information about a subclass.

    `subclass_index`: index of the subclass to get.
    """
    return query(f"/api/subclasses/{subclass_index}")


def get_subclass_levels(subclass_index: str) -> DNDResponse:
    """Get a list of all levels for the subclass.

    `subclass_index`: index of the subclass to get levels for.
    `level`: level to get data for.
    """
    return query(f"/api/subclasses/{subclass_index}/levels")


def get_subclass_level(subclass_index: str, level: int) -> DNDResponse:
    """Get level information for a subclass and level.

    `subclass_index`: index of the subclass to get levels for.
    `level`: level to get data for.
    """
    return query(f"/api/subclasses/{subclass_index}/levels/{level}")


def get_abilities() -> DNDResponse:
    """Get a list of all available ability scores.
    """
    return query("/api/ability-scores")["results"]


def get_ability(ability_index: str) -> DNDResponse:
    """Get information about an ability score.

    `ability_index`: index of the ability to get.
    """
    return query(f"/api/ability-scores/{ability_index}")


def get_proficiency(proficiency_index: str) -> DNDResponse:
    """Get information about a proficiency.

    `proficiency_index`: index of the proficiency to get.
    """
    return query(f"/api/proficiencies/{proficiency_index}")


def get_proficiencies() -> DNDResponse:
    """Get a list of all available proficiencies.
    """
    return query("/api/proficiencies")["results"]


def get_skills() -> DNDResponse:
    """Get a list of all available skills.
    """
    return query("/api/skills")["results"]


def get_skill(skill_index: str) -> DNDResponse:
    """Get information about a skill.

    `skill_index`: index of the skill to get.
    """
    return query(f"/api/skills/{skill_index}")


def get_proficiency_ability(proficiency_index: str) -> DNDResponse:
    """Gets the ability related to the given proficiency.

    `proficiency_index`: index of the proficiency to get the ability for.
    """
    proficiency = get_proficiency(proficiency_index)["reference"]["index"]
    skill = get_skill(proficiency)
    if skill:
        return get_ability(skill["ability_score"]["index"])

    util.warn(f"Proficiency {proficiency_index} isn't associated with an ability.")
    return DNDResponse({})


def get_features() -> DNDResponse:
    """Get a list of all available features.
    """
    return query("/api/features")["results"]


def get_feature(feature_index: str) -> DNDResponse:
    """Get information about a feature.

    `feature_index`: index of the feature to get.
    """
    return query(f"/api/features/{feature_index}")


def get_traits() -> DNDResponse:
    """Get a list of all available traits.
    """
    return query("/api/traits")["results"]


def get_trait(trait_index: str) -> DNDResponse:
    """Get information about a trait.

    `trait_index`: index of the trait to get.
    """
    return query(f"/api/traits/{trait_index}")


def get_spells() -> DNDResponse:
    """Get a list of all available spells.
    """
    return query("/api/spells")["results"]


def get_spell(spell_index: str) -> DNDResponse:
    """Get information about a spell.
    """
    if spell_index.startswith("/api/spells/"):
        spell_index = spell_index.replace("/api/spells/", "")
    return query(f"/api/spells/{spell_index}")


def get_class_spells(class_index: str, spell_level: int) -> DNDResponse:
    """Get a list of all available spells for the given class and level.
    """
    # if spell_level == 2:
    #     return []
    return query(f"/api/classes/{class_index}/levels/{spell_level}/spells")["results"]


def get_equipment(equipment_index: str) -> DNDResponse:
    """Get information about an equipment.

    `equipment_index`: index of the equipment to get.
    """
    return query(f"/api/equipment/{equipment_index}")


def get_equipment_category(category_index: str) -> DNDResponse:
    """Get information about an equipment category.

    `category_index`: index of the equipment category to get.
    """
    return query(f"/api/equipment-categories/{category_index}")


def get_languages() -> DNDResponse:
    """Get a list of all available languages.
    """
    return query("/api/languages")["results"]


def get_language(language_index: str) -> DNDResponse:
    """Get information about a language.

    `language_index`: the language to get.
    """
    return query(f"/api/languages/{language_index}")


def get_magic_item(magic_item_index: str) -> DNDResponse:
    """Get information about a magic item.

    `magic_item_index`: index of the magic item to get.
    """
    return query(f"/api/magic-items/{magic_item_index}")


def get_monster(monster_index: str) -> DNDResponse:
    """Get information about a monster.

    `monster_index`: index of the monster to get.
    """
    return query(f"/api/monsters/{monster_index}")


def get_monsters(challenge_ratings: list[float] | None = None) -> DNDResponse:
    """Get a list of monsters based on challenge rating.

    `challenge_ratings`: challenge ratings to filter by.
    """
    return query("/api/monsters", {"challenge_rating": challenge_ratings})["results"]


def get_monster_of_rating(challenge_ratings: list[float]) -> DNDResponse:
    """Get a random monster of the given ratings. A random rating is chosen first, then
    a monster from the resulting rating.

    `challenge_ratings`: challenge ratings to filter by.
    """
    ratings = challenge_ratings.copy()
    monsters: DNDResponse | None = None
    while not monsters and ratings:
        monster_cr = random.choice(ratings)
        ratings.remove(monster_cr)
        monsters = get_monsters([monster_cr])
    assert monsters
    monster_index = random.choice(monsters)["index"]
    return get_monster(monster_index)


def get_monster_lower(challenge_rating=0.5) -> DNDResponse:
    """Get a monster with the given `challenge_rating` or lower

    `challenge_rating`: challenge rating to filter by, includes every CR below.
    """
    challenge_ratings = [0, 0.125, 0.25, 0.5][:math.floor(challenge_rating * 8) + 1]
    challenge_ratings += [i + 1 for i in range(math.floor(challenge_rating))]
    return get_monster_of_rating(challenge_ratings)


def get_monster_image_link(monster: DNDResponse) -> str:
    """Get the image link associated with a monster, if any.

    `monster`: monster dict to get the image link for.
    """
    link = ""
    if "image" in monster:
        link = f"{BASE_URL}{monster['image']}"

    return link.replace("www.", "")


def xp_to_max_cr(experience_points: int) -> float:
    """Converts the given Character xp to maximum Monster Challenge Rating.

    This allows monsters to gradually increase in difficulty as the Character
    gains xp, not just on level.

    `experience_points`: Character xp to convert.
    """
    cr: float = math.floor((30 / math.sqrt(355000)) * math.sqrt(experience_points))
    if experience_points < 900:
        cr = [0, 0.125, 0.25, 0.5][math.floor((((0.5 / 1800) * experience_points) + 0.25) * 8)]
    return min(30, cr)


def ability_modifier(ability_score: int) -> int:
    """Get the ability modifier for a given score.

    `ability_score`: ability score value to determine the modifier for.
    """
    return math.floor((ability_score - 10) / 2)


def cr_proficiency(cr: float) -> int:
    """Determine the proficiency bonus from the given challenge rating.

    `cr`: CR to determine proficiency bonus for.
    """
    return 2 if cr == 0 else math.ceil(cr / 4) + 1

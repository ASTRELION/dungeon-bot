import dnd.dndclient as dndclient


class Spell:
    """D&D Spell
    """
    def __init__(self, spell_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = spell_dict.reduce()
        self.index = str(spell_dict["index"])
        self.name = str(spell_dict["name"])
        self.desc = "\n".join(spell_dict["desc"])
        self.higher_level = "\n".join(spell_dict["higher_level"])
        self.range = str(spell_dict["range"]).lower()
        self.components = list(spell_dict["components"])
        self.material = ""
        if "material" in spell_dict:
            self.material = str(spell_dict["material"])
        self.area_of_effect = {}
        if "area_of_effect" in spell_dict:
            self.area_of_effect = dict(spell_dict["area_of_effect"])
        self.ritual = bool(spell_dict["ritual"])
        self.duration = str(spell_dict["duration"])
        self.concentration = bool(spell_dict["concentration"])
        self.casting_time = str(spell_dict["casting_time"])
        self.level = int(spell_dict["level"])
        self.dc_type = ""
        self.dc_success = ""
        self.dc_desc = ""
        self.dc = False
        if "dc" in spell_dict:
            self.dc_type = str(spell_dict["dc"]["dc_type"]["index"])
            self.dc_success = str(spell_dict["dc"]["dc_success"]).lower()
            if "desc" in spell_dict["dc"]:
                self.dc_desc = str(spell_dict["dc"]["desc"])
            self.dc = True
        self.attack_type = ""
        if "attack_type" in spell_dict:
            self.attack_type = str(spell_dict["attack_type"]).lower()
        self.damage = {}
        if "damage" in spell_dict:
            self.damage = dict(spell_dict["damage"])
        self.school = dict(spell_dict["school"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)

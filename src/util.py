import inspect
import json
import logging
import os
import shutil
from typing import Any
import requests_cache
import toml
from datetime import timedelta


SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60
MICROSECONDS_PER_SECOND = 1000000


def debug(text: Any):
    """Log a debug message.

    `text`: text to log.
    """
    stack = inspect.stack()
    caller_frame = stack[1][0]
    caller_name = os.path.basename(caller_frame.f_globals["__file__"])
    base, _ = os.path.splitext(caller_name)
    logger.debug(f"{base}: {text}")


def error(text: Any):
    """Log an error message.

    `text`: text to log.
    """
    stack = inspect.stack()
    caller_frame = stack[1][0]
    caller_name = os.path.basename(caller_frame.f_globals["__file__"])
    base, _ = os.path.splitext(caller_name)
    logger.error(f"{base}: {text}")


def warn(text: Any):
    """Log a warning message.

    `text`: text to log.
    """
    stack = inspect.stack()
    caller_frame = stack[1][0]
    caller_name = os.path.basename(caller_frame.f_globals["__file__"])
    base, _ = os.path.splitext(caller_name)
    logger.warning(f"{base}: {text}")


def info(text: Any):
    """Log a info message.

    `text`: text to log.
    """
    stack = inspect.stack()
    caller_frame = stack[1][0]
    caller_name = os.path.basename(caller_frame.f_globals["__file__"])
    base, _ = os.path.splitext(caller_name)
    logger.info(f"{base}: {text}")


def format_duration(seconds: int | float) -> str:
    """Formats the given time in seconds as Days, HH:MM

    `seconds`: amount of seconds to format.
    """
    seconds = int(seconds)
    days, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    if days == 1:
        return f"{days} day, {hours:02d}:{minutes:02d}"

    if days > 0:
        return f"{days} days, {hours:02d}:{minutes:02d}"

    return f"{hours:02d}:{minutes:02d}"


def format_timedelta(delta: timedelta) -> str:
    """Format a datetime like 5 days, 5 hours, 5 minutes, 5.5 seconds
    """
    s = []
    days = delta.days
    hours, seconds = divmod(delta.seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)
    seconds = int(seconds + (delta.microseconds / MICROSECONDS_PER_SECOND))

    if days > 0:
        s.append("{} day{}".format(days, "s" if days != 1 else ""))
    if hours > 0:
        s.append("{} hour{}".format(hours, "s" if hours != 1 else ""))
    if minutes > 0:
        s.append("{} minute{}".format(minutes, "s" if minutes != 1 else ""))
    if seconds > 0:
        s.append("{:.2f} second{}".format(seconds, "s" if seconds != 1 else ""))

    if len(s) == 0:
        return "0 seconds"

    return ", ".join(s)


def get_clock(seconds: int | float) -> str:
    """Get the clock emoji associated with the given amount of seconds.

    `seconds`: amount of seconds to get the emoji for.
    """
    seconds = int(seconds)
    _, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    hours = hours % 12 or 12
    base = 0x1f550 # 🕐

    if minutes >= 30:
        base = 0x1f55c # 🕜

    return f"{chr(base + hours - 1)}"


def clamp(number: float | int, minimum: float | int, maximum: float | int) -> float | int:
    """Clamp the given number to the range [minimum, maximum].

    `number`: the number to clamp.
    `minimum`: the minimum number.
    `maximum`: the maximum number.
    """
    return max(minimum, min(maximum, number))


def int_in_array(obj: list | int, num: int) -> bool:
    """Find if a given int is in an arbitrarily nested int array

    `obj`: list or int to search.
    `num`: number to search for.
    """
    if isinstance(obj, int) and obj == num:
        return True
    elif isinstance(obj, list):
        return any(int_in_array(element, num) for element in obj)
    return False


def sum_array(array: list, func = None) -> int:
    """Sum an arbitrarily nested int array

    `array`: sum every number in a nested array.
    `func`: optional mapping function.
    """
    total = 0
    for item in array:
        if isinstance(item, int):
            total += item if func is None else func(item)
        elif isinstance(item, list):
            total += sum_array(item, func)
    return total


def sort_multi(array: list) -> list:
    """Sort a multidimensional array. Returns a new array.
    """
    new_array = []
    for element in array:
        if isinstance(element, list):
            new_array.append(sort_multi(element))
        else:
            new_array.append(element)

    def find_first_num(arr: list) -> int | float:
        if isinstance(arr[0], list): return find_first_num(arr[0])
        return arr[0]

    return sorted(new_array, key=lambda k: find_first_num(k) if isinstance(k, list) else k)


def flatten_array(array: list) -> list:
    """Flatten an arbitrarily nested array.
    """
    result = []
    for element in array:
        if isinstance(element, list):
            result.extend(flatten_array(element))
        else:
            result.append(element)
    return result


def float_to_frac(number: float) -> str:
    """Convert a decimal number to a fraction string.
    e.g. 0.25 -> "1/4", 4 -> "4"
    """
    if int(number) == number:
        return str(int(number))

    nums = number.as_integer_ratio()
    return f"{nums[0]}/{nums[1]}"


def key_or_less(key: str, dictionary: dict[str, Any]) -> str:
    """Get the given key from the dictionary, or a key with a value less
    than `key`, if it exists.

    `key`: key to get, should be cast-able to int.
    `dictionary`: dict to search.
    """
    if key in dictionary:
        return key

    keys = list(dictionary)
    i = len(keys) - 1
    max_key = keys[i]
    while int(max_key) > int(key) and i > 0:
        i -= 1
        max_key = keys[i]

    return max_key


def sign(number: int) -> int:

    if number > 0: return 1
    if number < 0: return -1
    return 0


def try_load(file_name: str) -> dict:
    """Try to load a configuration file, creating the default if it cannot be loaded.

    `file_name`: file name to load, assumes default directory `config/` and data directory `data/`
    """
    default_data: dict = {}
    # load defaults
    with open(os.path.join(root, f"config/{file_name}"), "r", encoding="UTF-8") as file:
        default_data = {}
        if file_name.endswith(".toml"):
            default_data = toml.load(file)
        elif file_name.endswith(".json"):
            default_data = json.load(file)

    # load existing
    try:
        with open(os.path.join(root, f"data/{file_name}"), "r", encoding="UTF-8") as file:
            existing_data = {}
            if file_name.endswith(".toml"):
                existing_data = toml.load(file)
            elif file_name.endswith(".json"):
                existing_data = json.load(file)

            def set_values(dict_a: dict, dict_b: dict):
                for key in dict_a:
                    if key in dict_b:
                        if isinstance(dict_a[key], dict):
                            set_values(dict_a[key], dict_b[key])
                        elif not key.startswith("_"):
                            dict_a[key] = dict_b[key]

            set_values(default_data, existing_data)

    # or, create defaults
    except FileNotFoundError:
        pass

    with open(os.path.join(root, f"data/{file_name}"), "w", encoding="UTF-8") as file:
        if file_name.endswith(".json"):
            json.dump(default_data, file)
        elif file_name.endswith(".toml"):
            toml.dump(default_data, file)

    return default_data


def save(file_name: str, data: dict) -> None:
    """Save the given data at the file name.

    `file_name`: name of the file, including file extension.
    `data`: dictionary of data to save.
    """
    with open(os.path.join(root, f"data/{file_name}"), "w", encoding="UTF-8") as file:
        if file_name.endswith(".json"):
            json.dump(data, file)
        elif file_name.endswith(".toml"):
            toml.dump(data, file)


def save_configs() -> None:
    """Save all config files to disk.
    """
    save("emojis.toml", emojis)
    save("config.toml", config)
    save("tips.toml", tips)
    save("blacklist.toml", blacklist)


def get_session():
    """Create or get the session for REST requests.
    """
    global session
    if not session:
        session = requests_cache.CachedSession(os.path.join(root, CACHE_PATH))
    return session


def reset_session():
    """Reset session.
    """
    global session
    if session:
        session.cache.clear()
        session.close()
        session = None


logger = logging.getLogger("dungeons")
logger.setLevel(logging.INFO)


def init_logger() -> None:

    logger.propagate = False
    formatter = logging.Formatter("%(asctime)s %(levelname)-8s %(message)s")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)


root = os.path.dirname(os.path.dirname(__file__))

# Path to cache requests (with no file extension)
CACHE_PATH = "data/cache"

session: requests_cache.CachedSession | None = None

# Ensure data dir exists
if not os.path.exists(os.path.join(root, "data/")):
    os.makedirs(os.path.join(root, "data/"))

# Init data
emojis = try_load("emojis.toml")
config = try_load("config.toml")
tips = try_load("tips.toml")
blacklist = try_load("blacklist.toml")

shutil.copyfile(os.path.join(root, "config/.token.example"), os.path.join(root, "data/.token.example"))

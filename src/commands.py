from datetime import datetime, timezone
import logging
import multiprocessing
from multiprocessing.managers import SyncManager
import os
import re
from typing import Any

from mastodon import Mastodon, MastodonError, StreamListener # type: ignore[import-untyped]
from tenacity import before_sleep_log, retry, retry_if_exception_type, stop_after_attempt, wait_fixed
import client
from dnd import dndclient
from effect import EffectType
import image
import lang # type: ignore[import-untyped]

import util


# Thread to listen to commands on
command_thread: multiprocessing.Process | None = None
manager: SyncManager | None = None
manager_ns: Any = None
"""Contains a reference to the snapshot state of `campaign` and
the internal `stream_handler`."""
dry = False


def start_command_thread():

    global command_thread, manager, manager_ns
    manager = multiprocessing.Manager()
    manager_ns = manager.Namespace()
    manager_ns.campaign = None
    manager_ns.stream_handler = None
    util.debug("Starting command thread...")
    command_thread = multiprocessing.Process(target=_command_thread, args=(manager_ns,), daemon=True)
    command_thread.start()


def _command_thread(ns):

    if dry:
        return

    client = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    while client:
        try:
            listener = CommandListener(ns)
            ns.stream_handler = client.stream_user(listener, run_async=False) # blocks until error
        except Exception as e:
            ns.stream_handler = None
            util.warn(f"Command thread encountered an error: {e}")


class CommandListener(StreamListener):

    def __init__(self, ns) -> None:

        self.ns = ns
        super().__init__()
        util.debug("Created command listener")


    # {
    #     'id': # id of the notification
    #     'type': # "mention", "reblog", "favourite", "follow", "poll" or "follow_request"
    #     'created_at': # The time the notification was created
    #     'account': # User dict of the user from whom the notification originates
    #     'status': # In case of "mention", the mentioning status
    #             # In case of reblog / favourite, the reblogged / favourited status
    # }
    @retry(
        stop=stop_after_attempt(util.config["max_retries"]),
        wait=wait_fixed(util.config["retry_delay"]),
        retry=retry_if_exception_type(MastodonError),
        before_sleep=before_sleep_log(util.logger, logging.WARN))
    def on_notification(self, notification: dict) -> None:

        if notification["type"] == "mention":
            util.debug(f"Received mention")
            self.command_handler(notification["account"], notification["status"])

        super().on_notification(notification)


    # {
    #     'id': # The ID of this conversation object
    #     'unread': # Boolean indicating whether this conversation has yet to be
    #             # read by the user
    #     'accounts': # List of accounts (other than the logged-in account) that
    #                 # are part of this conversation
    #     'last_status': # The newest status in this conversation
    # }
    @retry(
        stop=stop_after_attempt(util.config["max_retries"]),
        wait=wait_fixed(util.config["retry_delay"]),
        retry=retry_if_exception_type(MastodonError),
        before_sleep=before_sleep_log(util.logger, logging.WARN))
    def on_conversation(self, conversation: dict) -> None:

        util.debug("Received DM")
        self.command_handler(conversation["accounts"][0], conversation["last_status"])

        super().on_conversation(conversation)


    def command_handler(self, user: dict, status: dict) -> None:
        """Handle commands sent by mentions.
        """
        from campaign import Campaign
        campaign: Campaign = self.ns.campaign

        if not campaign:
            util.warn("Campaign not available.")

        result = re.search(f"(^|\s|<br />|<br/>|<br>)+{util.config['command_prefix']}(\w+)( ( |\w)*)?", status["content"])

        if not result:
            return

        command = result.group(2).lower()
        args = []
        if result.group(3):
            args = re.sub(" {2,}", " ", result.group(3).strip().lower()).split(" ")
        util.debug(f"Received command '{command}' with args '{args}'")

        match command:
            # Display proficient armor
            case "armor":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                armors = campaign.character.proficient_armor()
                armor_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for armor in armors:
                    cost = armor.cost
                    if afford and cost > campaign.character.currency:
                        continue
                    armor_str.append((cost, f"{armor.name} ({int(cost.amount)})"))

                if not armor_str:
                    armor_str.append((0, "None"))

                armor_str = sorted(armor_str, key=lambda a: a[0], reverse=not asc)
                client.command_reply(", ".join([a[1] for a in armor_str]), status, command)

            # Display author URL
            case "author":
                client.command_reply(util.config["author_url"], status, command)

            # Display the character avatar
            case "avatar":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return
                if not os.path.exists(image.AI_AVATAR_ORIGINAL_PATH):
                    client.command_reply("Character avatar currently unavailable.", status, command)
                    return
                client.command_reply(
                    f"{campaign.character.title()}'s Avatar",
                    status, command,
                    image_path=image.AI_AVATAR_ORIGINAL_PATH,
                    image_alt="AI-generated Character avatar"
                )

            # Blacklist the given user from being chosen
            case "blacklist":
                if str(user["id"]) not in util.blacklist["b"]:
                    util.blacklist["b"].append(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.blacklisted.choose(), status, command)

            # Give a link to the campaigns tag
            case "campaigns":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_CAMPAIGN_START.removeprefix("#")
                ), status, command)

            # Display character information
            case "character":
                if campaign.character:
                    client.command_reply(
                        campaign.character.extended_bio(),
                        status,
                        command,
                        image_path=image.AI_AVATAR_ORIGINAL_PATH,
                        image_alt="AI-generated Character avatar"
                    )
                else:
                    client.command_reply("Character currently unavailable.", status, command)

            case "charactersheet":
                if campaign.character and campaign.character.id != 0:
                    client.command_reply(
                        "{}/characters/{}".format(
                            util.config["website_url"],
                            campaign.character.id
                        ),
                        status,
                        command
                    )
                else:
                    client.command_reply("Character sheet not available.", status, command)

            # List the effects on the character
            case "effects":
                if campaign.character:
                    string = ""
                    for effect_type in EffectType:
                        effect_strs = []
                        for effect in [e for e in campaign.character.effects if e.effect_type == effect_type]:
                            effect_strs.append(
                                "{} {} {}s".format(effect.effect_stat.name, effect.mod_str(), effect.duration)
                            )
                        if len(effect_strs) > 0:
                            string += "{}\n{}\n\n".format(effect_type.name.title(), ", ".join(effect_strs))
                    if not string:
                        string = "There are no active effects."
                    client.command_reply(string, status, command)

                else:
                    client.command_reply("Character currently unavailable.", status, command)

            case "graves" | "graveyard":
                client.command_reply("{}/graveyard".format(util.config["website_url"]), status, command)

            case "header" | "banner":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return
                if not os.path.exists(image.AI_HEADER_ORIGINAL_PATH):
                    client.command_reply("Character header currently unavailable.", status, command)
                    return
                client.command_reply(
                    f"{campaign.character.title()}'s Header",
                    status, command,
                    image_path=image.AI_HEADER_ORIGINAL_PATH,
                    image_alt="AI-generated Character header"
                )

            # Display monster information
            case "monster":
                if campaign.monster:
                    image_path = campaign.monster.image_url or None
                    client.command_reply(
                        campaign.monster.extended_bio(),
                        status,
                        command,
                        image_path=image_path,
                        image_alt="AI-generated Monster avatar"
                    )
                else:
                    client.command_reply("Monster currently unavailable.", status, command)

            # Display possible monsters
            case "monsters":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                monster_cr = dndclient.xp_to_max_cr(campaign.character.xp)
                monsters = dndclient.get_monsters(dndclient.CHALLENGE_RATINGS[:dndclient.CHALLENGE_RATINGS.index(monster_cr) + 1])
                monster_str = []

                for m in monsters:
                    monster = dndclient.get_monster(m["index"])
                    monster_str.append((monster["challenge_rating"], f"{monster['name']} ({monster['challenge_rating']})"))

                asc = args and args[0].startswith("asc")
                monster_str = sorted(monster_str, key=lambda m: m[0], reverse=not asc)
                client.command_reply(", ".join([m[1] for m in monster_str]), status, command)

            # Display recent poll info
            case "posts" | "polls":
                if not campaign.root_post_url:
                    client.command_reply("No polls available.", status, command)
                    return

                text = f"Root poll: {campaign.root_post_url}"
                text += f"\nLast completed poll: {campaign.completed_post_url or 'None'}"
                text += f"\nCurrent poll: {campaign.current_post_url}"
                text += "\n\nAll campaign posts: {}/@{}/tagged/c{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    campaign.id
                )
                client.command_reply(text, status, command)

            # Display the rolls that ocurred
            case "rolls":
                rolls_str = ""

                if campaign.character and campaign.character.get_recent_rolls():
                    rolls_str += f"{campaign.character.emoji()} Character Rolls\n{campaign.character.get_recent_rolls()}\n\n"

                if campaign.monster and campaign.monster.get_recent_rolls():
                    rolls_str += f"{campaign.monster.emoji()} Monster Rolls\n{campaign.monster.get_recent_rolls()}"

                if rolls_str:
                    client.command_reply(rolls_str, status, command)
                else:
                    client.command_reply("Rolls currently unavailable.", status, command)

            # Display all items that could appear in a shop (proficient)
            case "shop" | "proficient" | "equipment":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                weapons = campaign.character.proficient_weapons()
                armors = campaign.character.proficient_armor()
                weapons_str = []
                armor_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for weapon in weapons:
                    cost = weapon.cost
                    if afford and cost > campaign.character.currency:
                        continue
                    weapons_str.append((cost, f"{weapon.name} ({cost})"))

                for armor in armors:
                    cost = armor.cost
                    if afford and cost > campaign.character.currency:
                        continue
                    armor_str.append((cost, f"{armor.name} ({cost})"))

                if not armor_str:
                    armor_str.append((0, "None"))

                weapons_str = sorted(weapons_str, key=lambda w: w[0], reverse=not asc)
                armor_str = sorted(armor_str, key=lambda a: a[0], reverse=not asc)
                client.command_reply(
                    f"Weapons:\n{', '.join([w[1] for w in weapons_str])}\n\n" +
                    f"Armor:\n{', '.join([a[1] for a in armor_str])}",
                    status,
                    command
                )

            # Display source URL
            case "source":
                client.command_reply(util.config["source_url"], status, command)

            # Display learned spells
            case "spells":
                if campaign.character:
                    spell_str = ""
                    for i, slot in enumerate(campaign.character.spells):
                        if not slot: continue
                        spell_str += f"{util.emojis['spell_slots'][str(i)]}\n"
                        spell_str += ", ".join([s.name for s in slot])
                        spell_str += "\n"

                    if spell_str:
                        client.command_reply(spell_str, status, command)
                    else:
                        client.command_reply("No spells available.", status, command)
                else:
                    client.command_reply("Character currently unavailable.", status, command)

            # Display status/api status
            case "status" | "ping":
                now_time = datetime.now(timezone.utc)
                text = ""

                if campaign.start_time:
                    text += "Campaign started: {} ago".format(
                        util.format_timedelta(now_time - campaign.start_time)
                    )
                if campaign.current_post_url:
                    post_time = client.mastodon.status(campaign.current_post_url.split("/")[-1])["created_at"]
                    text += "\nLast post: {} ago".format(
                        util.format_timedelta(now_time - post_time)
                    )

                text += "\n\nInstance health: {}".format(
                    "Healthy" if client.mastodon.instance_health() else "Unhealthy"
                )
                text += "\nD&D API health: {}".format(
                    "Healthy" if dndclient.api_health() else "Unhealthy"
                )

                status_time: datetime = status["created_at"].astimezone(timezone.utc)
                text += "\n\nResponded in {}".format(
                    util.format_timedelta(now_time - status_time)
                )

                client.command_reply(text, status, command)

            # Display link to #tip
            case "tips":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_TIP.removeprefix("#")
                ), status, command)

            # Display link to #update
            case "updates" | "changelog":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_UPDATE.removeprefix("#")
                ), status, command)

            # Display proficient weapon
            case "weapons":
                if not campaign.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                weapons = campaign.character.proficient_weapons()
                weapons_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for weapon in weapons:
                    cost = weapon.cost
                    if afford and cost > campaign.character.currency:
                        continue
                    weapons_str.append((cost, f"{weapon.name} ({cost})"))

                weapons_str = sorted(weapons_str, key=lambda w: w[0], reverse=not asc)
                client.command_reply(", ".join([w[1] for w in weapons_str]), status, command)

            case "website":
                client.command_reply(util.config["website_url"], status, command)

            # Whitelist the given user (removes from blacklist)
            case "whitelist":
                if str(user["id"]) in util.blacklist["b"]:
                    util.blacklist["b"].remove(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.whitelisted.choose(), status, command)

            # Invalid command
            case _:
                commands = [
                    "armor",
                    "author",
                    "avatar",
                    "campaigns",
                    "character",
                    "charactersheet",
                    "blacklist",
                    "effect",
                    "graves", "graveyard",
                    "header", "banner",
                    "help",
                    "monster",
                    "monsters",
                    "posts", "polls",
                    "rolls",
                    "shop", "proficient", "equipment",
                    "source",
                    "status", "ping",
                    "tips",
                    "updates", "changelog",
                    "weapons",
                    "website",
                    "whitelist",
                ]
                help = "Available commands:\n"
                help += ", ".join(commands)
                help += "\n\nDetailed command list here:\n{}/graveyard".format(
                    util.config["website_url"]
                )
                client.command_reply(help, status, "help")

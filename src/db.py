# mypy: disable-error-code="valid-type,misc,no-redef,var-annotated,assignment"

from datetime import datetime, timezone
import enum
import json
import logging
import os
import psycopg2
import requests
from sqlalchemy import Boolean, Column, Engine, Float, ForeignKey, Integer, String, create_engine, JSON, DateTime, Enum
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy.exc import SQLAlchemyError
from tenacity import before_sleep_log, retry, retry_if_exception_type, stop_after_attempt, wait_fixed
from constants import State
import util
from alembic import command
from alembic.config import Config
import dnd.dndclient as dndclient


Base = declarative_base()


class CharacterModel(Base):
    __tablename__ = "character"

    id = Column(Integer, primary_key=True)
    campaign = Column(Integer, ForeignKey("campaign.id"))
    # character specific
    race = Column(JSON)
    clazz = Column(JSON)
    subrace = Column(JSON)
    subclass = Column(JSON)
    level = Column(Integer)
    hit_dice = Column(Integer)
    xp = Column(Integer)
    stats = Column(JSON)
    death_protection = Column(Boolean)
    hands = Column(JSON)
    armor = Column(JSON)
    currency = Column(JSON)
    class_specific = Column(JSON)
    spell_slots = Column(JSON)
    spells = Column(JSON)
    spells_known = Column(JSON)
    spell_ability = Column(String)
    complexion = Column(String)
    clothes = Column(JSON)
    culture = Column(String)
    gender = Column(String)
    # general to creatures
    created_at = Column(DateTime)
    died_at = Column(DateTime, nullable=True)
    name = Column(String)
    hitpoints = Column(Integer)
    max_hitpoints = Column(Integer)
    temp_hitpoints = Column(Integer)
    armor_class = Column(Integer)
    alignment = Column(String)
    size = Column(String)
    proficiency_bonus = Column(Integer)
    initiative = Column(Integer)
    abilities = Column(JSON)
    skills = Column(JSON)
    saving_throws = Column(JSON)
    proficiencies = Column(JSON)
    features = Column(JSON)
    traits = Column(JSON)
    languages = Column(JSON)
    vulnerabilities = Column(JSON)
    resistances = Column(JSON)
    immunities = Column(JSON)
    effects = Column(JSON)
    surprised = Column(Boolean)
    walk_speed = Column(Integer)
    swim_speed = Column(Integer)
    fly_speed = Column(Integer)
    climb_speed = Column(Integer)
    burrow_speed = Column(Integer)
    used_today = Column(JSON)
    recent_rolls = Column(JSON)

    @staticmethod
    def from_character(character, campaign_id: int) -> "CharacterModel":
        from character import Character
        character: Character = character
        return CharacterModel(
            campaign=campaign_id,
            # character-specific
            race=character.race,
            clazz=character.clazz,
            subrace=character.subrace or {},
            subclass=character.subclass or {},
            level=character.level,
            hit_dice=character.hit_dice,
            xp=character.xp,
            stats=character.stats,
            death_protection=character.has_death_protection,
            hands=character.hands,
            armor=character.armor,
            currency=character.currency,
            class_specific=character.class_specific,
            spell_slots=character.spell_slots,
            spells=character.spells,
            spells_known=character.spells_known,
            spell_ability=character.spell_ability,
            complexion=character.complexion,
            clothes=character.clothes,
            culture=character.culture,
            gender=character.gender,
            # general to creatures
            created_at=character.created_at,
            died_at=character.died_at,
            name=character.name,
            hitpoints=character.hitpoints,
            max_hitpoints=character.max_hitpoints,
            temp_hitpoints=character.temp_hitpoints(),
            armor_class=character.armor_class(),
            alignment=character.alignment,
            size=character.size,
            proficiency_bonus=character.proficiency_bonus,
            initiative=character.initiative,
            abilities=character.abilities,
            skills=character.skills,
            saving_throws=character.saving_throws,
            proficiencies=character.proficiencies,
            features=character.features,
            traits=character.traits,
            languages=character.languages,
            vulnerabilities=character.vulnerabilities,
            resistances=character.resistances,
            immunities=character.immunities,
            effects=character.effects,
            surprised=character.is_surprised,
            walk_speed=character.walk_speed,
            swim_speed=character.walk_speed,
            fly_speed=character.fly_speed,
            climb_speed=character.climb_speed,
            burrow_speed=character.burrow_speed,
            used_today=character.used_today,
            recent_rolls=character.recent_rolls,
        )


class MonsterModel(Base):
    __tablename__ = "monster"

    id = Column(Integer, primary_key=True)
    campaign = Column(Integer, ForeignKey("campaign.id"))
    # specific to monster
    monster = Column(JSON)
    type = Column(String)
    unique_name = Column(String)
    xp = Column(Integer)
    challenge_rating = Column(Float)
    image_url = Column(String)
    recharging = Column(JSON)
    actions = Column(JSON)
    special_abilities = Column(JSON)
    senses = Column(JSON)
    legendary_actions = Column(JSON)
    spell_slots = Column(JSON)
    spells = Column(JSON)
    spell_ability = Column(String)
    spells_known = Column(JSON)
    spell_level = Column(Integer)
    spell_school = Column(String)
    spell_innate = Column(Boolean)
    spell_dc = Column(Integer)
    spell_modifier = Column(Integer)
    subtype = Column(String)
    description_modifier = Column(String)
    # general to creatures
    created_at = Column(DateTime)
    died_at = Column(DateTime, nullable=True)
    name = Column(String)
    hitpoints = Column(Integer)
    max_hitpoints = Column(Integer)
    temp_hitpoints = Column(Integer)
    armor_class = Column(Integer)
    alignment = Column(String)
    size = Column(String)
    proficiency_bonus = Column(Integer)
    initiative = Column(Integer)
    abilities = Column(JSON)
    skills = Column(JSON)
    saving_throws = Column(JSON)
    proficiencies = Column(JSON)
    features = Column(JSON)
    traits = Column(JSON)
    languages = Column(JSON)
    vulnerabilities = Column(JSON)
    resistances = Column(JSON)
    immunities = Column(JSON)
    effects = Column(JSON)
    surprised = Column(Boolean)
    walk_speed = Column(Integer)
    swim_speed = Column(Integer)
    fly_speed = Column(Integer)
    climb_speed = Column(Integer)
    burrow_speed = Column(Integer)
    used_today = Column(JSON)
    recent_rolls = Column(JSON)

    @staticmethod
    def from_monster(monster, campaign_id: int) -> "MonsterModel":
        from dnd.monster import Monster
        monster: Monster = monster
        return MonsterModel(
            campaign=campaign_id,
            monster=monster._dnd_dict,
            type=monster.monster_type,
            unique_name=monster.unique_name,
            xp=monster.xp,
            challenge_rating=monster.challenge_rating,
            image_url=monster.image_url,
            recharging=monster.recharging,
            actions=monster.actions,
            legendary_actions=monster.legendary_actions,
            special_abilities=monster.special_abilities,
            senses=monster.senses,
            spell_slots=monster.spell_slots,
            spells=monster.spells,
            spell_ability=monster.spell_ability,
            spells_known=monster.spells_known,
            spell_level=monster.spell_level,
            spell_school=monster.spell_school,
            spell_innate=monster.spell_innate,
            spell_dc=monster.spell_dc,
            spell_modifier=monster.spell_modifier,
            subtype=monster.monster_subtype,
            description_modifier=monster.description_modifier,
            created_at=monster.created_at,
            died_at=monster.died_at,
            name=monster.name,
            hitpoints=monster.hitpoints,
            max_hitpoints=monster.max_hitpoints,
            temp_hitpoints=monster.temp_hitpoints(),
            armor_class=monster.armor_class(),
            alignment=monster.alignment,
            size=monster.size,
            proficiency_bonus=monster.proficiency_bonus,
            initiative=monster.initiative,
            abilities=monster.abilities,
            skills=monster.skills,
            saving_throws=monster.saving_throws,
            proficiencies=monster.proficiencies,
            features=monster.features,
            traits=monster.traits,
            languages=monster.languages,
            vulnerabilities=monster.vulnerabilities,
            resistances=monster.resistances,
            immunities=monster.immunities,
            effects=monster.effects,
            surprised=monster.is_surprised,
            walk_speed=monster.walk_speed,
            swim_speed=monster.walk_speed,
            fly_speed=monster.fly_speed,
            climb_speed=monster.climb_speed,
            burrow_speed=monster.burrow_speed,
            used_today=monster.used_today,
            recent_rolls=monster.recent_rolls,
        )


class CombatLogModel(Base):
    __tablename__ = "combat_log"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    character = Column(Integer, ForeignKey("character.id"))
    monster = Column(Integer, ForeignKey("monster.id"))
    attacker = Column(Enum("character", "monster", name="attacker_enum"))
    defender = Column(Enum("character", "monster", name="defender_enum"))
    attacker_name = Column(String)
    defender_name = Column(String)
    attack_name = Column(String)

    @staticmethod
    def from_creatures(attacker, defender, attack_name: str) -> "CombatLogModel":
        from creature import Creature
        from character import Character
        attacker: Creature = attacker
        defender: Creature = defender
        character_id = attacker.id
        monster_id = defender.id
        attacker_type = "character"
        defender_type = "monster"
        if isinstance(defender, Character):
            character_id = defender.id
            monster_id = attacker.id
            attacker_type = "monster"
            defender_type = "character"

        return CombatLogModel(
            created_at=datetime.now(timezone.utc),
            character=character_id,
            monster=monster_id,
            attacker=attacker_type,
            defender=defender_type,
            attacker_name=attacker.name,
            defender_name=defender.name,
            attack_name=attack_name,
        )


class CampaignModel(Base):
    __tablename__ = "campaign"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    ended_at = Column(DateTime, nullable=True)
    version = Column(String)
    state = Column(Enum(State, name="campaign_state"))
    votes = Column(Integer)
    map = Column(JSON)
    world = Column(JSON)
    last_result = Column(String)
    root_post = Column(Integer, ForeignKey("post.id"))
    completed_post = Column(Integer, ForeignKey("post.id"))
    current_post = Column(Integer, ForeignKey("post.id"))

    @staticmethod
    def from_campaign(campaign) -> "CampaignModel":
        from campaign import Campaign
        campaign: Campaign = campaign
        return CampaignModel(
            created_at=campaign.start_time,
            ended_at=campaign.end_time,
            version=campaign.version,
            state=campaign.state,
            votes=campaign.votes,
            map=campaign.emoji_map,
            world=campaign.world,
            last_result=campaign.last_result,
            root_post=campaign.root_post_id,
            current_post=campaign.current_post_id,
            completed_post=campaign.completed_post_id,
        )


class PostModel(Base):
    __tablename__ = "post"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    parent_post = Column(Integer, ForeignKey("post.id"), nullable=True)
    child_post = Column(Integer, ForeignKey("post.id"), nullable=True)
    prompt = Column(String)
    last_result = Column(String)
    options = Column(JSON)
    option_groups = Column(JSON, nullable=True)
    multiple = Column(Boolean)
    url = Column(String)
    status_id = Column(String)


class EmojiModel(Base):
    __tablename__ = "emoji"

    id = Column(String, primary_key=True)
    emoji = Column(String)


engine: Engine
Session: sessionmaker
metadata = Base.metadata
dry = False


def serialize(obj):

    if isinstance(obj, dndclient.DNDResponse):
        return obj._dnd_data

    if isinstance(obj, enum.Enum):
        return obj.value

    if isinstance(obj, set):
        return list(obj)

    if hasattr(obj, "__dict__"):
        return obj.__dict__


def init_db() -> None:

    global dry
    global engine
    global Session
    global metadata
    if dry:
        return

    if "db_url" not in util.config or len(str(util.config["db_url"]).strip()) == 0:
        dry = True
        util.info("db_url not provided, not connecting to a database.")
        return

    try:
        # migrate db
        util.info("Migrating database...")
        alembic_config = Config(os.path.join(util.root, "config/alembic.ini"))
        command.upgrade(alembic_config, "head")

        util.info("Connecting to database...")
        DATABASE_URL = util.config["db_url"]
        engine = create_engine(
            DATABASE_URL,
            json_serializer=lambda obj: json.dumps(obj, default=serialize)
        )
        Session = sessionmaker(bind=engine)
        metadata = Base.metadata
        util.info("Successfully established database connection.")

    except Exception as e:
        util.error(f"Could not connect to database: {e}")
        dry = True
        raise ValueError("Could not connect to database. If you don't want to use a database, set db_url to an empty string in config.toml")


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def create_campaign(campaign, character) -> tuple[int, int]:
    """Insert a new campaign and character into the database.

    `campaign`: the `Campaign`.
    `character`: the `Character` of the `Campaign`.

    Returns a tuple of (campaign ID, character ID).
    """
    from character import Character
    from campaign import Campaign
    character: Character = character
    campaign: Campaign = campaign
    util.debug(f"DB: Creating campaign")
    if dry:
        return (0, 0)

    session = Session()
    new_campaign = CampaignModel.from_campaign(campaign)
    session.add(new_campaign)
    session.commit()
    campaign_id = int(new_campaign.id)
    new_character = CharacterModel.from_character(character, campaign_id)
    session.add(new_character)
    session.commit()
    character_id = int(new_character.id)
    session.close()
    return (campaign_id, character_id)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def create_monster(monster, campaign) -> int:
    """Insert a new monster and return its ID.
    """
    from dnd.monster import Monster
    from campaign import Campaign
    monster: Monster = monster
    campaign: Campaign = campaign
    if dry:
        return 0

    session = Session()
    new_monster = MonsterModel.from_monster(monster, campaign.id)
    session.add(new_monster)
    session.commit()
    monster_id = int(new_monster.id)
    session.close()
    return monster_id


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def create_combat_log(attacker, defender, attack_name: str) -> int:
    """Log a character or monster death.
    """
    from creature import Creature
    attacker: Creature = attacker
    defender: Creature = defender
    if dry:
        return 0

    try:
        session = Session()
        new_log = CombatLogModel.from_creatures(attacker, defender, attack_name)
        session.add(new_log)
        session.commit()
        log_id = int(new_log.id)
        session.close()
        return log_id
    except psycopg2.IntegrityError as e:
        # "Allow" integrity errors in case the current monster hasn't been saved
        # This isn't mission-critical data
        util.error(f"Could not update combat log due to an integrity error: {e}")
    return 0


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def create_post(
        created_at: DateTime,
        prompt: str,
        last_result: str,
        options: list,
        option_groups: list | None,
        multiple: bool,
        url: str,
        status_id: str,
        parent_id: int | None
    ) -> int:
    """Create a new post in the database.
    """
    if dry:
        return 0

    session = Session()
    new_post = PostModel(
        created_at=created_at,
        prompt=prompt,
        last_result=last_result,
        options=options,
        option_groups=option_groups,
        multiple=multiple,
        url=url,
        status_id=status_id,
        parent_post=parent_id,
    )
    session.add(new_post)
    session.commit()
    post_id = int(new_post.id)
    session.close()
    return post_id


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def update_emojis() -> None:

    if dry:
        return

    emoji_data = {}
    def recur(d: dict, path: list):
        for key in d:
            if isinstance(d[key], dict):
                recur(d[key], path + [key])
            else:
                id = "/".join(path + [key])
                emoji_data[id] = d[key]
    recur(util.emojis, [])

    session = Session()
    for key in emoji_data:
        new_emoji = EmojiModel(
            id=key,
            emoji=emoji_data[key]
        )
        session.merge(new_emoji)
    session.commit()
    session.close()


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def update_post(id: int,
        created_at: DateTime | None = None,
        prompt: str | None = None,
        last_result: str | None = None,
        options: list | None = None,
        option_groups: list | None = None,
        multiple: bool | None = None,
        url: str | None = None,
        status_id: str | None = None,
        parent_id: int | None = None,
        child_id: int | None = None
    ) -> int:
    """Update a post.
    """
    if dry:
        return 0

    session = Session()
    post: PostModel = session.query(PostModel).get(id) or PostModel(id=id)
    post.created_at = created_at if created_at is not None else post.created_at
    post.prompt = prompt if prompt is not None else post.prompt
    post.last_result = last_result if last_result is not None else post.last_result
    post.options = options if options is not None else post.options
    post.option_groups = option_groups if option_groups is not None else post.option_groups
    post.multiple = multiple if multiple is not None else post.multiple
    post.url = url if url is not None else post.url
    post.status_id = status_id if status_id is not None else post.status_id
    post.parent_post = parent_id if parent_id is not None else post.parent_post
    post.child_post = child_id if child_id is not None else post.child_post
    session.merge(post)
    session.commit()
    post_id = int(post.id)
    session.close()
    return post_id


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_last_campaign() -> CampaignModel | None:

    if dry:
        return None

    session = Session()
    campaign_model = session.query(CampaignModel).order_by(CampaignModel.id.desc()).first()
    session.close()
    return campaign_model


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((psycopg2.Error, SQLAlchemyError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def update_campaign(campaign) -> tuple[int, int, int]:
    """Insert all current campaign data into the database.

    Also updates the corresponding character's data.
    """
    from campaign import Campaign
    campaign: Campaign = campaign
    campaign_id = campaign.id
    character_id = campaign.character.id
    monster_id = campaign.monster.id if campaign.monster is not None else 0
    if dry:
        return (0, 0, 0)

    util.debug(f"Updating campaign {campaign_id}, character {character_id}, and monster {monster_id}")
    session = Session()
    campaign_row: CampaignModel = session.query(CampaignModel).get(campaign_id) or CampaignModel(id=campaign_id)
    character_row: CharacterModel = session.query(CharacterModel).get(character_id) or CharacterModel(id=character_id)
    monster_row: MonsterModel | None = session.query(MonsterModel).get(monster_id) or MonsterModel(id=monster_id) if monster_id != 0 else None

    new_campaign_data = CampaignModel.from_campaign(campaign)
    for key, value in vars(new_campaign_data).items():
        if hasattr(campaign_row, key) and key not in ["id", "character", "_sa_instance_state"]:
            setattr(campaign_row, key, value)

    new_character_data = CharacterModel.from_character(campaign.character, campaign_id)
    for key, value in vars(new_character_data).items():
        if hasattr(character_row, key) and key not in ["id", "_sa_instance_state"]:
            setattr(character_row, key, value)

    if monster_row is not None:
        new_monster_data = MonsterModel.from_monster(campaign.monster, campaign_id)
        for key, value in vars(new_monster_data).items():
            if hasattr(monster_row, key) and key not in ["id", "_sa_instance_state"]:
                setattr(monster_row, key, value)

    session.merge(campaign_row)
    session.merge(character_row)
    if monster_row is not None:
        session.merge(monster_row)
    session.commit()
    campaign_id = campaign_row.id
    character_id = character_row.id
    monster_id = monster_row.id if monster_row is not None else 0
    session.close()
    return (campaign_id, character_id, monster_id)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def upload_file(path: str, name: str):
    """Upload a file to storage.

    `path`: path of the local file to load and upload.
    `name`: the name to store the file as.
    `overwrite`: whether to overwrite existing files.
    """
    if "db_storage_url" not in util.config or len(str(util.config["db_storage_url"]).strip()) == 0:
        util.info("db_storage_url not provided, not uploading image.")
        return

    if "db_key" not in util.config or len(str(util.config["db_key"]).strip()) == 0:
        util.info("db_key not provided, not uploading image.")
        return

    url = util.config["db_storage_url"]
    key = util.config["db_key"]

    util.debug(f"Uploading file {path}")

    with open(path, "rb") as f:
        file_content = f.read()
        extension = os.path.splitext(os.path.basename(path))[1][1:]

        response = requests.post(
            f"{url}/{name}",
            headers={
                "authorization": f"Bearer {key}",
                "Content-Type": f"image/{extension}",
            },
            data=file_content,
        )

        if response.status_code == 200:
            util.debug(f"Successfully uploaded file")
            return response.json()["Key"]
        else:
            util.warn(f"Upload failed with code {response.status_code}: {response.json()}")
            util.warn(f"If you don't want to use image uploads, set db_storage_url and db_key to empty strings in config.toml")

    return None

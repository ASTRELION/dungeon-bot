# mypy: disable-error-code="union-attr"

from abc import ABC, abstractmethod
from datetime import datetime, timezone
import math
import random
import re
from typing import Any
from attacks import Attack, AttackMonster, AttackResult, AttackSpell, AttackSummary, AttackWeapon

from dice import Dice
import dnd.dndclient as dndclient
from dnd.language import Language
from dnd.spell import Spell
from effect import Effect, EffectEnd, EffectMod, EffectStat, EffectType
from dnd.equipment import Equipment
from dnd.feature import Feature
from dnd.proficiency import Proficiency
from dnd.trait import Trait
import util


class ShortRestResult:

    def __init__(self, hitpoints_gained: int, hitdice_used: int, effects_lost: list[Effect]) -> None:

        self.hitpoints_gained = hitpoints_gained
        self.hitdice_used = hitdice_used
        self.effects_lost = effects_lost


class LongRestResult:

    def __init__(self, hitpoints_gained: int, hitdice_gained: int, effects_lost: list[Effect]) -> None:

        self.hitpoints_gained = hitpoints_gained
        self.hitdice_gained = hitdice_gained
        self.effects_lost = effects_lost


class Creature(ABC):
    """Describes a Creature, being either a player Character or a Monster.
    """
    ORDERED_ABILITY_INDEXES = ("str", "dex", "con", "int", "wis", "cha")

    def __init__(self, name: str, max_hitpoints=1, proficiency_bonus=2) -> None:
        """Create a new `Creature` (a `Character` or `Monster`).

        `name`: name of the `Creature`.
        `max_hitpoints`: maximum hitpoints for the `Creature`.
        `proficiency_bonus`: initial proficiency bonus for the `Creature`.
        """
        super().__init__()

        # Creature name
        self.id = 0
        self.name = name
        self.created_at: datetime = datetime.now(timezone.utc)
        self.died_at: datetime | None = None

        self.alignment = "true neutral"
        self.size = "medium"

        # Maximum hitpoints
        self.max_hitpoints = 1
        self.set_max_hitpoints(max_hitpoints)
        # Current hitpoints
        self.hitpoints = 1
        self.set_hitpoints(self.max_hitpoints)
        # Initiative
        self.initiative = 0

        # Dict of ability: score
        self.abilities: dict[str, int] = {}
        # Dict of skill: bool, True if proficient
        self.skills: dict[str, int] = {}
        # Dict of saving throw: bool, True if proficient
        self.saving_throws: dict[str, int] = {}

        # List of proficiencies
        self.proficiencies: set[Proficiency] = set()
        # Set of Features
        self.features: set[Feature] = set()
        # List of traits
        self.traits: set[Trait] = set()
        # Proficiency bonus (based on level or CR)
        self.proficiency_bonus = proficiency_bonus

        # Set of languages known
        self.languages: set[Language] = set()

        # Set of vulnerabilities to types of damage (double damage)
        self.vulnerabilities: set[str] = set()
        # Set of resistances to types of damage (half damage)
        self.resistances: set[str] = set()
        # Set of immunities to types of damage (no damage)
        self.immunities: set[str] = set()

        # Status effects
        self.effects: list[Effect] = []

        self.spell_slots = [dndclient.SpellSlot(l) for l in range(10)]
        self.spell_slots[0].cantrip = True
        self.spells: list[set[Spell]] = [set() for _ in range(10)]
        self.spell_ability = ""
        self.spells_known = {
            "cantrips": 0,
            "spells": 0
        }

        # If the Creature is surprised or not (skips an attack)
        self.is_surprised = False
        # Prevent first death
        self.has_death_protection = False

        # Speeds
        self.walk_speed = 0
        self.swim_speed = 0
        self.fly_speed = 0
        self.climb_speed = 0
        self.burrow_speed = 0

        if not self.abilities:
            for ability in dndclient.get_abilities():
                self.abilities[ability["index"]] = 0

        if not self.saving_throws:
            for ability in dndclient.get_abilities():
                self.saving_throws[ability["index"]] = 0

        if not self.skills:
            for skill in dndclient.get_skills():
                self.skills[skill["index"]] = 0

        # features/abilities used today that can't be used again
        self.used_today: dict = {}

        self.recent_rolls: dict = {}


    def set_max_hitpoints(self, max_hitpoints: int) -> int:
        """Set the maximum amount of hitpoints the `Creature` can have.

        `max_hitpoints`: new maximum hitpoints.
        """
        self.max_hitpoints = max(1, max_hitpoints)
        return self.max_hitpoints


    def set_hitpoints(self, hitpoints: int) -> int:
        """Set current hitpoints to the given value, returning the amount gained.

        `hitpoints`: new current hitpoints, capped at `max_hitpoints`.
        """
        old_hitpoints = self.hitpoints
        self.hitpoints = min(self.max_hitpoints, hitpoints)
        return self.hitpoints - old_hitpoints


    def get_walk_speed(self) -> int:

        effects = [e for e in self.effects if e.effect_stat == EffectStat.WALK_SPEED]
        return Effect.apply_effects(self.walk_speed, effects)


    def get_fly_speed(self) -> int:

        effects = [e for e in self.effects if e.effect_stat == EffectStat.FLY_SPEED]
        return Effect.apply_effects(self.fly_speed, effects)


    def get_swim_speed(self) -> int:

        effects = [e for e in self.effects if e.effect_stat == EffectStat.SWIM_SPEED]
        return Effect.apply_effects(self.swim_speed, effects)


    def get_burrow_speed(self) -> int:

        effects = [e for e in self.effects if e.effect_stat == EffectStat.BURROW_SPEED]
        return Effect.apply_effects(self.burrow_speed, effects)


    def get_climb_speed(self) -> int:

        effects = [e for e in self.effects if e.effect_stat == EffectStat.CLIMB_SPEED]
        return Effect.apply_effects(self.climb_speed, effects)


    def set_surprised(self, is_surprised: bool = True) -> bool:
        """Set whether the Creature is surprised or not. Surprised Creatures will
        skip their next turn.

        `is_surprised`: is the Creature surprised? Defaults to True.
        """

        self.is_surprised = is_surprised
        return self.is_surprised


    def get_surprised(self) -> bool:
        """Get if the Creature is surprised.
        """
        return self.is_surprised


    def get_resistances(self) -> set[str]:
        """Get the resistances for the creature.
        """
        resistances = set(self.resistances)
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.RESISTANCE]:
            resistances.add(effect.value)

        return resistances


    def get_immunities(self) -> set[str]:
        """Get the immunities for the creature.
        """
        immunities = set(self.immunities)
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.IMMUNITY]:
            immunities.add(effect.value)

        return immunities


    def get_vulnerabilities(self) -> set[str]:
        """Get the vulnerabilities for the creature.
        """
        vulnerabilities = set(self.vulnerabilities)
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.VULNERABILITY]:
            vulnerabilities.add(effect.value)

        return vulnerabilities


    def resistant_to(self, damage_type: str | None = None, attack: Attack | None = None, weapon: Equipment | None = None, weapon_damage_type: str | None = None) -> bool:

        return self._riv_to(list(self.get_resistances()), damage_type, attack, weapon, weapon_damage_type)


    def immune_to(self, damage_type: str | None = None, attack: Attack | None = None, weapon: Equipment | None = None, weapon_damage_type: str | None = None) -> bool:

        return self._riv_to(list(self.get_immunities()), damage_type, attack, weapon, weapon_damage_type)


    def vulnerable_to(self, damage_type: str | None = None, attack: Attack | None = None, weapon: Equipment | None = None, weapon_damage_type: str | None = None) -> bool:

        return self._riv_to(list(self.get_vulnerabilities()), damage_type, attack, weapon, weapon_damage_type)


    def _riv_to(self, collection: list[str], damage_type: str | None = None, attack: Attack | None = None, weapon: Equipment | None = None, weapon_damage_type: str | None = None) -> bool:
            """Helper function for `resistant_to`, `immune_to`, and `vulnerable_to`.
            """
            if attack is not None:
                if isinstance(attack, AttackWeapon):
                    if self._riv_to(collection, weapon=attack._dnd_dict, weapon_damage_type=attack.damage_types[0]):
                        return True

                # Spells count as magical damage
                elif isinstance(attack, AttackSpell):
                    if any(self._riv_to(collection, damage_type=d) for d in attack.damage_types):
                        return True

                elif isinstance(attack, AttackMonster):
                    if any(self._riv_to(collection, damage_type=d) for d in attack.damage_types):
                        return True

            if weapon is not None and weapon_damage_type is None:
                weapon_damage_type = weapon.damage_type

            for t in collection:
                t_split = t.split(" from ")
                # conditional
                if len(t_split) > 1:
                    types = re.split(r", | and ", t_split[0])
                    condition = t_split[1]

                    to_nonmagic = "nonmagic" in condition
                    to_weapons = "weapons" in condition
                    to_attacks = "attacks" in condition
                    to_nonsilvered = "aren't silvered" in condition or "not made with silver" in condition
                    to_nonadamantine = "aren't adamantine" in condition

                    # check conditions
                    is_valid_weapon = (weapon and to_weapons and weapon.is_weapon()) or (weapon is not None and to_attacks)
                    has_damage_type = is_valid_weapon and weapon.damage_type in types
                    meets_conditions = has_damage_type and ((to_nonmagic and not weapon.magical) or not to_nonmagic)\
                        and ((to_nonsilvered and not weapon.silvered) or not to_nonsilvered)\
                        and ((to_nonadamantine and not weapon.adamantine) or not to_nonadamantine)
                    if is_valid_weapon and has_damage_type and meets_conditions:
                        return True

                    # "damage from spells" only (Archmage)
                    if isinstance(attack, AttackSpell) and "spells" in condition:
                        return True

                # unconditional
                else:
                    type = t_split[0]
                    if damage_type is not None and damage_type == type:
                        return True

                    if weapon\
                            and weapon_damage_type is not None\
                            and weapon_damage_type == type:
                        return True

            return False


    def heal(self, heal_amount: int) -> int:
        """Heal for the given amount, returning the amount gained.

        `heal_amount`: amount of hitpoints to regain.
        """
        return self.set_hitpoints(self.hitpoints + heal_amount)


    def damage(self, damage_amount: int) -> bool:
        """Take the given amount of damage, returning True if it kills the Creature.

        `damage_amount`: amount of damage to take.
        """
        remaining = self.damage_temp_hitpoints(damage_amount)
        if remaining > 0:
            self.set_hitpoints(self.hitpoints - remaining)
        return self.hitpoints <= 0


    def temp_hitpoints(self) -> int:
        """Get the temp hitpoints of the creature.
        """
        effects = [e for e in self.effects if e.effect_stat == EffectStat.TEMP_HP]
        return Effect.apply_effects(0, effects)


    def damage_temp_hitpoints(self, damage_amount: int) -> int:
        """Damage any temp hitpoints, returning the amount of damage remaining.

        `damage_amount`: amount of damage to take.
        """
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.TEMP_HP]:
            new_damage_amount = max(0, damage_amount - effect.value)
            effect.value = max(0, effect.value - damage_amount)
            damage_amount = new_damage_amount
            if effect.value <= 0:
                self.effects.remove(effect)

        return damage_amount


    def add_effect(self, effect: Effect) -> list[Effect]:
        """Add the effect to the creature.

        `effect`: the effect to add.

        Returns a list of all effects on the creature after adding.
        """
        # We need to actually modify the stat the effect is generating, so
        # collapse the stat into a single constant that we can manipulate
        if effect.effect_stat == EffectStat.TEMP_HP:
            temp_hp_effects = [e for e in self.effects if e.effect_stat == EffectStat.TEMP_HP]
            for e in temp_hp_effects: self.effects.remove(e)
            self.effects.append(Effect.reduce(0, temp_hp_effects + [effect]))
        else:
            self.effects.append(effect)

        return self.effects


    def clear_effects_end(self, effect_end: EffectEnd) -> list[Effect]:
        """Clear effects that have the specific `EffectEnd`.

        `effect_end`: the `EffectEnd` type to remove.

        Returns the `Effect`s removed, if any.
        """
        removed: list[Effect] = []
        for effect in self.effects:
            if effect.has_effect_end(effect_end):
                removed.append(effect)
                self.effects.remove(effect)

        return removed


    def clear_effects_duration(self, time: int) -> list[Effect]:
        """Apply the given time to any Effects with a duration, removing any that
        expire.

        `time`: the time to negate from Effect durations.

        Returns the `Effect`s removed, if any.
        """
        removed: list[Effect] = []
        for i, effect in enumerate(self.effects):
            if effect.duration is None: continue
            if not effect.has_effect_end(EffectEnd.DURATION): continue
            effect.duration -= time
            if effect.duration <= 0:
                removed.append(effect)
                self.effects[i] = None # type: ignore[call-overload]

        self.effects = [e for e in self.effects if e is not None]
        return removed


    def add_proficiency(self, proficiency_index: str) -> Proficiency | None:
        """Add a proficiency. If it's a skill or saving throw, add it as proficient.

        `proficiency_index`: index of the proficiency to add.
        """
        proficiency = Proficiency(dndclient.get_proficiency(proficiency_index))
        if proficiency in self.proficiencies:
            return None

        if proficiency.type == "skills":
            self.skills[proficiency.reference["index"]] = 1

        elif proficiency.type == "saving throws":
            self.saving_throws[proficiency.reference["index"]] = 1

        self.proficiencies.add(proficiency)
        return proficiency


    def has_proficiency(self, proficiency_index: str) -> bool:
        """Determine if the creature is proficient in the given proficiency.

        `proficiency_index`: the proficiency to check for.

        Returns True if proficient, False otherwise.
        """
        return any(p.index == proficiency_index for p in self.proficiencies)


    def add_feature(self, feature_index: str) -> Feature | None:
        """Add a Feature.

        `feature_index`: index of the feature to add.

        Returns the Feature added, or None if already present.
        """
        feature = Feature(dndclient.get_feature(feature_index))
        if feature in self.features:
            return None

        if feature.index in dndclient.EXTRA_ATTACK_FEATURES:
            self.add_effect(Effect(
                EffectType.POSITIVE,
                EffectMod.ADD,
                EffectStat.ACTION_COUNT,
                1,
                None,
                set([EffectEnd.NEVER]),
                feature
            ))

        self.features.add(feature)
        return feature


    def has_feature(self, feature_index: str) -> bool:
        """Determine if the `Character` has the given feature.
        """
        return any(f.index == feature_index for f in self.features)


    def add_trait(self, trait_index: str) -> Trait | None:
        """Add a trait. Also adds relevant proficiencies.

        `trait_index`: index of the trait to add.

        Returns the Trait added, or None if already present.
        """
        trait = Trait(dndclient.get_trait(trait_index))
        if trait in self.traits:
            return None

        # Add proficiencies gained
        if trait.proficiencies:
            for proficiency in trait.proficiencies:
                self.add_proficiency(proficiency["index"])

        # Choose proficiencies
        if trait.proficiency_choices:
            choose_count = int(trait.proficiency_choices["choose"])
            options = list(trait.proficiency_choices["from"]["options"])
            for _ in range(choose_count):
                choice = random.choice(options)
                options.remove(choice)
                self.add_proficiency(choice["item"]["index"])

        # Choose languages
        if trait.language_options:
            choose_count = int(trait.language_options["choose"])
            options = list(trait.language_options["from"]["options"])
            for _ in range(choose_count):
                choice = random.choice(options)
                options.remove(choice)
                self.add_language(choice["item"]["index"])

        self.traits.add(trait)
        return trait


    def has_trait(self, trait_index: str) -> bool:
        """Determine if the creature has the given trait.

        `trait_index`: index of the trait to check for.

        Returns True if the trait is present, False otherwise.
        """
        return any(t.index == trait_index for t in self.traits)


    def add_language(self, language_index: str) -> Language | None:

        language = Language(dndclient.get_language(language_index))
        if language in self.languages:
            return None

        self.languages.add(language)
        return language


    def has_language(self, language_index: str) -> bool:

        return any(l.index == language_index for l in self.languages)


    def pass_time(self, time: int) -> list[Effect]:
        """Simulates a passing of time on the creature. Mostly used for removing temporary effects.

        `time`: amount of time to pass in seconds.

        Returns effects removed.
        """
        return self.clear_effects_duration(time)


    def attack_modifier(self, ability_index: str) -> int:
        """Get the attack modifier if using the given ability.

        `ability_index`: the ability to attack with.
        """
        modifier = self.ability_modifier(ability_index)
        effects = [e for e in self.effects if e.effect_stat == EffectStat.ATTACK_ROLL]
        return Effect.apply_effects(modifier, effects)


    def damage_modifier(self, ability_index: str) -> int:
        """Get the damage modifier if using the given ability.

        `ability_index`: the ability to damage with.
        """
        modifier = self.ability_modifier(ability_index)
        effects = [e for e in self.effects if e.effect_stat == EffectStat.DAMAGE_ROLL]
        return Effect.apply_effects(modifier, effects)


    def weapon_attack_modifier(self, equipment: Equipment) -> int:
        """Get the best ability modifier for the item's attack roll.

        `equipment`: the weapon to attack with.
        """
        if not equipment.is_weapon():
            raise ValueError(f"Equipment '{self.name}' is not a weapon.")

        modifiers = equipment.get_weapon_ability()
        modifier = max([self.attack_modifier(m) for m in modifiers])
        return modifier


    def weapon_damage_modifier(self, equipment: Equipment) -> int:
        """Get the best ability modifier for the item's damage roll.

        `equipment`: the weapon to attack with.
        """
        if not equipment.is_weapon():
            raise ValueError(f"Equipment '{self.name}' is not a weapon.")

        modifiers = equipment.get_weapon_ability()
        modifier = max([self.damage_modifier(m) for m in modifiers])
        return modifier


    def weapon_max_damage(self, equipment: Equipment) -> int:
        """Get the maximum possible damage of the given weapon. Assumes a
        maximum dice roll and adds damage modifiers.

        `equipment`: the equipment (should be a weapon) to find max damage for.
        """
        if not equipment.is_weapon(): return 0
        return equipment.damage_dice.max() + self.weapon_damage_modifier(equipment)


    def ability_score(self, ability_index: str) -> int:
        """Get the ability score for the given ability, modified by any effects.

        `ability_index`: index of the ability to get.
        """
        effect_stats = set(i.value for i in EffectStat)
        effects = [e for e in self.effects if ability_index in effect_stats and e.effect_stat == EffectStat(ability_index)]
        return Effect.apply_effects(self.abilities[ability_index], effects)


    def ability_modifier(self, ability_index: str) -> int:
        """Calculate the ability modifier for the given ability.

        `ability_index`: index of the ability to get the modifier for.
        """
        ability_score = self.ability_score(ability_index)
        return math.floor((ability_score - 10) / 2)


    def ability_check(self, ability_index: str) -> int:
        """Perform an ability check for the given ability.

        `ability_index`: index of the ability to perform a check against.
        """
        roll = Dice("1d20").roll()
        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        return roll + self.ability_modifier(ability_index)


    def saving_throw(self, ability_index: str) -> int:
        """Perform a saving throw for the given ability.

        `ability_index`: index of the ability to perform a saving throw against.
        """
        roll = Dice("1d20").roll()

        advantage = 0
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.SAVING_THROW_ADVANTAGE]:
            if effect.value == ability_index:
                advantage += 1

        if advantage > 0:
            roll = Dice("1d20").roll_advantage(advantage)

        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        effects = [e for e in self.effects if e.effect_stat == EffectStat.SAVING_THROW]
        roll = Effect.apply_effects(roll, effects)

        if self.saving_throws[ability_index]:
            roll += max(self.saving_throws[ability_index], self.proficiency_bonus)

        return roll + self.ability_modifier(ability_index)


    def skill_check(self, skill_index: str) -> int:
        """Perform a skill check for the given skill.

        `skill_index`: index of the skill to perform a check against.
        """
        visible = 1
        for effect in [e for e in self.effects if e.effect_stat == EffectStat.VISIBILITY]:
            visible += 1 if effect.value else -1

        roll = Dice("1d20").roll()

        # gain advantage on stealth checks when not visible
        if skill_index == "stealth" and visible <= 0:
            roll = Dice("1d20").roll_advantage()

        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        skill_ability = dndclient.get_skill(skill_index)["ability_score"]["index"]
        if self.skills[skill_index]:
            roll += max(self.skills[skill_index], self.proficiency_bonus)

        return roll + self.ability_modifier(skill_ability)


    def ability_passive_check(self, ability_index: str) -> int:
        """Perform a passive ability check.

        `ability_index`: index of the ability to passive check against.
        """
        return 10 + self.ability_modifier(ability_index)


    def skill_passive_check(self, skill_index) -> int:
        """Perform a passive skill check.

        `skill_index`: index of the skill to passive check against.
        """
        base = 10
        skill_ability = dndclient.get_skill(skill_index)["ability_score"]["index"]
        if self.skills[skill_index]:
            base += max(self.skills[skill_index], self.proficiency_bonus)

        return base + self.ability_modifier(skill_ability)


    def roll_initiative(self) -> int:
        """Rolls initiative for the Creature, setting `_initiative` and returning the value.
        """
        initiative = self.ability_check("dex")
        self.initiative = initiative
        return self.initiative


    def get_initiative(self) -> int:
        """Get the Creature's initiative.
        """
        return self.initiative


    def hitpoint_bar(self) -> str:
        """Get the hitpoint bar for the Creature, with 1 heart representing 1/10 hitpoints.
        """
        protected = util.emojis["health"]["protected"]
        full = util.emojis["health"]["full"]
        empty = util.emojis["health"]["empty"]
        broken = util.emojis["health"]["broken"]
        temp = util.emojis["health"]["temp"]
        protected_count = 0
        full_count = max(0, self.hitpoints)
        broken_count = 0
        empty_count = self.max_hitpoints - full_count

        if self.max_hitpoints > 10:
            floor = max(0, math.floor((self.hitpoints / self.max_hitpoints) * 10))
            ceil = max(0, math.ceil((self.hitpoints / self.max_hitpoints) * 10))
            protected_count = 1 if self.hitpoints > 0 and self.has_death_protection else 0
            full_count = max(0, floor - protected_count)
            broken_count = 1 if ceil > floor and not (self.has_death_protection and floor == 0) else 0
            empty_count = 10 - full_count - broken_count - protected_count

        bar = "{}{}{}{}{} {}/{}".format(
            protected * protected_count,
            full * full_count,
            broken * broken_count,
            temp * (1 if self.temp_hitpoints() > 0 else 0),
            empty * empty_count,
            self.hitpoints,
            self.max_hitpoints,
        )
        if self.temp_hitpoints() > 0:
            bar += " + {}".format(self.temp_hitpoints())

        return bar


    def abilities_str(self) -> str:
        """Get the ability score string for the creature.

        I.e. `"Str 13 Dex 13 Con 17 Int 13 Wis 9 Cha 19"`
        """
        ability_strs = []
        for ability in self.ORDERED_ABILITY_INDEXES:
            ability_strs.append(f"{ability.title()} {self.ability_score(ability)}")
        return " ".join(ability_strs)


    def is_flier(self) -> bool:
        """Determine if the Creature is a flying creature or not.
        A creature is considered flying if its fly speed is the greatest of its speeds.
        """
        return self.fly_speed > self.walk_speed and self.fly_speed > self.swim_speed


    def is_swimmer(self) -> bool:
        """Determine if the Creature is a swimming creature or not.
        A creature is considered a swimmer if its swim speed is the greatest of its speeds.
        """
        return self.swim_speed > self.walk_speed and self.swim_speed > self.fly_speed


    def run(self, other: "Creature") -> dndclient.ActionOutcome:
        """Simulate an attempt to run from the other creature. Walk, Fly, and Swim move speeds
        are averaged and used together for the purposes of this function.

        `other`: `Creature` to try to run from.
        """
        non_zero = [speed for speed in [self.walk_speed, self.fly_speed, self.swim_speed] if speed != 0]
        self_move_speed = sum(non_zero) / len(non_zero) if non_zero else 0
        non_zero = [speed for speed in [other.walk_speed, other.fly_speed, other.swim_speed] if speed != 0]
        other_move_speed = sum(non_zero) / len(non_zero) if non_zero else 0

        diff = math.trunc((self_move_speed - other_move_speed) / 4)
        roll = max(self.skill_check("athletics"), self.skill_check("acrobatics"))
        other_roll = min(other.skill_check("athletics"), other.skill_check("acrobatics"))

        util.debug(f"Attempting to run {roll}+{diff}/{other_roll}")
        self.recent_rolls["Run"] = roll
        other.recent_rolls["Run"] = other_roll

        if roll == 20 or other_roll == 1:
            return dndclient.ActionOutcome.CRIT_SUCCESS
        elif roll == 1 or other_roll == 20:
            return dndclient.ActionOutcome.CRIT_FAILURE
        elif roll + diff >= other_roll:
            return dndclient.ActionOutcome.SUCCESS

        return dndclient.ActionOutcome.FAILURE


    def avoid(self, other: "Creature") -> dndclient.ActionOutcome:
        """Avoid another `Creature`, performing a Stealth check against the `other`'s passive Perception check.

        `other`: the `Creature` to try to avoid.
        """
        perception_check = other.skill_passive_check("perception")

        successes = 0
        failures = 0
        while successes < 3 and failures < 3:
            stealth_check = self.skill_check("stealth")
            if stealth_check == 20:
                successes += 3
            elif stealth_check == 1:
                failures += 2
            elif stealth_check > perception_check:
                successes += 1
            else:
                failures += 1

        return dndclient.ActionOutcome.SUCCESS if successes >= 3 else dndclient.ActionOutcome.FAILURE


    def get_recent_rolls(self) -> str:
        """Get most recent rolls for the `Creature` as a string.
        """
        rolls = []
        for key, value in self.recent_rolls.items():
            if not value:
                continue
            if isinstance(value, list):
                rolls.append(f"{key.title()}: {', '.join([str(v) for v in value])}")
            else:
                rolls.append(f"{key.title()}: {value}")
        return "\n".join(rolls)


    @abstractmethod
    def armor_class(self, armor: Any = None) -> int:
        """Get the Armor Class of the Creature.
        If `armor` is provided, AC is calculated as if using it (only on a Character).

        `armor`: armor to calculate the Armor Class with, instead of existing armor.
        """
        raise NotImplementedError


    @abstractmethod
    def attack(self, other: "Creature", attack_summary: AttackSummary) -> AttackResult:
        """Attack the `other` Creature, returning the damage to be done.
        This does not actually inflict damage, only calculates an attack against `other`.

        `other`: `Creature` to simulate an attack against.
        """
        raise NotImplementedError


    @abstractmethod
    def emoji(self) -> str:
        """Get the emoji associated with the Creature.
        """
        raise NotImplementedError


    @abstractmethod
    def bio(self) -> str:
        """Get a long description of the Creature. Generally seen when directly interacting with
        the Creature.
        """
        raise NotImplementedError


    @abstractmethod
    def short_bio(self) -> str:
        """Get a short description of the Creature. Generally seen as a preview before interacting
        with the Creature.
        """
        raise NotImplementedError

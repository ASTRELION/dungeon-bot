from datetime import datetime, timezone
import math
import os
import pickle
import random
from typing import Callable
from attacks import AttackResult, AttackSpell, AttackSummary
import commands

from currency import Currency, Unit
import client
import db
from dice import Dice
import dnd.dndclient as dndclient
from character import Character
from world import Location, Weather, World
from constants import Dimensions, State
from dnd.equipment import Equipment
import image
import lang
from dnd.monster import Monster
import util
from item import Item
from dnd.magic_item import RARITY_WEIGHTS, MagicItem, PotionOfFlying, PotionOfHealing, PotionOfGiantStrength, PotionOfHeroism, PotionOfInvisibility, PotionOfResistance, PotionOfSpeed


class Option():

    def __init__(self, text: str, action: Callable[..., str], description: Callable[..., image.Description]) -> None:

        self.text = text
        self.action = action
        self.description = description


    def __eq__(self, __value: object) -> bool:

        if isinstance(__value, Option):
            return self.text == __value.text and self.action == __value.action
        return False


class Campaign:
    """A single Campaign, spanning the lifetime of a single character.
    A new Campaign is created upon character death.
    """

    def __init__(self, campaign_id: int = 0, version: str = "0.0.0") -> None:
        """Create a new Campaign.

        `campaign_id`: campaign ID.
        `version`: version string this campaign uses.
        """
        self.id: int = campaign_id
        self.character: Character = None # type: ignore[assignment]
        self.monster: Monster | None = None
        self.previous_monster: Monster | None = None
        self.world = World()
        self.start_time: datetime | None = None
        self.end_time: datetime | None = None
        self.votes = 0
        self.state: State = State.BUILD
        self.emoji_map: list[str] = []
        self.version = version

        self.root_post_url = ""
        self.completed_post_url = ""
        self.current_post_url = ""

        self.root_post_id: int | None = None
        self.completed_post_id: int | None = None
        self.current_post_id: int | None = None

        self.last_result = ""
        # This is set using argparser's polls_to_auto arg
        self.skip_polls = 0
        self.skip_choice = 0
        self.polls_done = 0


    def update_command_handler(self) -> int:
        """Connect event handlers.
        """
        if not commands.dry:
            assert commands.manager_ns
            commands.manager_ns.campaign = self
        return 1


    def save(self) -> None:
        """Save Campaign object to file.
        """
        SAVE_PATH = os.path.join(util.root, "data/save")
        with open(SAVE_PATH, "wb") as file:
            pickle.dump(
                {
                    "campaign": self,
                    "status_chain": client.status_chain
                },
                file
            )
        db.update_campaign(self)


    @staticmethod
    def load() -> "Campaign":
        """Load Campaign object from file.
        """
        with open(os.path.join(util.root, "data/save"), "rb") as file:
            data = pickle.load(file)
            # Temporarily account for old save methods
            if isinstance(data, Campaign):
                return data

            campaign: Campaign
            for key in data:
                match key:
                    case "campaign":
                        campaign = data[key]
                    case "status_chain":
                        client.status_chain = data[key]
                    case _:
                        util.error(f"Unexpected data key: {key}")

            if not hasattr(campaign.character, "campaign_id"):
                campaign.character.campaign_id = campaign.id

            return campaign


    def map_append(self, emoji: str) -> None:
        """Append an emoji to the map.
        """
        self.emoji_map.append(emoji)
        while len(self.emoji_map) > 10:
            self.emoji_map.pop(0)


    def map_str(self) -> str:
        """Get the map as a str.
        """
        return "".join(self.emoji_map)


    def poll(
            self,
            text: str,
            last_result: str,
            options: list[str],
            chain = False,
            option_groups: list | None = None,
            multiple = 1,
            dry_skip: list[int] | None = None,
            duration: int | None = None,
        ) -> list[int]:
        """Post a poll and get the result. Returns index in `options` that wins.

        `text`: the body of the poll.
        `last_result`: the last result of the previous poll (prepended to `text`)
        `options`: poll options.
        `chain`: whether to reply to the previous poll or not.
        `option_groups`: option groupings for winner determination.
        `multiple`: how many options that can be chosen
        `dry_skip`: list of indices in `options` to not choose if running `--dry`.
        """
        is_multiple = multiple > 1

        status = client.post_poll(self.id, text, last_result, options, is_multiple=is_multiple, duration=duration)
        post_id = db.create_post(
            status["created_at"],
            text,
            last_result,
            options,
            option_groups,
            is_multiple,
            status["url"],
            status["id"],
            self.completed_post_id
        )
        if self.completed_post_id is not None:
            db.update_post(self.completed_post_id, child_id=post_id)

        if not self.root_post_url:
            self.root_post_url = status["url"]
        if self.root_post_id is None:
            self.root_post_id = post_id

        self.current_post_url = status["url"]
        self.current_post_id = post_id

        if self.character:
            db.update_campaign(self)

        self.update_command_handler()

        result: tuple | None = None
        if self.polls_done < self.skip_polls:
            self.polls_done += 1
            result = ([self.skip_choice], 0, {"id": 0, "url": ""})
        else:
            result = client.poll_result(status["id"], chain, option_groups, dry_skip, multiple=multiple)

        while result is None:
            util.debug("Poll has no votes, reposting...")
            status = client.post_poll(self.id, text, last_result, options, is_multiple=is_multiple, duration=int(util.config["no_vote_duration"]))
            self.current_post_url = status["url"]
            db.update_post(post_id, status_id=status["id"], url=status["url"])
            result = client.poll_result(status["id"], chain, option_groups, dry_skip, multiple=multiple)

        result_status = result[2]
        db.update_post(post_id, status_id=result_status["id"], url=result_status["url"])
        self.current_post_url = status["url"] # roll_result might change status, so we need to update this too
        self.completed_post_url = status["url"]
        self.completed_post_id = post_id
        self.votes += result[1]
        return result[0]


    def start(self, initial_text = "") -> None:
        """Start the Campaign loop.

        `result`: the message string of the last poll result.
        """
        util.info(f"STARTING CAMPAIGN #{self.id}")

        STATE_MAP = {
            State.BUILD: self.build,
            State.ADVENTURE: self.adventure,
            State.FIGHT: self.fight,
            State.TOWN: self.town,
            State.SHOP: self.shop,
            State.TRADER: self.trader,
            State.TREASURE: self.treasure
        }

        if initial_text:
            self.last_result = f"{initial_text}\n{self.last_result}"

        while self.state != State.END:

            self.last_result = STATE_MAP[self.state]()

            if self.character:
                client.update_profile(self.bio())

            self.save()
            self.update_command_handler()

        self.end()
        self.save()
        self.update_command_handler()

#region states

    def build(self) -> str:
        """Build a new Character.
        """
        # Generate character options
        util.info("Creating character options")
        options: list[Option] = []
        followers = list(client.get_followers())
        followers_copy = followers.copy()
        classes = list(dndclient.get_classes())
        races = list(dndclient.get_races())
        subclasses = dndclient.get_subclasses()
        subraces = dndclient.get_subraces()
        c_max = max([len(c["name"]) for c in classes])
        sc_max = max([len(sc["name"]) for sc in subclasses])
        r_max = max([len(r["name"]) for r in races])
        sr_max = max([len(sr["name"]) for sr in subraces])
        max_name_length = client.POLL_OPTION_LIMIT - max(c_max, sc_max) - max(r_max, sr_max) - 11

        for _ in range(client.MAX_POLL_SIZE):
            name = ""
            # choose a unique random name from follower list if available
            while len(name) < 2 and followers:
                choice = random.choice(followers)
                followers.remove(choice)
                name = choice.split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            if not name:
                name = random.choice(followers_copy).split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            name = name[:max_name_length]

            # try to have unique classes/races
            race: dndclient.DNDResponse
            clazz: dndclient.DNDResponse

            if races:
                race = random.choice(races)
                races.remove(race)
                race = dndclient.get_race(race["index"])

            if classes:
                clazz = random.choice(classes)
                classes.remove(clazz)
                clazz = dndclient.get_class(clazz["index"])

            options.append(self.option_character(Character(self.id, name, race, clazz)))

        # Poll
        util.info("Polling for character selection...")

        prompt = f"{util.emojis['campaign']['id']} Campaign {self.id} {client.HASHTAG_CAMPAIGN_START}\n\n"
        prompt += lang.create.prompt.choose()

        client.reset_avatar()
        client.reset_header()
        image.delete_generated()

        result = self.poll(prompt, self.last_result, [o.text for o in options], chain=False, duration=int(util.config["create_duration"]))
        action_result = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action_result


    def fight(self) -> str:
        """Fight a Monster.
        """
        assert self.monster
        util.info(f"Fighting {self.monster.name}")

        attack_options = self.character.attack_options(client.MAX_POLL_SIZE - 1, self.monster)
        options: list[Option] = [self.option_attack_option(a) for a in attack_options]
        options.append(self.option_run(self.monster))

        result = self.poll(self.monster.bio(self.character.level), self.last_result, [o.text for o in options])
        action = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action


    def trader(self) -> str:
        """Stranger interaction turns out to be a wandering trader.
        """
        # We don't care about fair spread for the trader like we do for the
        # shop, so just give a random assortment of items.
        weapon_options = self.character.proficient_weapons()
        armor_options = self.character.proficient_armor()
        item_options: list[Item] = list(weapon_options + armor_options)
        options: list[Option] = []

        if self.character.hitpoints < self.character.max_hitpoints:
            item_options += PotionOfHealing.all()
        item_options += PotionOfGiantStrength.all()
        item_options += PotionOfFlying.all()
        item_options += PotionOfHeroism.all()
        item_options += PotionOfInvisibility.all()
        item_options += PotionOfResistance.all()
        item_options += PotionOfSpeed.all()

        skill_check = self.character.skill_check("persuasion")
        buy_scale = 1.0 - (0.25 * util.clamp(skill_check / 20, 0, 1)) # up to 25% off

        trader_size = client.MAX_POLL_SIZE - 1

        possible_options = []
        while len(item_options) > 0:
            item = random.choice(item_options)
            item_options.remove(item)
            cost = item.cost
            if isinstance(item, Equipment):
                unequipped = self.character.equip_equipment(item, True, True)
                cost = Equipment.net_spend(unequipped, [item], buy_scale=buy_scale).rounded()
                if item.is_weapon():
                    if ((len(self.character.hands) == 2
                            and all(h.is_weapon() for h in self.character.hands)
                            and all([item.index == w.index for w in self.character.hands]))
                        or (all(not h.has_property("light") for h in self.character.hands)
                            and any(h.index == item.index for h in self.character.hands))
                        ):
                        continue
                if item.is_armor():
                    can_carry = item.str_minimum <= self.character.abilities["str"]
                    if (not can_carry
                            or all([item.index == a.index for a in self.character.armor])
                            or (item.is_shield() and any([h.is_shield() for h in self.character.hands]))):
                        continue

            if (cost * buy_scale).rounded() <= self.character.currency:
                possible_options.append(self.option_buy_item(item, buy_scale=buy_scale))

        if self.character.currency.amount >= 100:
            for hand in self.character.hands:
                if hand.silvered: continue
                possible_options.append(self.option_silver(hand))
                break

        while trader_size > 0 and len(possible_options) > 1:
            option = random.choice(possible_options)
            possible_options.remove(option)
            options.append(option)
            trader_size -= 1

        if not options:
            self.state = State.ADVENTURE
            return lang.trader.result.empty.choose()

        options.append(self.option_leave())

        current = util.emojis["equipment"]["inventory"]
        current += f"\n{self.character.hand_str()}"
        current += f" {self.character.armor_str()}"
        result = self.poll(f"{lang.trader.prompt.choose()}\n{current}", self.last_result, [o.text for o in options])
        action = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action


    def treasure(self) -> str:
        """Interact with a treasure chest.
        """
        options: list[Option] = []

        possible_potions: list[MagicItem] = []
        if self.character.hitpoints < self.character.max_hitpoints:
            possible_potions.extend(PotionOfHealing.all())
        possible_potions.extend(PotionOfGiantStrength.all())
        possible_potions.extend(PotionOfFlying.all())
        possible_potions.extend(PotionOfHeroism.all())
        possible_potions.extend(PotionOfInvisibility.all())
        possible_potions.extend(PotionOfResistance.all())
        possible_potions.extend(PotionOfSpeed.all())
        weights = [RARITY_WEIGHTS[p.rarity] for p in possible_potions]

        roll = Dice(self.character.skill_check("investigation"), 8)
        options.append(self.option_take_item(Currency(roll.roll(), Unit.gp)))

        for _ in range(1, client.MAX_POLL_SIZE - 1):
            if len(possible_potions) == 0: continue
            potion = random.choices(possible_potions, weights=weights, k=1)[0]
            i = possible_potions.index(potion)
            possible_potions.pop(i)
            weights.pop(i)
            options.append(self.option_take_item(potion))

        skill_check = self.character.skill_check("investigation")
        treasure_count = max(1, round((min(20, skill_check) / 20) * len(options)))
        options.append(self.option_leave())
        leave_index = len(options) - 1

        result = self.poll(lang.treasure.prompt.choose(treasure_count), self.last_result, [o.text for o in options], multiple=treasure_count)

        # if the highest voted option was leave, skip all others
        if result[0] == leave_index:
            action = options[result[0]].action()
            client.start_image_reply_gen(self.character, self, options[result[0]].description())
            return action

        # otherwise, take until we hit leave or end of queue
        action_results: list[str] = []
        for winner in result:
            if winner == leave_index:
                client.start_image_reply_gen(self.character, self, options[result[0]].description())
                return "\n".join(action_results)
            action_results.append(options[winner].action())
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return "\n".join(action_results)


    def adventure(self) -> str:
        """Adventure beyond safety and explore.
        """
        max_monster_cr = dndclient.xp_to_max_cr(self.character.xp)
        monster = None
        mimic = Monster(dndclient.get_monster("mimic"))
        prompt = ""
        interaction = random.random()

        option_groups: list = []
        options: list[Option] = []

        can_rest = self.character.hit_dice > 0\
            or self.character.has_feature("pact-magic")\
            or self.character.has_feature("arcane-recovery")

        chest_chance = float(util.config["treasure_chance"])
        if mimic.challenge_rating > max_monster_cr:
            chest_chance /= 2
        stranger_chance = float(util.config["trader_chance"])
        util.debug(f"Choosing interaction: {interaction} {chest_chance} {stranger_chance}")

        # Chest
        if interaction >= 1.0 - chest_chance:
            mimic_chance = 0.5
            if mimic.challenge_rating > max_monster_cr:
                mimic_chance = 0
            mimic_skill_check = mimic.skill_check("stealth")
            character_skill_check = self.character.skill_check("perception")

            if random.random() <= mimic_chance:
                monster = mimic

            prompt = lang.chest.prompt.choose()
            if character_skill_check >= mimic_skill_check:
                # mimic
                if monster:
                    prompt = lang.chest.mimic.prompt.choose()
                # treasure
                else:
                    prompt = lang.chest.treasure.prompt.choose()

            self.state = State.ADVENTURE_CHEST
            option_groups = [[0, 1], [2, 3]]
            options += [
                self.option_attack(monster),
                self.option_investigate(monster)
            ]
            if not can_rest:
                options.append(self.option_avoid(None, "chest"))

        # Stranger
        # TODO: base stranger chance on gold amount instead of just bandit
        elif interaction >= 1.0 - chest_chance - stranger_chance:
            # https://www.desmos.com/calculator/mssjrefci6
            gold_threshold = self.character.GOLD_ROLL.max() - self.character.GOLD_ROLL.mean()
            bandit_chance = (math.atan(max(self.character.currency.amount - gold_threshold, 0) / 100) / (0.8 * math.pi)) + 0.2
            bandit_skill_check = "persuasion"
            spawn_bandit = random.random() <= bandit_chance

            # spawn bandit
            if spawn_bandit:
                bandit_captain = Monster(dndclient.get_monster("bandit"))
                bandit = Monster(dndclient.get_monster("bandit"))
                monster = bandit
                if bandit_captain.challenge_rating <= max_monster_cr:
                    monster = random.choice([bandit, bandit_captain])
                bandit_skill_check = "deception"

            # spawn trader
            else:
                monster = Monster(dndclient.get_monster("commoner"))
                monster.unique_name = client.get_supporter_name()

            character_check = self.character.skill_check("perception")
            monster_check = monster.skill_check(bandit_skill_check)

            prompt = lang.stranger.prompt.choose()
            if character_check >= monster_check:
                if spawn_bandit:
                    prompt = lang.stranger.bandit.prompt.choose()
                else:
                    prompt = lang.stranger.trader.prompt.choose()

            self.state = State.ADVENTURE_STRANGER
            option_groups = [[0, 1], [2, 3]]
            options += [
                self.option_attack(monster),
                self.option_talk(monster)
            ]
            if not can_rest:
                options.append(self.option_avoid(None, "stranger"))

        # Fight
        else:
            monster = Monster(dndclient.get_monster_lower(max_monster_cr))
            prompt = lang.fight.prompt.choose(monster.short_bio(self.character.level))
            option_groups = [[0], [1, 2]]
            if can_rest:
                option_groups = [[0], [[1], [2, 3]]]
            self.state = State.ADVENTURE_MONSTER
            options += [
                self.option_fight(monster),
                self.option_avoid(monster)
            ]

        if can_rest:
            options.append(self.option_short_rest())

        options.append(self.option_town())

        result = self.poll(prompt, self.last_result, [o.text for o in options], option_groups=option_groups)
        action = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action


    def town(self) -> str:
        """Rest in a town or shop.
        """
        options = [
            self.option_adventure(),
            self.option_shop(),
            self.option_long_rest(),
            self.option_retire()
        ]

        result = self.poll(lang.town.prompt.choose(), self.last_result, [o.text for o in options], option_groups=[[0, 1, 2]], dry_skip=[3])
        action = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action


    def shop(self) -> str:
        """Shop for new equipment.
        """
        options: list[Option] = []
        weapon_options = self.character.proficient_weapons()
        armor_options = self.character.proficient_armor()

        sell_size = int(
            (self.character.has_feature("barbarian-unarmored-defense")
            or self.character.has_feature("monk-unarmed-defense"))
            and len(self.character.armor) > 0
        )
        shop_size = client.MAX_POLL_SIZE - sell_size - 1
        weapon_count = 0
        if not armor_options:
            weapon_count = min(len(weapon_options), shop_size)
        else:
            weapon_count = min(len(weapon_options), random.randint(1, math.ceil(shop_size / 2)))
        armor_count = min(len(armor_options), shop_size - weapon_count)

        for _ in range(weapon_count):
            weapon = None
            cost = Currency(0)
            while not weapon and weapon_options:
                weapon = random.choice(weapon_options)
                weapon_options.remove(weapon)
                unequipped = self.character.equip_equipment(weapon, True, True)
                cost = Equipment.net_spend(unequipped, [weapon]).rounded()
                if (cost > self.character.currency
                        or (len(self.character.hands) == 2
                            and all(h.is_weapon() for h in self.character.hands)
                            and all([weapon.index == w.index for w in self.character.hands]))
                        or (all(not h.has_property("light") for h in self.character.hands)
                            and any(h.index == weapon.index for h in self.character.hands))
                    ):
                    weapon = None

            if weapon:
                options.append(self.option_buy_item(weapon))

        for _ in range(armor_count):
            armor = None
            cost = Currency(0)
            while (not armor or armor == self.character.armor) and armor_options:
                armor = random.choice(armor_options)
                armor_options.remove(armor)
                unequipped = self.character.equip_equipment(armor, True, True)
                cost = Equipment.net_spend(unequipped, [armor]).rounded()
                can_carry = armor.str_minimum <= self.character.abilities["str"]
                if (cost > self.character.currency
                        or not can_carry
                        or all([armor.index == a.index for a in self.character.armor])
                        or (armor.is_shield() and any([h.is_shield() for h in self.character.hands]))):
                    armor = None

            if armor:
                options.append(self.option_buy_item(armor))

        if sell_size > 0:
            options.append(self.option_sell_armor())

        if not options:
            self.state = State.TOWN
            return lang.shop.result.empty.choose()

        options.append(self.option_leave())

        current = util.emojis["equipment"]["inventory"]
        current += f"\n{self.character.hand_str()}"
        current += f" {self.character.armor_str()}"

        name = client.get_supporter_name()
        prompt = lang.shop.prompt.choose()
        if name:
            prompt = lang.shop.prompt.named.choose(name)

        result = self.poll(f"{prompt}\n{current}", self.last_result, [o.text for o in options])
        action = options[result[0]].action()
        client.start_image_reply_gen(self.character, self, options[result[0]].description())
        return action


    def end(self) -> None:
        """End the campaign, resulting in a death message.
        """
        util.info("Campaign ended.")
        self.map_append(util.emojis["health"]["death"])
        NOW = datetime.now(timezone.utc)
        self._ended_at = NOW

        if not self.character:
            return

        self.character.died_at = NOW

        assert self.start_time
        delta = NOW - self.start_time
        bio = "{}\n\n{}\n{} {} {} {} {} {}\n{}".format(
            self.character.short_bio(),
            self.map_str(),
            util.emojis["campaign"]["id"],
            self.id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds()),
            util.emojis["campaign"]["user"],
            self.votes,
            client.HASHTAG_CAMPAIGN_END
        )

        status = client.post_status(
            self.id,
            bio,
            self.last_result,
            image_path=image.AI_AVATAR_ORIGINAL_PATH,
            image_alt="AI-generated Character avatar",
            is_story=True
        )
        post_id = db.create_post(status["created_at"], bio, self.last_result, [], None, False, status["url"], status["id"], self.completed_post_id)
        assert self.completed_post_id is not None
        db.update_post(self.completed_post_id, child_id=post_id)

#endregion
#region options

    def option_character(self, character: Character) -> Option:

        def action() -> str:
            self.character = character
            self.character.init()

            self.start_time = datetime.now(timezone.utc)

            if util.config["start_exp"] > 0:
                self.character.add_experience(util.config["start_exp"])
                self.character.long_rest()
            client.update_profile(self.bio())

            campaign_id, character_id = db.create_campaign(self, self.character)
            if campaign_id != 0:
                self.id = campaign_id
            if character_id != 0:
                self.character.id = character_id
            else:
                self.character.id = self.id
            client.start_image_gen(self.character, self)

            self.map_append(self.character.emoji())
            self.state = State.TOWN
            return lang.create.result.choose(character.name)

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="arriving in a town for the first time"
            )

        return Option(
            lang.options.character.choose(
                character.emoji(), character.title()
            ),
            action,
            description,
        )


    def option_adventure(self) -> Option:

        def action() -> str:
            self.state = State.ADVENTURE
            self.world.reroll_location()
            self.world.reroll_weather()
            return lang.adventure.result.choose()

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="leaving the city gates",
                dimensions=Dimensions.TALL,
                location=Location.none()
            )

        return Option(
            lang.options.adventure.choose(
                util.emojis["prompts"]["adventure"]
            ),
            action,
            description,
        )


    def option_buy_item(self, item: Item, buy_scale = 1.0, sell_scale = 0.5) -> Option:

        cost = (item.cost * buy_scale).rounded()
        if isinstance(item, Equipment):
            cost = Equipment.net_spend(
                self.character.equip_equipment(item, True, True),
                [item],
                buy_scale,
                sell_scale
            ).rounded()

        def action() -> str:
            if not isinstance(item, Currency):
                self.character.spend(cost)
            equip_result = self.character.equip_item(item, True)

            match self.state:
                case State.SHOP:
                    self.state = State.TOWN
                    return lang.purchase.result.choose(item.nice_name())

                case State.TRADER:
                    self.state = State.ADVENTURE
                    match item:
                        case Equipment():
                            return lang.trader.result.choose(
                                item.nice_name()
                            )

                        case PotionOfHealing()\
                                | PotionOfGiantStrength()\
                                | PotionOfFlying()\
                                | PotionOfHeroism()\
                                | PotionOfInvisibility()\
                                | PotionOfResistance()\
                                | PotionOfSpeed():
                            return lang.trader.result.effect.choose(
                                item.nice_name(),
                                item.effect_str
                            )

                        case _:
                            raise ValueError(f"Invalid item {item}")

                case _:
                    raise ValueError(f"Invalid state {self.state}")

        def description() -> image.Description:

            desc = image.Description(
                world=self.world,
                text=f"{item.nice_name()}",
                hero=False,
                weather=Weather.none(),
                location=Location("inside a shop"),
            )

            match item:
                case Equipment():
                    if item.is_weapon():
                        desc.text += " weapon"
                    elif item.is_armor() and not item.is_shield():
                        desc.text += " armor"
                case MagicItem():
                    desc.text += " magic item"
            # This makes the generator pick a random option from case, rack, pedestal
            desc.text += " on a display {case|rack|pedestal}"
            return desc

        text = ""
        match item:
            case Equipment():
                if item.is_weapon():
                    text = "{} {} [{}] ~ {} ({:+} {})".format(
                        item.get_emoji(),
                        item.nice_name(),
                        self.character.weapon_dice_str(item, item.damage_dice),
                        " ".join(item.get_property_emojis()),
                        -int(cost.amount),
                        util.emojis["equipment"]["gold"]
                    )
                elif item.is_armor():
                    ac = f"[{self.character.armor_class([item])}]"
                    if item.is_shield():
                        ac = "[+{}] ~ {}".format(
                            item.armor_class,
                            util.emojis["weapon_properties"]["one-handed"]
                        )
                    text = "{} {} {} ({:+} {})".format(
                        item.get_emoji(),
                        item.nice_name(),
                        ac,
                        -int(cost.amount),
                        util.emojis["equipment"]["gold"]
                    )

            case MagicItem():
                text = "{} {} ({:+} {})".format(
                    item.emoji,
                    item.name,
                    -int(cost.amount),
                    cost.emoji
                )

        return Option(
            lang.options.buy_item.choose(
                text
            ),
            action,
            description,
        )


    def option_take_item(self, item: Item) -> Option:

        def action():
            equip_result = self.character.equip_item(item)
            self.state = State.ADVENTURE

            match item:
                case Currency():
                    return lang.treasure.result.choose(f"{int(item.amount)} GP")

                case Equipment():
                    return lang.treasure.result.choose(f"{item.nice_name()}")

                case PotionOfHealing()\
                        | PotionOfGiantStrength()\
                        | PotionOfFlying()\
                        | PotionOfHeroism()\
                        | PotionOfInvisibility()\
                        | PotionOfResistance()\
                        | PotionOfSpeed():
                    return lang.treasure.result.effect.choose(
                        item.nice_name(),
                        item.effect_str
                    )

                case _:
                    raise TypeError(f"Invalid Item type {type(item)}.")

        def description() -> image.Description:
            desc = image.Description(
                world=self.world,
                hero=False
            )
            match item:
                case Currency():
                    desc.text = "a pile of gold coins {on the ground|in a chest|in a pouch}"
                    return desc

                case Equipment():
                    desc.text = f"{item.nice_name()} on the ground"
                    return desc

                case PotionOfHealing()\
                        | PotionOfGiantStrength()\
                        | PotionOfFlying()\
                        | PotionOfHeroism()\
                        | PotionOfInvisibility()\
                        | PotionOfResistance()\
                        | PotionOfSpeed():
                    desc.text = f"{item.nice_name()} on the ground"
                    return desc

                case _:
                    raise TypeError(f"Invalid Item type {type(item)}.")

        text = ""
        match item:
            case Currency():
                text = lang.options.take_item.choose(
                    util.emojis["equipment"]["gold"],
                    f"{int(item.amount)} GP"
                )

            case MagicItem():
                text = lang.options.take_item.choose(
                    util.emojis["equipment"]["potion"],
                    item.nice_name()
                )

            case _:
                raise TypeError(f"Invalid Item type {type(item)}.")

        return Option(
            text,
            action,
            description,
        )


    def option_silver(self, equipment: Equipment) -> Option:

        SILVER_COST = Currency(100)

        def action():

            for hand in self.character.hands:
                if hand.silvered: continue
                self.character.spend(SILVER_COST)
                hand.set_silvered(True)
                self.state = State.ADVENTURE
                return lang.trader.result.silver.choose(hand.nice_name())

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="silvering a weapon",
                weapon=False
            )

        return Option(
            lang.options.silver.choose(
                util.emojis["equipment"]["silver"],
                "{} {}".format(-int(SILVER_COST.amount), util.emojis["equipment"]["gold"])
            ),
            action,
            description,
        )


    def option_sell_armor(self, sell_scale = 0.5) -> Option:

        current_armor = self.character.armor
        result_ac = self.character.armor_class([])
        net_gold = Equipment.net_spend(current_armor, [], sell_scale=sell_scale)

        def action():
            self.state = State.TOWN
            self.character.armor = []
            self.character.spend(net_gold)
            return lang.sell.result.armor.choose(
                -int(net_gold.amount),
                util.emojis["equipment"]["gold"]
            )

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="selling a piece of armor",
                location=Location("inside a merchant shop"),
                weapon=False
            )

        return Option(
            lang.options.sell_armor.choose(
                util.emojis["equipment"]["money"],
                result_ac,
                "{:+}".format(-int(net_gold.amount)),
                util.emojis["equipment"]["gold"]
            ),
            action,
            description,
        )


    def option_shop(self) -> Option:

        def action() -> str:
            self.state = State.SHOP
            return lang.shop.result.choose()

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="enters a medieval merchant shop, walking through the doors",
                dimensions=Dimensions.TALL,
                location=Location("inside a merchant shop"),
                weapon=False
            )

        return Option(
            lang.options.shop.choose(
                util.emojis["prompts"]["shop"]
            ),
            action,
            description,
        )


    def option_short_rest(self) -> Option:

        def action() -> str:
            hit_dice = self.character.hit_dice
            short_rest_result = self.character.short_rest()
            self.state = State.ADVENTURE
            self.world.reroll_weather()

            result_strs = []

            if hit_dice > 0:
                result_strs.append(
                    lang.rest_short.result.choose(short_rest_result.hitpoints_gained)
                )
            else:
                result_strs.append(lang.rest_short.result.no_hit_dice.choose())

            if len(short_rest_result.effects_lost) > 0:
                result_strs.append(
                    lang.rest_short.result.effects.choose(len(short_rest_result.effects_lost))
                )

            return "\n".join(result_strs)

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="taking a short rest, leaning against a {rock|tree}",
                dimensions=Dimensions.WIDE,
                weapon=False
            )

        return Option(
            lang.options.short_rest.choose(
                util.emojis["prompts"]["short_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            action,
            description,
        )


    def option_long_rest(self) -> Option:

        def action() -> str:
            long_rest_result = self.character.long_rest()
            self.state = State.TOWN
            self.world.reroll_weather()
            result_strs = [
                lang.rest_long.result.choose(
                    long_rest_result.hitpoints_gained,
                    long_rest_result.hitdice_gained
                )
            ]
            if len(long_rest_result.effects_lost) > 0:
                result_strs.append(
                    lang.rest_long.result.effects.choose(
                        len(long_rest_result.effects_lost)
                    )
                )

            return "\n".join(result_strs)

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="sleeping by the campfire",
                dimensions=(1152, 896),
                weapon=False
            )

        return Option(
            lang.options.long_rest.choose(
                util.emojis["prompts"]["long_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            action,
            description,
        )


    def option_retire(self) -> Option:

        def action() -> str:
            self.state = State.END
            return lang.retire.choose()

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="Walking away into the sunset",
                dimensions=Dimensions.ULTRA_WIDE
            )

        return Option(
            lang.options.retire.choose(
                util.emojis["prompts"]["retire"]
            ),
            action,
            description,
        )


    def option_town(self, text: str | None = None) -> Option:

        def action() -> str:
            if self.state == State.SHOP:
                self.state = State.TOWN
                return lang.shop.result.left.choose()
            else:
                self.map_append(util.emojis["prompts"]["town"])
                self.state = State.TOWN
                return lang.town.result.choose()

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="enters the town gates",
                dimensions=Dimensions.TALL,
                weapon=False
            )

        if text is None:
            text = lang.options.town.choose(
                util.emojis["prompts"]["town"]
            )
        return Option(text, action, description)


    def option_fight(self, monster: Monster) -> Option:

        def action() -> str:
            mi = monster.roll_initiative()
            ci = self.character.roll_initiative()
            util.debug(f"MI: {mi}, CI: {ci}")
            self.monster = monster
            self.character.recent_rolls["Initiative"] = ci
            self.monster.recent_rolls["Initiative"] = mi
            self.monster.id = db.create_monster(self.monster, self)
            self.map_append(monster.emoji())
            self.state = State.FIGHT
            return lang.fight.result.choose(monster.name)

        def description() -> image.Description:

            assert self.monster
            monster_type_desc = self.monster.monster_type
            if monster_type_desc in ["humanoid", "beast"]:
                monster_type_desc = ""
            return image.Description(
                world=self.world,
                hero=False,
                text=(
                    f"{self.monster.description_modifier} "
                    f"{self.monster.name} {monster_type_desc} in the wilderness"
                ),
                dimensions=Dimensions.SQUARE,
                style=f"fantasy encounter"
            )

        return Option(
            lang.options.fight.choose(
                util.emojis["prompts"]["fight"]
            ),
            action,
            description,
        )


    def option_attack(self, monster: Monster | None) -> Option:
        """Attack a monster. Attack is different than Fight because it is
        not necessarily prompted, while a Fight is mutual.
        """
        def action() -> str:
            # Chest
            if self.state == State.ADVENTURE_CHEST:
                # mimic
                if monster and "mimic" in monster.index:
                    self.option_fight(monster).action()
                    assert self.monster
                    self.monster.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.chest.mimic.result.attack.choose()
                # treasure
                else:
                    self.state = State.ADVENTURE
                    return lang.chest.treasure.result.attack.choose()

            # Stranger
            elif self.state == State.ADVENTURE_STRANGER:
                # bandit
                if monster and "bandit" in monster.index:
                    self.option_fight(monster).action()
                    assert self.monster
                    self.monster.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.stranger.bandit.result.attack.choose()
                # trader
                else:
                    self.state = State.ADVENTURE
                    return lang.stranger.trader.result.attack.choose()

            else:
                raise ValueError(f"Invalid parent state {self.state}")

        def description() -> image.Description:

            desc = image.Description(
                world=self.world,
                text="attacking a random object",
                dimensions=Dimensions.WIDE
            )
            if monster:
                desc.text = f"attacking a {monster.name}"
                desc.style = f"Hero vs {monster.monster_type} one-on-one fight"
            return desc

        return Option(
            lang.options.attack.choose(
                util.emojis["prompts"]["fight"]
            ),
            action,
            description,
        )


    def option_talk(self, monster: Monster) -> Option:

        def action() -> str:
            # Stranger
            if self.state == State.ADVENTURE_STRANGER:
                # bandit
                if "bandit" in monster.index:
                    self.option_fight(monster).action()
                    self.character.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.stranger.bandit.result.talk.choose()
                # trader
                else:
                    self.map_append(monster.emoji())
                    self.state = State.TRADER

                    result = lang.stranger.trader.result.talk.choose()
                    if monster.unique_name:
                        result = lang.stranger.trader.result.talk_named.choose(monster.unique_name)

                    return result

            else:
                raise ValueError(f"Invalid parent state {self.state}")

        def description() -> image.Description:
            #We remove these descriptions, as it leads to always making monsters
            monster_type_desc = monster.monster_type
            if monster_type_desc in ["humanoid", "beast"]:
                monster_type_desc = ""
            return image.Description(
                world=self.world,
                text=(
                    f"{monster.description_modifier} "
                    f"{monster.name} {monster_type_desc}"
                ),
                dimensions=Dimensions.SQUARE,
                style=f"Fantasy peaceful interaction",
                weapon=False,
                hero=False
            )

        return Option(
            lang.options.talk.choose(
                util.emojis["skills"]["persuasion"]
            ),
            action,
            description,
        )


    def option_investigate(self, monster: Monster | None) -> Option:

        def action() -> str:
            if monster and "mimic" in monster.index:
                self.state = State.FIGHT
                self.option_fight(monster).action()
                self.character.set_surprised()
                self.map_append(monster.emoji())
                return lang.chest.mimic.result.investigate.choose()
            else:
                self.map_append(util.emojis["equipment"]["chest"])
                self.state = State.TREASURE
                return lang.chest.treasure.result.investigate.choose()

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text="stealthily investigating a location",
                dimensions=Dimensions.TALL
            )

        return Option(
            lang.options.investigate.choose(
                util.emojis["skills"]["perception"]
            ),
            action,
            description,
        )


    def option_attack_option(self, attack_summary: AttackSummary) -> Option:

        character_attacks: AttackResult | None = None
        monster_attacks: AttackResult | None = None

        def action() -> str:
            nonlocal character_attacks, monster_attacks
            attack_summaries = []
            # order attacks
            assert self.monster
            creatures: tuple = (self.character, self.monster)
            while creatures[0].get_initiative() == creatures[1].get_initiative():
                creatures[0].roll_initiative()
                creatures[1].roll_initiative()
            creatures = tuple(sorted(creatures, key=lambda c: c.get_initiative(), reverse=True))
            util.debug(f"Combat order: {creatures}")

            # nullify surprise if both creatures are surprised
            # https://i.kym-cdn.com/entries/icons/original/000/023/397/C-658VsXoAo3ovC.jpg
            if all(c.get_surprised() for c in creatures):
                for c in creatures: c.set_surprised(False)

            # roll in pairs until someone hits
            attack_results: list[AttackResult] = []
            while not attack_results or all(a.attack_hit.value <= 0 for a in attack_results):
                attack_results.clear()
                # character attacks first
                if isinstance(creatures[0], Character):
                    attack_results.append(creatures[0].attack(creatures[1], attack_summary))
                    attack_results.append(creatures[1].attack(creatures[0]))
                # monster attacks first
                else:
                    attack_results.append(creatures[0].attack(creatures[1]))
                    attack_results.append(creatures[1].attack(creatures[0], attack_summary))
                for c in creatures: c.set_surprised(False)

            self.state = State.FIGHT

            # perform creature attacks
            # if an attack kills the opposing creature, the fight ends immediately
            for i, attack in enumerate(attack_results):
                j = 1 - i
                kill = creatures[j].damage(attack.damage)
                option = "default"
                if attack.attack_hit == dndclient.ActionOutcome.CRIT_SUCCESS:
                    option = "crit_success"

                # character attack
                if isinstance(creatures[i], Character):
                    nice_attack_name = " and ".join(set([a.name for a in attack.attacks])) or attack.name
                    if attack.attack_hit.value > 0:
                        attack_summaries.append(lang.attack_character.result.choose(
                            nice_attack_name,
                            creatures[j].name,
                            attack.damage,
                            option=option
                        ))
                        character_attacks = attack

                    # use up spell slots
                    spells = set()
                    for a in attack.attacks:
                        if isinstance(a, AttackSpell) and a.spell.index not in spells:
                            spells.add(a.spell.index)
                            self.character.spell_slots[a.slot_level].use()

                    if kill:
                        attack_summaries.append(lang.death_monster.result.choose(
                            self.monster.name
                        ))

                        db.create_combat_log(creatures[i], creatures[j], nice_attack_name)
                        creatures[j].died_at = datetime.now(timezone.utc)
                        creatures[i].stats["kills"] += 1

                        inv_check = self.character.skill_check("investigation")
                        per_check = self.character.skill_check("perception")
                        gold_rolls = 1
                        if inv_check == 20 or per_check == 20:
                            gold_rolls = 3
                        elif inv_check == 0 or per_check == 0:
                            gold_rolls = 0
                        elif max(inv_check, per_check) > 10:
                            gold_rolls = 2

                        # earn loot
                        diff_ratio = (dndclient.CHALLENGE_RATINGS.index(self.monster.challenge_rating) + 1) / len(dndclient.CHALLENGE_RATINGS)
                        loot_chance = util.clamp((diff_ratio * 2 * max(inv_check, per_check)) / 100, 0, 1)

                        drops = list(self.monster.drops())
                        if len(drops) > 0 and (inv_check == 20 or random.random() <= loot_chance):
                            item = random.choice(drops)
                            self.character.equip_item(item)
                            if isinstance(item, MagicItem):
                                attack_summaries.append(lang.death_monster.result.loot_effect.choose(
                                    item.name,
                                    self.monster.name,
                                    item.effect_str # type: ignore[attr-defined]
                                ))

                        # earn gold and xp
                        reward_gold = max(1, math.ceil(self.monster.challenge_rating * Dice("1d6").roll(gold_rolls)))
                        reward_gold = int(reward_gold * float(util.config["gold_mult"]))
                        creatures[i].earn(Currency(reward_gold, Unit.gp))

                        reward_xp = int(self.monster.xp * float(util.config["exp_mult"]))
                        leveled_up = creatures[i].add_experience(reward_xp)
                        if leveled_up:
                            self.map_append(util.emojis["experience"]["level"])

                        attack_summaries.append("+{}{} +{}{}".format(
                            reward_gold,
                            util.emojis["equipment"]["gold"],
                            reward_xp,
                            util.emojis["experience"]["xp"]
                        ))

                        # the monster died, so we need to make sure its updated before
                        # we discard it
                        db.update_campaign(self)
                        self.state = State.ADVENTURE
                        self.previous_monster = self.monster
                        self.monster = None
                        return "\n".join(attack_summaries)
                # monster attack
                else:
                    nice_attack_name = " and ".join(set([a.name for a in attack.attacks])) or attack.name
                    if attack.attack_hit.value > 0:
                        attack_summaries.append(lang.attack_monster.result.choose(
                            creatures[i].name,
                            nice_attack_name,
                            attack.damage,
                            option=option
                        ))
                        monster_attacks = attack

                    # use up spell slots
                    spells = set()
                    for a in attack.attacks:
                        if isinstance(a, AttackSpell) and a.spell.index not in spells:
                            spells.add(a.spell.index)
                            self.monster.spell_slots[a.slot_level].use()

                    if kill:
                        # excess damage causes instant kill
                        if abs(creatures[j].hitpoints) >= creatures[j].max_hitpoints:
                            attack_summaries.append(lang.death_character.result.choose(option="instant"))
                            db.create_combat_log(creatures[i], creatures[j], nice_attack_name)
                            self.state = State.END
                            return "\n".join(attack_summaries)

                        elif creatures[j].death_saving_throws():
                            attack_summaries.append(lang.death_character.result.choose(option="unconscious"))

                        else:
                            attack_summaries.append(lang.death_character.result.choose())
                            db.create_combat_log(creatures[i], creatures[j], nice_attack_name)
                            self.state = State.END
                            return "\n".join(attack_summaries)

            return "\n".join(attack_summaries)

        def description() -> image.Description:

            crit_modifiers = [
                "ferociously",
                "expertly",
                "gracefully",
                "perfectly",
                "fiercely"
            ]
            desc = image.Description(
                world=self.world,
                dimensions=Dimensions.WIDE
            )

            # Monster killed
            if self.monster is None:
                assert self.previous_monster
                desc.style = f"A heroic battle victory, battle wounds, blood splatter"
                victory_texts = [
                    "cheerily raising their weapon to the sky",
                    "walking away wearily",
                    "battle cry",
                ]
                desc.text = random.choice(victory_texts)
                return desc

            # Character killed
            elif self.state == State.END:
                death_options = [
                    ("kneeling on the ground, exhausted, head down","Battle wounds, blood splatter"),
                    ("shallow grave, ovegrown, abandoned","eery background, sadness"),
                    ("bloodied hand reaching to the sky", "closeup, sad, last grasp of life"),
                    ("motionless arm grasping a weapon lying on the ground", "closeup, desaturated colors"),
                ]
                death_choice = random.choice(death_options)
                desc.text = death_choice[0]
                desc.style = death_choice[1]

            # Character hit their attack
            elif character_attacks:
                if character_attacks.attack_hit == dndclient.ActionOutcome.CRIT_SUCCESS:
                    desc.text += f"{random.choice(crit_modifiers)} "
                desc.text = f"attacking with their {' and '.join([a.name for a in character_attacks.attacks])}"
                desc.style = "Aggressive, hostile"

            # Monster hit their attack
            elif monster_attacks:
                desc.hero = False
                desc.text = f"{self.monster.description_modifier} {self.monster.name}, "
                if monster_attacks.attack_hit == dndclient.ActionOutcome.CRIT_SUCCESS:
                    desc.text += f"{random.choice(crit_modifiers)} "
                desc.text += f"attacking with its {' and '.join([a.name for a in monster_attacks.attacks])}"
                desc.style = "Aggressive, hostile"

            else:
                desc.dimensions = Dimensions.ULTRA_WIDE
                desc.style = f"Hero vs {self.monster.monster_type} one-on-one fight,"
                desc.text = f"fighting against a {self.monster.name} monster"

            return desc

        return Option(
            lang.options.attack_option.choose(
                attack_summary.get_prompt()
            ),
            action,
            description,
        )


    def option_run(self, monster: Monster) -> Option:

        def action() -> str:
            assert self.monster
            attack_summary = []
            run_result = self.character.run(self.monster)
            monster_attack = None

            match run_result:
                # Can't get away and monster gets an attack
                case dndclient.ActionOutcome.CRIT_FAILURE:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    attack_summary.append(lang.run_character.result.choose(option="crit_failure"))
                    if monster_attack.damage > 0:
                        attack_summary.append(lang.attack_monster.result.choose(
                            self.monster.name,
                            monster_attack.name,
                            monster_attack.damage
                        ))
                    self.state = State.FIGHT

                # May get away, but if monster attack hits, combat resumes
                case dndclient.ActionOutcome.FAILURE:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        self.state = State.FIGHT
                        option = "failure"

                    attack_summary.append(lang.run_character.result.choose(
                        monster.name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))

                    if monster_attack.damage <= 0:
                        self.state = State.ADVENTURE
                        self.previous_monster = self.monster
                        self.monster = None

                # Get away, but monster can attack
                case dndclient.ActionOutcome.SUCCESS:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        option = "success"

                    attack_summary.append(lang.run_character.result.choose(
                        monster.name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))
                    self.state = State.ADVENTURE
                    self.previous_monster = self.monster
                    self.monster = None

                # Always get away with no consequences
                case dndclient.ActionOutcome.CRIT_SUCCESS:
                    attack_summary.append(lang.run_character.result.choose(option="crit_success"))
                    self.state = State.ADVENTURE
                    self.previous_monster = self.monster
                    self.monster = None

            if monster_attack and monster_attack.damage > 0:
                excess_damage = monster_attack.damage - self.character.hitpoints
                if monster_attack and self.character.damage(monster_attack.damage):
                    nice_attack_name = " and ".join(set([a.name for a in monster_attack.attacks])) or monster_attack.name
                    if excess_damage >= self.character.max_hitpoints:
                        db.create_combat_log(monster, self.character, nice_attack_name)
                        attack_summary.append(lang.death_character.result.choose(option="instant"))
                        self.state = State.END

                    elif self.character.death_saving_throws():
                        attack_summary.append(lang.death_character.result.choose(option="unconscious"))

                    else:
                        db.create_combat_log(monster, self.character, nice_attack_name)
                        attack_summary.append(lang.death_character.result.choose())
                        self.state = State.END

            return "\n".join(attack_summary)

        def description() -> image.Description:

            return image.Description(
                world=self.world,
                text=f"scared and running away from a {monster.name}",
                dimensions=Dimensions.WIDE,
                style=f"Hero vs {monster.monster_type} one-on-one encounter,",
                weapon=False
            )

        return Option(
            lang.options.run.choose(
                util.emojis['prompts']['run']
            ),
            action,
            description,
        )


    def option_avoid(self, monster: Monster | None = None, name: str = "") -> Option:

        def action() -> str:
            nonlocal name
            if monster and not name:
                name = monster.name

            if monster is None:
                self.state = State.ADVENTURE
                return lang.avoid.result.choose(name, option="success")

            elif self.character.avoid(monster).value > 0:
                self.state = State.ADVENTURE
                return lang.avoid.result.choose(name, option="success")

            else:
                mi = monster.roll_initiative()
                ci = self.character.roll_initiative()
                util.debug(f"MI: {mi}, CI: {ci}")
                self.monster = monster
                self.character.recent_rolls["Initiative"] = ci
                self.monster.recent_rolls["Initiative"] = mi
                self.monster.id = db.create_monster(self.monster, self)
                self.map_append(monster.emoji())
                self.state = State.FIGHT
                return lang.avoid.result.choose(name, option="failure")

        def description() -> image.Description:

            desc = image.Description(
                world=self.world,
                text="avoiding a suspicious object",
                dimensions=Dimensions.WIDE
            )
            if monster:
                desc.text = f"avoiding a {monster.name}"
                desc.style = f"Hero vs {monster.monster_type} one-on-one encounter,"
            return desc

        return Option(
            lang.options.avoid.choose(
                util.emojis["skills"]["stealth"]
            ),
            action,
            description,
        )


    def option_leave(self, text = None) -> Option:

        def action() -> str:
            match self.state:
                case State.TREASURE:
                    self.state = State.ADVENTURE
                    return lang.treasure.result.leave.choose()

                case State.SHOP:
                    self.state = State.TOWN
                    return lang.shop.result.leave.choose()

                case State.TRADER:
                    self.state = State.ADVENTURE
                    return lang.trader.result.leave.choose()

                case _:
                    raise ValueError(f"Invalid parent state {self.state}")

        def description() -> image.Description:

            desc = image.Description(
                world=self.world,
                dimensions=Dimensions.TALL,
                weapon=False
            )

            match self.state:
                case State.TREASURE:
                    desc.text = f"leaving the rest of the treasure."
                    return desc

                case State.SHOP:
                    desc.text = "exiting a shop through its doors."
                    desc.location = Location("inside a merchant stop")
                    return desc

                case State.TRADER:
                    desc.text = "saying goodbye to merchant."
                    return desc

                case _:
                    raise ValueError(f"Invalid parent state {self.state}")

        if text is None:
            text = lang.options.leave.choose(util.emojis["prompts"]["back"])
        desc = description()
        return Option(text, action, lambda: desc)

#endregion

    def bio(self) -> str:
        """Get the string to display in the bot bio.
        """
        assert self.start_time
        delta = datetime.now(timezone.utc) - self.start_time
        return "{}\n\n{}\n{} {} {} {} {} {}".format(
            self.character.bio(),
            self.map_str(),
            util.emojis["campaign"]["id"],
            self.id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds()),
            util.emojis["campaign"]["user"],
            self.votes
        )

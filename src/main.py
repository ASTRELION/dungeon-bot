# Dungeons Bot
# Copyright (C) 2023  ASTRELION

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import signal
import sys
from time import sleep

from campaign import Campaign, State
import client
import commands
import db
import util


def cancel(signal, frame):
    """Intercept for ctrl-c.
    """
    util.warn("Forcibly exiting...")
    sys.exit(0)


def main():
    """Main :)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry", "-d",
        help="Do not actually make posts and instead print to console.",
        action="store_true"
    )
    parser.add_argument(
        "--auto", "-a",
        help="Automatically run --dry campaigns.",
        action="store_true"
    )
    parser.add_argument(
        "--skip-polls",
        help="Skip these amount of polls",
        action="store",
        type=int
    )
    parser.add_argument(
        "--skip-choice",
        help="When skipping polls, always use this option (starts at 0)",
        action="store",
        type=int
    )
    parser.add_argument(
        "--no-save", "-n",
        help="Do not perform or load saves.",
        action="store_true"
    )
    parser.add_argument(
        "--verbose", "-v",
        help="Set log level to DEBUG.",
        action="store_true"
    )
    parser.add_argument(
        "--quiet", "-q",
        help="Set log level to NONE.",
        action="store_true"
    )
    args = parser.parse_args()

    signal.signal(signal.SIGINT, cancel)

    if args.quiet:
        util.logger.setLevel(0)
    elif args.verbose:
        util.logger.setLevel(util.logging.DEBUG)

    util.reset_session()
    campaign: Campaign = None
    campaign_id = 0
    client.dry = args.dry
    client.auto = args.auto
    db.dry = args.dry
    commands.dry = args.dry
    if not args.dry:
        client.init_client()
        commands.start_command_thread()
        db.init_db()
    util.init_logger()

    db.update_emojis()

    util.info("Staring epic...")

    if not args.no_save:
        try:
            campaign = Campaign.load()
            campaign.skip_polls = args.skip_polls if args.skip_polls else 0
            campaign.skip_choice = args.skip_choice if args.skip_choice else 0
            campaign_id = campaign.id
            if campaign.version != util.config["_version"]:
                campaign = None
                util.warn("Loaded save from file, but it's outdated, discarding.")
            else:
                util.info("Loaded save from file, resuming.")
                # If the last campaign ended, just start a new one
                if campaign.state == State.END:
                    campaign = None
        except:
            campaign = None
            # try to obtain new campaign ID from database
            last_campaign = db.get_last_campaign()
            if last_campaign:
                campaign_id = last_campaign.id
                util.debug(f"Loaded previous campaign ID of {campaign_id}")
            util.warn("Unable to load a save, starting fresh.")

    client.start_tips()

    while True:
        if not client.dry:
            util.reset_session()
        if commands.manager_ns:
            commands.manager_ns.campaign = None

        if not campaign:
            campaign_id += 1
            campaign = Campaign(campaign_id, util.config["_version"])
            campaign.skip_polls = args.skip_polls if args.skip_polls else 0
            campaign.skip_choice = args.skip_choice if args.skip_choice else 0
            campaign.update_command_handler()
            campaign.start()
        else:
            campaign.update_command_handler()
            campaign.start(util.emojis["prompts"]["resume"])

        if not args.dry:
            sleep(util.config["death_duration"])

        campaign = None


if __name__ == "__main__":
    main()

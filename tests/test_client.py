import client
import util


def test_get_media_post() -> None:

    assert client.get_media_post("some/image.png", "alt text") is None


def test_command_reply() -> None:

    status = client.post_status(1, "test")
    assert "id" in client.command_reply("test reply", status)


def test_post_status() -> None:

    assert "id" in client.post_status(1, "Test status", "Last result")
    assert "id" in client.post_status(1, "Test status", "Last result", False)


def test_post_poll() -> None:

    status = client.post_poll(1, "Test status", "Last result", ["Option 1", "Option 2"])
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll(1, "Test status", "Last result", ["Option 1", "Option 2"])
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll(1, "Test status", "Last result", ["Option 1", "Option 2"], duration=10)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll(1, "Test status", "Last result", ["Option 1", "Option 2"], True)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]


def test_delete_post() -> None:

    status = client.post_status(1, "Test status", "Last result")
    assert client.delete_post(status["id"])


def test_poll_result() -> None:

    client.auto = True

    options = ["Option 1", "Option 2"]
    status = client.post_poll(1, "Test status", "Last result", options)
    result = client.poll_result(status["id"])
    assert isinstance(result, tuple)
    assert len(result[0]) == 1
    assert result[1] > 0


def test_find_winning_index() -> None:

    groups: list

    votes = [1, 2, 3, 5]
    assert client.find_winning_index(votes) == 3

    votes = [1, 2, 3, 5]
    groups = [1, 2]
    assert client.find_winning_index(votes, groups) == 2

    votes = [1, 1, 1, 0]
    groups = [[0], [1], [2, 3]]
    assert client.find_winning_index(votes, groups) == 0

    votes = [1, 2, 3, 5]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 2

    votes = [1, 2, 3, 5]
    groups = [[0, 1], [2, 3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 2, 3, 5]
    groups = [[0], [1], [2], [3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 1, 0, 2]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 0

    votes = [1, 1, 0, 3]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 1, 0, 1]
    groups = [[2, 3], [0], [1]]
    assert client.find_winning_index(votes, util.sort_multi(groups)) == 0

    votes = [19, 1, 13, 7]
    groups = [[2, 3], [0], [1]]
    assert client.find_winning_index(votes, util.sort_multi(groups)) == 2

    votes = [1, 1, 1, 0]
    groups = [0, [1, [2, 3]]]
    assert client.find_winning_index(votes, groups) == 1

    votes = [1, 1, 1, 0]
    groups = [0, [1, [2, 3]]]
    assert client.find_winning_index(votes, groups) == 1

    votes = [1, 1, 1, 1]
    groups = [0, [1, [2, 3]]]
    assert client.find_winning_index(votes, groups) == 2

    votes = [1, 1, 10, 1]
    groups = [0, [1, [3]]]
    assert client.find_winning_index(votes, groups) == 1


def test_sort_groups() -> None:

    groups: list

    votes = [1, 2, 3, 4]
    assert client.sort_groups(votes) == [3, 2, 1, 0]

    votes = [1, 2, 3, 4]
    groups = [0, 1, 2, 3]
    assert client.sort_groups(votes, groups) == [3, 2, 1, 0]

    votes = [1, 2, 3, 4]
    groups = [[0, 1], [2, 3]]
    assert client.sort_groups(votes, groups) == [[3, 2], [1, 0]]

    votes = [1, 2, 3, 4]
    groups = [[0, 3], [1, 2]]
    assert client.sort_groups(votes, groups) == [[3, 0], [2, 1]]


def test_get_followers() -> None:

    followers = client.get_followers()
    assert len(followers) > 0

    for follower in followers:
        assert isinstance(follower, str)
        assert len(follower) > 0


def test_update_profile() -> None:

    assert "note" in client.update_profile("Bio")


def test_get_supporters() -> None:

    # This should always just be an empty set, since a default API URL isn't
    # provided.
    assert len(client.get_supporters()) >= 0


def test_get_supporter_name() -> None:

    assert len(client.get_supporter_name()) >= 0

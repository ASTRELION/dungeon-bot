import client
import db


# Test the test setup, i.e. that everything is dry running
def test_test_config() -> None:

    assert client.dry
    assert db.dry

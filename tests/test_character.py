from copy import deepcopy
import pytest
import re
from effect import Effect, EffectMod, EffectStat, EffectType
from dnd.magic_item import MagicItem, PotionOfFlying, PotionOfGiantStrength, PotionOfHealing, PotionOfHeroism, PotionOfInvisibility, PotionOfResistance, PotionOfSpeed

import util as util
from character import Character
import client
import dnd.dndclient as dndclient
from currency import Currency, Unit
from dnd.equipment import Equipment


@pytest.fixture
def character() -> Character:

    client.dry = True
    return Character(1, "test")


@pytest.fixture
def character_init(character: Character) -> Character:

    character.init()
    return character


def test_init(character: Character) -> None:

    # __init__
    assert character.name.startswith("Test")
    assert character.race
    assert character.clazz

    assert character.level == 1
    assert character.xp == 0
    assert character.hit_dice == character.level

    assert character.proficiency_bonus == 2

    # init
    character.init()

    for ability in character.abilities:
        assert character.abilities[ability] >= 3
        assert character.abilities[ability] <= 20

    assert len(character.proficiencies) > 0
    assert len(character.hands) > 0
    assert character.armor == [] or len(character.armor) > 0
    assert (character.GOLD_ROLL.min() * 10) + util.config["start_gold"] <= character.currency.amount <= (character.GOLD_ROLL.max() * 10) + util.config["start_gold"]

    assert character.max_hitpoints > 0
    assert character.hitpoints == character.max_hitpoints


def test_ability_priority(character_init: Character) -> None:

    assert len(character_init.ability_priority()) > 0
    assert len(character_init.ability_priority()) <= len(character_init.ORDERED_ABILITY_INDEXES)
    for ability in character_init.ability_priority():
        assert ability in character_init.ORDERED_ABILITY_INDEXES


def test_proficient_weapons(character_init: Character) -> None:

    weapons = character_init.proficient_weapons()
    assert len(weapons) > 0
    assert all(isinstance(w, Equipment) for w in weapons)


def test_proficient_armor(character_init: Character) -> None:

    armor = character_init.proficient_armor()
    assert armor == [] or len(armor) > 0
    assert all(isinstance(a, Equipment) for a in armor)


def test_armor_class(character_init: Character) -> None:

    character_init.unequip_equipment()

    ac = character_init.armor_class([Equipment(dndclient.get_equipment("leather-armor"))])
    assert ac == 11 + character_init.ability_modifier("dex")

    ac = character_init.armor_class([Equipment(dndclient.get_equipment("chain-mail"))])
    assert ac == 16


def test_spend(character_init: Character) -> None:

    gold = character_init.currency
    assert character_init.spend(Currency(1)) == gold - 1
    assert character_init.currency == gold - 1


def test_earn(character_init: Character) -> None:

    gold = character_init.currency
    assert character_init.earn(Currency(1)) == gold + 1
    assert character_init.currency == gold + 1


def test_equip_equipment(character_init: Character) -> None:

    character_init.unequip_equipment()
    assert len(character_init.hands) == 0
    assert len(character_init.armor) == 0

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    dagger = Equipment(dndclient.get_equipment("dagger")) # light 1d4
    shortsword = Equipment(dndclient.get_equipment("shortsword")) # light 1d6
    mace = Equipment(dndclient.get_equipment("mace")) # one-handed 1d6
    longsword = Equipment(dndclient.get_equipment("longsword")) # one-handed 1d8
    greataxe = Equipment(dndclient.get_equipment("greataxe")) # two-handed 1d12
    shield = Equipment(dndclient.get_equipment("shield")) # shield

    assert character_init.equip_equipment(dagger) == []
    assert character_init.hands == [dagger]

    assert character_init.equip_equipment(shortsword) == []
    assert character_init.hands == [shortsword, dagger]

    character_init.unequip_equipment()
    character_init.equip_equipment(longsword)

    assert character_init.equip_equipment(shield) == []
    assert character_init.hands == [longsword, shield]

    character_init.unequip_equipment()
    character_init.equip_equipment(greataxe)

    assert character_init.equip_equipment(dagger, True) == [greataxe]
    assert character_init.hands == [dagger]

    character_init.unequip_equipment()
    character_init.equip_equipment(greataxe)

    assert character_init.equip_equipment(shield, True) == [greataxe]
    assert character_init.hands == [shield]

    character_init.unequip_equipment()
    character_init.equip_equipment(dagger)
    character_init.equip_equipment(dagger)

    assert character_init.equip_equipment(shield, True) == [dagger]
    assert character_init.hands == [dagger, shield]

    assert character_init.equip_equipment(shield, True) == [shield]
    assert character_init.hands == [dagger, shield]

    assert character_init.equip_equipment(longsword, True) == [dagger]
    assert character_init.hands == [longsword, shield]

    assert character_init.equip_equipment(longsword, True) == [longsword]
    assert character_init.hands == [longsword, shield]

    assert character_init.equip_equipment(mace, True) == [longsword]
    assert character_init.hands == [mace, shield]

    character_init.unequip_equipment()
    character_init.equip_equipment(dagger)
    character_init.equip_equipment(shield)

    assert character_init.equip_equipment(dagger, True) == [shield]
    assert character_init.hands == [dagger, dagger]

    character_init.unequip_equipment()
    character_init.equip_equipment(dagger)
    character_init.equip_equipment(dagger)

    assert character_init.equip_equipment(dagger, True) == [dagger]
    assert character_init.hands == [dagger, dagger]

    assert character_init.equip_equipment(shortsword, True) == [dagger]
    assert character_init.hands == [shortsword, dagger]

    assert character_init.equip_equipment(shortsword, True) == [dagger]
    assert character_init.hands == [shortsword, shortsword]

    character_init.unequip_equipment()
    character_init.equip_equipment(dagger)
    character_init.equip_equipment(dagger)

    assert character_init.equip_equipment(greataxe, True, True) == [dagger, dagger]
    assert character_init.hands == [dagger, dagger]
    assert character_init.equip_equipment(longsword, True, True) == [dagger, dagger]
    assert character_init.hands == [dagger, dagger]
    assert character_init.equip_equipment(dagger, True, True) == [dagger]
    assert character_init.hands == [dagger, dagger]
    assert character_init.equip_equipment(shield, True, True) == [dagger]
    assert character_init.hands == [dagger, dagger]

    character_init.unequip_equipment()
    character_init.equip_equipment(leather_armor)

    assert character_init.armor == [leather_armor]
    assert character_init.equip_equipment(leather_armor, True, True) == [leather_armor]
    assert character_init.armor == [leather_armor]


def test_equip_item(character_init: Character) -> None:

    potion: MagicItem

    for potion in PotionOfHealing.all():
        character_init.damage(1)
        character_init.equip_item(potion)
        assert character_init.hitpoints == character_init.max_hitpoints

    for potion in PotionOfGiantStrength.all():
        character_init.equip_item(potion)
        assert character_init.ability_score("str") >= potion.strength

    potion = PotionOfFlying.inst()
    character_init.equip_item(potion)
    assert character_init.get_fly_speed() > 0

    potion = PotionOfHeroism.inst()
    character_init.equip_item(potion)
    assert character_init.temp_hitpoints() == 10

    potion = PotionOfInvisibility.inst()
    character_init.equip_item(potion)

    for potion in PotionOfResistance.all():
        character_init.equip_item(potion)
        assert potion.variant_type.value in character_init.get_resistances()

    potion = PotionOfSpeed.inst()
    old_armor = character_init.armor_class()
    character_init.equip_item(potion)
    assert character_init.get_walk_speed() == character_init.walk_speed * 2
    assert character_init.armor_class() == old_armor + 2
    assert character_init.attack_count() == 2


def test_attack(character_init: Character) -> None:

    for attack_summary in character_init.attack_options(3, character_init):
        attack = character_init.attack(character_init, attack_summary)
        if attack.attack_hit.value <= 0:
            assert attack.damage == 0
        else:
            assert attack.damage > 0


def test_death_saving_throws(character_init: Character) -> None:

    throws = character_init.death_saving_throws()
    if throws:
        assert character_init.hitpoints == 1


def test_add_experience(character_init: Character) -> None:

    level = character_init.level
    xp = character_init.xp

    character_init.add_experience(1)
    assert character_init.xp == xp + 1
    assert character_init.level == level

    dndclient.util.info(character_init.xp)
    character_init.add_experience(dndclient.EXPERIENCE_REQUIREMENT[3])
    assert character_init.level == 4


def test_short_rest(character_init: Character) -> None:

    effect = Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 30, 3600)
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))

    character_init.damage(1)
    hitpoints = character_init.hitpoints
    hitdice = character_init.hit_dice
    rest = character_init.short_rest()
    assert rest.hitpoints_gained > 0
    assert rest.hitdice_used > 0
    assert len(rest.effects_lost) == 0
    assert character_init.hit_dice == hitdice - 1
    assert character_init.hitpoints == min(character_init.max_hitpoints, hitpoints + rest.hitpoints_gained)

    rest = character_init.short_rest()
    assert len(rest.effects_lost) == 5


def test_long_rest(character_init: Character) -> None:

    effect = Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 30, 3600)
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))
    character_init.add_effect(deepcopy(effect))

    hitdice = character_init.hit_dice
    character_init.damage(1)
    character_init.used_today["test"] = 10
    rest = character_init.long_rest()
    assert rest.hitpoints_gained > 0
    assert rest.hitdice_gained >= 0
    assert len(rest.effects_lost) == 5
    assert character_init.hitpoints == character_init.max_hitpoints
    assert character_init.hit_dice >= hitdice
    for key in character_init.used_today:
        assert character_init.used_today[key] == 0

    assert len(character_init.effects) == 0


def test_title(character_init: Character) -> None:

    assert len(character_init.title()) > 0


def test_bio(character_init: Character) -> None:

    bio = character_init.bio()
    assert re.search(r"Str \d+ Dex \d+ Con \d+ Int \d+ Wis \d+ Cha \d+", bio)


def test_short_bio(character_init: Character) -> None:

    assert len(character_init.short_bio()) > 0


def test_extended_bio(character_init: Character) -> None:

    assert len(character_init.extended_bio()) > 0


def test_characters() -> None:

    races = dndclient.get_races()
    classes = dndclient.get_classes()

    for r in races:
        for c in classes:
            character = Character(1, f"test{r['index']}{c['index']}", dndclient.get_race(r["index"]), dndclient.get_class(c["index"]))
            test_init(character)
            test_proficient_weapons(character)
            test_proficient_armor(character)
            test_armor_class(character)
            test_spend(character)
            test_earn(character)
            test_equip_equipment(character)
            test_add_experience(character)
            test_attack(character)
            test_short_rest(character)
            test_long_rest(character)
            test_death_saving_throws(character)

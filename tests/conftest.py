

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))

import pytest

import client
import commands
import db


@pytest.fixture(autouse=True)
def before_all():

    client.dry = True
    db.dry = True
    commands.dry = True

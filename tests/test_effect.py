from effect import Effect, EffectMod, EffectStat, EffectType


def test_effect() -> None:

    effect = Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(1, [effect]) == 10

    effect = Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(1, [effect]) == 11

    effect = Effect(EffectType.POSITIVE, EffectMod.SUB, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(11, [effect]) == 1

    effect = Effect(EffectType.POSITIVE, EffectMod.MULT, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(2, [effect]) == 20

    effect = Effect(EffectType.POSITIVE, EffectMod.DIV, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(20, [effect]) == 2

    effect = Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(20, [effect]) == 20

    effect = Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 10)
    assert Effect.apply_effects(1, [effect]) == 10

    effects = [
        Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.SUB, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.MULT, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.DIV, EffectStat.STRENGTH, 10)
    ]
    assert Effect.apply_effects(1, effects) == 10

    effects = [
        Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.SUB, EffectStat.STRENGTH, 5),
        Effect(EffectType.POSITIVE, EffectMod.MULT, EffectStat.STRENGTH, 4),
        Effect(EffectType.POSITIVE, EffectMod.DIV, EffectStat.STRENGTH, 2)
    ]
    assert Effect.apply_effects(20, effects) == 45

    effects = [
        Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.SUB, EffectStat.STRENGTH, 5),
        Effect(EffectType.POSITIVE, EffectMod.MULT, EffectStat.STRENGTH, 4),
        Effect(EffectType.POSITIVE, EffectMod.DIV, EffectStat.STRENGTH, 2),
        Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 10)
    ]
    assert Effect.apply_effects(20, effects) == 45

    effects = [
        Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10),
        Effect(EffectType.POSITIVE, EffectMod.SUB, EffectStat.STRENGTH, 5),
        Effect(EffectType.POSITIVE, EffectMod.MULT, EffectStat.STRENGTH, 4),
        Effect(EffectType.POSITIVE, EffectMod.DIV, EffectStat.STRENGTH, 2),
        Effect(EffectType.POSITIVE, EffectMod.MAX, EffectStat.STRENGTH, 30)
    ]
    assert Effect.apply_effects(20, effects) == 65

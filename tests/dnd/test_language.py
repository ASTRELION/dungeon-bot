import dnd.dndclient as dndclient
from dnd.language import Language


def test_init() -> None:

    prev_language = None
    for l in dndclient.get_languages():
        dnd_language = dndclient.get_language(l["index"])
        language = Language(dnd_language)
        assert language
        assert language.index == dnd_language["index"]
        assert language == language
        if prev_language is not None:
            assert prev_language != language
        prev_language = language

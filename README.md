<div align="center">
<a href="https://mastodon.social/@dungeons" rel="me">
<img src="images/banner-large.png" width="100%">
</a>
<br />
<a href="https://mastodon.social/@dungeons" rel="me">
<img src="https://img.shields.io/mastodon/follow/109354069933492315?style=social">
<br />
<a href="https://gitlab.com/ASTRELION/dungeons-bot/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/ASTRELION/dungeons-bot/badges/main/pipeline.svg" /></a>
</a>
<a href="https://gitlab.com/ASTRELION/dungeons-bot/-/commits/main"><img alt="coverage report" src="https://gitlab.com/ASTRELION/dungeons-bot/badges/main/coverage.svg" /></a>

<h1>Dungeons Bot</h1>
</div>

[Mastodon](https://joinmastodon.org/) bot that simulates D&D-like adventures through public polls. Utilizes [Mastodon.py](https://github.com/halcy/Mastodon.py) and the [D&D 5e SRD API](https://dnd5eapi.co).

Optional AI image generation is done by [AI Horde](https://aihorde.net/) and its [API](https://aihorde.net/api/).

**Official account: <a href="https://mastodon.social/@dungeons" rel="me">@dungeons@mastodon.social</a>**

Public test account: <a href="https://mastodon.social/@dungeonsbeta" rel="me">@dungeonsbeta@mastodon.social</a>

Private test account: <a href="https://mastodon.social/@dungeonslabs" rel="me">@dungeonslabs@mastodon.social</a>

[**Help & Extra Information**](https://dungeons.astrelion.com)

## Features

*Unchecked features are features I'd like to implement, not a commitment.*

- [x] Random character creation with names chosen from followers
- [x] Simple combat between random monsters
- [x] Advanced combat between monsters (spells, feats, attack types, etc.)
- [x] Leveling up
- [x] Loot
- [ ] Dungeons (back-to-back monster fights, loot)
- [x] Neutral town areas for shopping, rest, etc.
- [ ] Maps (towns, terrains, dungeons, etc.)
- [ ] Structured stories/missions
- [ ] File/node-driven maps/stories for custom campaigns
- [ ] Homebrew content support
- [x] Saving (state is saved between runs of the app)
- [x] Query campaign/character info by commenting or DMs
- [x] Static website for docs and information
- [x] Dynamic website for full character stats, campaign history, etc.
- [x] AI generated profile avatar/header based on character

*I'd like to eventually scrap this version of the bot in favor of an entirely rewritten version in another language at some point in the future.*

## Running yourself

### Create Mastodon account & application

1. Create a [Mastodon account](https://joinmastodon.org/servers)
2. Create a new application on your Mastodon account (e.g. `https://<server>/settings/applications/new`)
    - You should also probably check 'This is a bot account' in your profile settings (e.g. `https://<server>/settings/profile`)
3. Copy the access token for the application

### Setup App

#### Docker

1. Create the [docker-compose file](docker-compose.yml) (or [build yourself](Dockerfile))
2. Copy files from `config/` to `/opt/dungeons/` or whatever directory you specify in the `volumes` section
    - Copy `.token.example` to `.token` and enter your token and Mastodon URL
    - If you want AI images generated, you will need an image to overlay the AI one named `avatar-overlay.png` for the avatar image, and `header-overlay.png` for the header
    - If you want a "reset" image (used in-between campaigns), you will need an image named `avatar.png` for the avatar image, and `header.png` for the header
3. Run
    ```shell
    docker-compose up -d
    ```

#### Traditional

1. [Clone the project](https://gitlab.com/ASTRELION/dungeons-bot.git)
2. Copy [`config/.token.example`](config/.token.example) to `data/.token` and add your token and Mastodon URL
    - If you want AI images generated, you will need an image to overlay the AI one named `avatar-overlay.png` for the avatar image, and `header-overlay.png` for the header
    - If you want a "reset" image (used in-between campaigns), you will need an image named `avatar.png` for the avatar image, and `header.png` for the header
3. Install prerequisites with `pip3 install -r requirements.txt`
4. Run with `python3 main.py`

*Note that according to the GNU AGPLv3 [License](LICENSE), you must provide the source code to the application and honor the original License.*

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## Awesome Dungeons

*The following are **unofficial** applications created by the Dungeons community. Create a new [issue](https://gitlab.com/ASTRELION/dungeons-bot/-/issues/new) if you'd like your project added, or submit a [MR](https://gitlab.com/ASTRELION/dungeons-bot/-/merge_requests/new).*

- [Dungeons Character Dashboard: Chrome Extension](https://github.com/skullzarmy/dungeons-chrome-extension) by [skllzrmy](https://mastodon.social/@skllzrmy)
- [Dungeon Guide](https://mastodon.social/@dungeon_guide) by [skllzrmy](https://mastodon.social/@skllzrmy)
- [DungeonConsole](https://codeberg.org/screw_dog/DungeonConsole) by [screw_dog](https://aus.social/@screw_dog)

## License

```
Dungeons Bot
Copyright (C) 2023  ASTRELION

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

## [Donate](https://astrelion.com/links#donate)

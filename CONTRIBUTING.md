# Contributing

The project is mostly maintained by one person and undergoes frequent system changes and rewrites. I would suggest only making low-scope contributions for the time being.

Consider joining the [Dev room in the Matrix space](https://matrix.to/#/#dungeonsdev:matrix.org) if you make frequent contributions or wish to discuss development.

## Issues

If you encounter a bug or want a feature added, [open an issue here](https://gitlab.com/ASTRELION/dungeons-bot/-/issues/new), and use the relevant issue template.

## Code & Merge Requests (MR)

1. [Fork the repository](https://gitlab.com/ASTRELION/dungeons-bot/-/forks/new)
2. Clone your fork locally
3. Create a branch for your changes based on the `develop` branch
4. Make your changes
5. Run tests (these tests do not offer great coverage, make sure to test yourself locally, ideally with a test Mastodon account)
    ```shell
    python3 -m pytest
    ```
6. Commit and push your changes
7. [Submit a Merge Request (MR)](https://gitlab.com/ASTRELION/dungeons-bot/-/merge_requests/new) (same as a Pull Request (PR)) to the `develop` branch of the original repository

### Dev Environment

#### Create a virtual environment

```bash
python -m venv .venv
source .venv/bin/activate
```

#### Install packages

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

#### Code validation

Run `mypy` to validate types, or [set it up in your editor](https://github.com/python/mypy?tab=readme-ov-file#integrations).

```bash
mypy
```

### Database

#### Generate migrations

If any model in `db.py` is changed, this **must** be ran before the changes can be merged.

```
alembic -c config/alembic.ini revision --autogenerate
```

#### Upgrade Remote DB

*This will automatically happen when running the bot.*

```
alembic -c config/alembic.ini upgrade head
```

### Description of files

**Root folders**
| Folder | Description |
|--------|-------------|
| `alembic/` | Auto-generated database migrations and configuration files. |
| `config/` | Contains configuration files. These are copied and used from `data/` when the program runs. The files in `config/` are used as defaults, so when modifying configuration values for an instance you should modify the data in `data/`. |
| `data/` | The program uses this directory for all data files, but is not actually part of the repository. If you need to add a `.token`, it should go here before running. |
| `images/` | Branding images, logos, banners, etc. |
| `public/` | Static website used for https://dungeons.astrelion.com. |
| `src/` | Source code files. |
| `tests/` | Any file in this directory will be ran as tests for `pytest`. Each file corresponds to a file in `src/` and follows the format `test_[src_file_name]`. |
